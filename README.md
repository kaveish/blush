# Introduction

BLUSH (Blood simulation using smoothed-particle hydrodynamics) is a code for the
simulation of blood flow using smoothed-particle hydrodynamics (SPH), a fully
mesh-free Lagrangian method. Development is particularly aimed at simulating the
movement of solid and gaseous particles in the flow.

# Usage

From base directory:

make parallel
./scripts/run.sh
./scripts/plot.py
./scripts/display.py

# Directory structure
`
blush/
  README.md
  Makefile # builds all modules
  bin/     # executables from all modules
    gcc/
      debug/
        serial/
          blush
          blush-test
          linesampler
          linesampler-test
        parallel/
      release/
        serial/
        parallel/
    clang/
      ''
    icc/
      ''
    python/
      ''
  docs/      # any documentation
  external/  # external libraries
    lib/     # compiled libraries
    include/ # library headers
  modules/   # all constituent parts of the project
    scripts/ # collection of python scripts, any sufficiently developed scripts
             # could be moved to their own module
    blush/   # base project
      Makefile # module makefile to include in project makefile
      examples/ # supplementary dir for usage examples, any additional folders
                # for a particular module are stored in the module directory,
                # not root
      src/   # all source files, .hpp .cpp etc.
      runs/  # another supplementary dir for job scripts
      test/  # all modules need tests!
    linesampler/ # utility c++ project
      Makefile
      src/
      test/
  obj/       # intermediate files generated during compilation
    (same as bin dirs)
      blush/
      blush-test/
      linesampler/
      linesampler-test/
`