#include "sph.hpp"

#include "equations.hpp"
#include "interact.hpp"
#include "particle.hpp"
#include "particlehandler.hpp"
#include "particleutils.hpp"
#include "runparams.hpp"

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

using namespace blush;

namespace {
  using InteractType = Interactor::InteractType;

  std::string OutputFileName(int n_file, const std::string& file_base,
                             const RunParams& params) {
    std::ostringstream file_name;
    std::string directory;
    if (params.init_mode)
      directory = params.directory_init;
    else
      directory = params.directory_run;
    file_name << directory << file_base
              << std::setw(4) << std::setfill('0') << n_file << ".dat";
    return(file_name.str());
  }

  std::string PrintParticle(const Particle& part)
  {
    std::stringstream stream;
    stream << part.r()[0] << " "
           << part.r()[1] << " "
           << part.v()[0] << " "
           << part.v()[1] << " "
           << part.rho() << " "
           << part.p() << " "
           << part.force()[0] << " "
           << part.force()[1] << " "
           << part.densityRate() << "\n";
    return stream.str();
  }

  void OutputParticles(const ParticleHandler& handler, int n_file,
                       int step, int time, const RunParams& params) {
    const std::vector<Particle>& part{handler.particles()};
    std::stringstream header ;
    header << "# BLUSH SPH particle position output\n"
           << "# Time: " << time << " Step: " << step << "\n"
           << "# x y vx vy density pressure forcex forcey densityrate\n" << std::endl;

    {
      std::string file_name = OutputFileName(n_file, "out_fluid_", params);
      std::ofstream file (file_name);

      file << header.str();
      for (const auto& it : part) {
        if (it.type() == ParticleType::Fluid)
          file << PrintParticle(it);
      }
      file.close();
    }

    {
      std::string file_name = OutputFileName(n_file, "out_bound_", params);
      std::ofstream file (file_name);

      file << header.str();
      for (const auto& it : part) {
        if (it.type() == ParticleType::Boundary)
          file << PrintParticle(it);
      }
      file.close();
    }

    {
      std::string file_name = OutputFileName(n_file, "out_cells_", params);
      std::ofstream file (file_name);
      file << "# index bottomleftcorner_x y size_x y hasContained numContained"
        "\n";
      for (const auto& it : handler.cells())
        file << it << "\n";
      file.close();
    }
  }

  // Move particles out of boundary to opposite side.
  // Could be made more efficient by only treating particles in edge cells.
  void LoopParticlesPeriodic(std::vector<Particle>& part,
                             const RunParams& params)
  {
    for (auto& it : part) {
      // Boundary particles do not move
      if (it.type() == ParticleType::Boundary)
        continue;

      // For each dimension (x,y)
      for (unsigned i=0; i<2; ++i) {
        double& pos = it.r()[i];
        const double& domainStart = params.domain[0][i];
        const double& domainEnd = params.domain[1][i];
        if (pos < domainStart)
          pos = domainEnd - (domainStart - pos);
        else if (pos > domainEnd)
          pos = domainStart + (pos - domainEnd);
      }
    }
  }

  void FlagParticlesOutsideDomain(std::vector<Particle>& part,
                                  const RunParams& params)
  {
    for (auto& it : part) {
      // Boundary particles do not move
      if (it.type() == ParticleType::Boundary)
        continue;

      // For each dimension (x,y)
      for (unsigned i=0; i<2; ++i) {
        double& pos = it.r()[i];
        const double& domainStart = params.domain[0][i];
        const double& domainEnd = params.domain[1][i];
        if (pos < domainStart || pos > domainEnd)
          it.removeParticle() = true;
      }
    }
  }

  void TreatParticlesLeavingDomain(std::vector<Particle>& part,
                                   const RunParams& params)
  {
    if (params.bound_type == Boundary::Periodic) {
      LoopParticlesPeriodic(part, params);
    }
    else if (params.bound_type == Boundary::Closed) {
      FlagParticlesOutsideDomain(part, params);
    }
  }

  void StepMonaghan(
      ParticleHandler& handler,
      const RunParams& params,
      double& dt) {
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid)
        it.force() = params.gravity;
    }

    // Calculate force at initial state. F(0)
    Interactor interactor(handler, params);
    interactor.update(InteractType::UpdateForce, false);

    double dt2 = 0.5f * dt;

    // Temporary storage of original velocities.
    std::map<std::size_t, Vec> vOrig;

    // Particle properties at half step.
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid) {
        // Store original velocities (would be more efficient to store the
        // new velocities to this array and keep the old array as it is,
        // but this is simpler for the moment)
        vOrig[it.n()] = it.v();
        // Position. r(1/2)=r(0)+dt/2*v(0)
        it.r() += dt2 * it.v();
        // Velocity. v(1/2)=v(0)+dt/2*F(0)
        it.v() += dt2 * it.force();
        // Density. rho(1/2)=rho(0)+dt/2*D(0)
        it.rho() += dt2 * it.densityRate();
        // Pressure. P(1/2)=f(rho(1/2))
        it.p() = pressureStd(it.rho(), params);
      }
    }

    TreatParticlesLeavingDomain(handler.particles(), params);
    // Check which cells the particles are now in.
    handler.update();
    // Calculate force at half step. F(1/2)
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid)
        it.force() = params.gravity;
    }

    interactor.update(InteractType::UpdateForce, true);
    double dt_new = interactor.dt();

    // Particle properties at full step.
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid) {
        // Velocity. v(1)=v(0)+dt*F(1/2)
        it.v() = vOrig[it.n()] + dt * it.force();
        // Position. r(1)=r(1/2)+dt/2*v(1)
        it.r() += dt2 * it.v();
      }
    }

    TreatParticlesLeavingDomain(handler.particles(), params);
    // Check which cells the particles are now in.
    handler.update();
    // Calculate density rate at end of step. D(1)
    interactor.update(InteractType::UpdateDensityRate, false);

    // Particle properties at full step.
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid) {
        // Density. rho(1)=rho(1/2)+dt/2*D(1)
        it.rho() += dt2 * it.densityRate();
        // Pressure. P(1)=f(rho(1))
        it.p() = pressureStd(it.rho(), params);
      }
    }

    // Set dt for next step.
    dt = dt_new;
  }

  void StepAdami(
      ParticleHandler& handler,
      const RunParams& params,
      double& dt) {
    double dt2 = 0.5f * dt;

    // Velocity and position at half step.
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid) {
        // Velocity. v(1/2)=v(0)+dt/2*F(0)
        it.v() += dt2 * it.force();
        // Position. r(1/2)=r(0)+dt/2*v(1/2)
        it.r() += dt2 * it.v();
      }
    }

    TreatParticlesLeavingDomain(handler.particles(), params);
    // Check which cells the particles are now in.
    handler.update();

    Interactor interactor(handler, params);
    // Calculate density rate at half step D(1/2).
    if (params.densityCalculation == DensityCalculation::Integrate)
      interactor.update(InteractType::UpdateDensityRate, false);
    else if (params.densityCalculation == DensityCalculation::Estimator)
      interactor.update(InteractType::UpdateDensity, false);

    // Density, pressure and position at full step.
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid) {
        // Density. rho(1)=rho(0)+dt*D(1/2)
        if (params.densityCalculation == DensityCalculation::Integrate)
          it.rho() = it.rho() + dt * it.densityRate();
        // Pressure. P(1)=f(rho(1))
        it.p() = pressureStd(it.rho(), params);
        // Position. r(1)=r(1/2)+dt/2*v(1/2)
        it.r() += dt2 * it.v();
      }
    }

    TreatParticlesLeavingDomain(handler.particles(), params);
    // Check which cells the particles are now in.
    handler.update();

    if (params.boundary_particles == BoundaryParticles::Adami) {
      // Update boundary particle velocities and pressures
      interactor.update(InteractType::UpdateBoundary, false);
    }

    // Force at full step F(1)(r(1),v(1/2),rho(1))
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid)
        it.force() = params.gravity;
    }

    interactor.update(InteractType::UpdateForce, true);
    double dt_new = interactor.dt();

    // Velocity and position at full step.
    for (auto& it : handler.particles()) {
      if (it.type() == ParticleType::Fluid) {
        // Velocity. v(1)=v(1/2)+dt/2*F(1)
        it.v() += dt2 * it.force();
      }
    }

    // Set dt for next step.
    dt = dt_new;
  }

  void SphStep(ParticleHandler& handler,
               const RunParams& params,
               double& dt) {
    if (params.integrator == Integrator::Monaghan)
      StepMonaghan(handler, params, dt);
    else if (params.integrator == Integrator::Adami)
      StepAdami(handler, params, dt);
  }
}

namespace blush {
  void Run(const Particles& particles, const RunParams& params) {
    auto handler = ParticleHandler(particles, params.domain, params.support,
                                   params.bound_type);

    double dt = 0;
    int step_count = 0;
    int output_count = 0;
    double max_speed = 0;
    int prev_step_count = 0;
    int max_output_count = params.time_run / params.time_output;
    bool speed_warning_shown = false;
    double average_dt = 0;
    time_t start_time = time(NULL);
    std::size_t nParticles = std::distance(std::begin(handler.particles()),
                                           std::end(handler.particles()));
    double pressureMin{100};
    bool pressureWarningShown{false};

    // Adami, 2012 time integrator requires persistent force arrays.
    // Forces in x and y direction.
    for (auto& it: handler.particles())
      if (it.type() == ParticleType::Fluid)
        it.force() = {0,0};

    for (double simtime = 0; ; simtime += dt) {
      // Get max speed for current step, max is reset for every new output file.
      double max_speed_current = maxSpeed(handler.particles().begin(),
                                         handler.particles().end(),
                                         ParticleType::Fluid);
      if (max_speed_current > max_speed)
        max_speed = max_speed_current;
      if (max_speed > params.v_max && !speed_warning_shown) {
        std::cout << "Warning: Maximum particle speed has exceeded assumed "
                  << "maximum, results may be\ninvalid. Suggest rerun with "
                  << "higher v_max."
                  << std::endl;
        speed_warning_shown = true;
      }

      SphStep(handler, params, dt);

      // Keep track of minimum pressure until next output file
      for (auto &it : handler.particles()) {
        if (it.type() != ParticleType::Fluid)
          continue;
        if (it.p() < pressureMin)
          pressureMin = it.p();
      }

      if (pressureMin <= 0.0 && !pressureWarningShown) {
        std::cout << "Warning: Pressure in fluid has dropped below zero. "
                  << "Suggest rerun with larger\nbackground pressure."
                  << std::endl;
        pressureWarningShown = true;
      }

      // Update average timestep
      average_dt = (dt + step_count * average_dt) / (step_count+1);

      ++step_count;

      if (simtime >= params.time_output * output_count) {

        time_t current_time = time(NULL);
        double runtime = difftime(current_time, start_time);
        double estimated_steps_remaining = (params.time_run - simtime) / average_dt;
        int estimated_total_runtime = runtime / step_count
          * (step_count + estimated_steps_remaining);
        int estimated_remaining_runtime = runtime / step_count
          * estimated_steps_remaining;
        if (output_count == 0) {
          estimated_total_runtime = 0;
          estimated_remaining_runtime = 0;
        }

        std::cout.precision(3);
        std::cout << std::scientific;
        std::cout << std::setw(4) << output_count
                  << std::setw(6) << step_count - prev_step_count
                  << std::setw(8) << step_count
                  << std::setw(10) << dt
                  << std::setw(10) << max_speed
                  << std::setw(10) << pressureMin
                  << std::setw(10) << simtime
                  << std::setfill('0')
                  << " "
                  << std::setw(2) << (estimated_total_runtime/86400) << ":"
                  << std::setw(2) << (estimated_total_runtime/3600) % 24 << ":"
                  << std::setw(2) << (estimated_total_runtime/60) % 60
                  << " "
                  << std::setw(2) << (estimated_remaining_runtime/86400) << ":"
                  << std::setw(2) << (estimated_remaining_runtime/3600) % 24 << ":"
                  << std::setw(2) << (estimated_remaining_runtime/60) % 60
                  << std::setfill(' ')
                  << std::endl;

        OutputParticles(handler, output_count, step_count, simtime, params);

        std::size_t currentNParticles =
          std::distance(std::begin(handler.particles()),
                        std::end(handler.particles()));
        if (currentNParticles < nParticles) {
          std::cout << nParticles - currentNParticles << " particles removed."
                    << std::endl;
          nParticles = currentNParticles;
        }

        if (output_count == max_output_count) {
          if (params.init_mode) {
            std::ifstream  src(OutputFileName(output_count, "out_fluid_", params),
                               std::ios::binary);
            std::ofstream  dst(params.init_file,   std::ios::binary);

            dst << src.rdbuf();
          }

          break;
        }

        ++output_count;
        max_speed = 0;
        prev_step_count = step_count;
      }
    }
  }
}
