#include "particlehandler.hpp"

#include "fwd.hpp"
#include "particle.hpp"
#include "vec.hpp"

#include <eigen3/Eigen/Dense>

#include <algorithm>
#include <cassert>
#include <iterator>
#include <set>
#include <tuple>

namespace
{
  using namespace blush;

  Vec domainSize(const Region& domain)
  {
    assert((domain[0][0] < domain[1][0] && domain[0][1] < domain[1][1]) &&
           "Start of domain is at larger coordinates than end of domain");
    return domain[1] - domain[0];
  }

  ParticleHandler::CellCoords cellCoordinatesFromIndex(std::size_t index,
                                      std::size_t height)
  {
    return {index / height, index % height};
  }

  std::size_t cellIndexFromCoordinates(ParticleHandler::CellCoords coordinates,
                                       std::size_t height)
  {
    return coordinates(0) * height + coordinates(1);
  }

  // Wrap cell over domain boundary if it is out of bounds and return
  // the direction of wrapping
  std::tuple<ParticleHandler::CellCoords, Loop> loopCell(
      ParticleHandler::CellCoords cell,
      ParticleHandler::CellCoords cellGridDimensions)
  {
    // Default to same cell coordinates and no periodic looping
    ParticleHandler::CellCoords loopedCell = cell;
    Loop loop {{NeighborLoop::None, NeighborLoop::None}};
    // For each coordinate, if cell is 1 under/over domain min/max
    // then shift to the cell on the opposite side and record the
    // direction of the shift (loop).
    for (unsigned i=0; i<2; ++i) {
      if (cell(i) == -1) {
        loopedCell(i) = cellGridDimensions[i] - 1;
        loop.at(i) = NeighborLoop::Backward;
      }
      else if (cell(i) == cellGridDimensions[i]) {
        loopedCell(i) = 0;
        loop.at(i) = NeighborLoop::Forward;
      }
    }
    return std::make_tuple(loopedCell, loop);
  }

  bool isCellLooped(Loop loop)
  {
    for (const auto &it : loop) {
      if (it == NeighborLoop::Backward || it == NeighborLoop::Forward)
        return true;
    }
    return false;
  }
}

namespace blush
{
  //
  // HandlerCell class
  //

  HandlerCell::HandlerCell(
      const std::size_t index, const Vec& cornerPosition, const Vec& size)
    : index_{index},
      cornerPosition_{cornerPosition},
      size_{size}
  { }

  void HandlerCell::setNumContained(int n)
  {
    numContained_ = n;
    // Statement broken up in this way to (hopefully) improve branch prediction
    if (hasContained_ == false) {
      if (n > 0)
        hasContained_ = true;
    }
  }

  std::ostream& operator<<(std::ostream& os, const HandlerCell& cell)
  {
    os << cell.index_ << " "
       << cell.cornerPosition_[0] << " " << cell.cornerPosition_[1] << " "
       << cell.size_[0] << " " << cell.size_[1] << " "
       << cell.hasContained_ << " " << cell.numContained_;

    return os;
  }

  //
  // ParticleHandler class
  //

  Particle& ParticleHandler::lookup(std::size_t n)
  {
    auto particle = std::find_if(particles_.begin(), particles_.end(),
                                 [n](const Particle p)
                                 {
                                   return p.n() == n;
                                 });
    assert(particle != std::end(particles_) &&
           "Looked up particle does not exist in handler");
    return *particle;
  }

  bool ParticleHandler::contains(std::size_t n)
  {
    auto particle = std::find_if(std::begin(particles_), std::end(particles_),
                                 [n](const Particle p)
                                 {
                                   return p.n() == n;
                                 });
    return (particle != std::end(particles_));
  }

  void ParticleHandler::update()
  {
    removeFlaggedParticles();
    updateContainingCells();
    sort();
    updateCells();
  }

  void ParticleHandler::insert(Particle particle)
  {
    particles_.push_back(particle);
    particles_.back().n() = index_;
    ++index_;
  }

  // point iterators in each cell to the correct particle range
  void ParticleHandler::updateCells()
  {
    Particles::iterator first = particles_.begin();
    Cells::iterator previousCell;
    std::size_t iCell=0;
    bool isFirst = true;
    // for each cell iCell
    for (auto it = cells_.begin(); it != cells_.end(); ++it) {
      // find first particle in sorted array of particles which is in cell iCell
      it->begin_ = std::lower_bound(
          first, particles_.end(), iCell,
          [](const Particle& left, std::size_t right)
          {
            return left.cell() < right;
          });
      // end of the previous cell is the start of the current cell, do
      // not apply to first cell as there is no previous cell
      if (isFirst)
        isFirst = false;
      else
        previousCell->end_ = it->begin_;

      // move to next cell, search can now begin at the current cell start
      ++iCell;
      first = it->begin_;
      previousCell = it;
    }
    // last cell ends at end of particle array
    cells_.back().end_ = particles_.end();

    // update cell statistics
    for (auto &it : cells_) {
      it.setNumContained(std::distance(it.begin_, it.end_));
    }
  }

  void ParticleHandler::updateContainingCells()
  {
    for (auto &it : particles_) {
      // Get containing cell number from particle position
      it.cell()
        = static_cast<int>((it.r()[0]-domainOrigin[0]) / cellDimensions[0])
        * cellGridDimensions[1]
        + static_cast<int>((it.r()[1]-domainOrigin[1]) / cellDimensions[1]);
    }
  }

  void ParticleHandler::sort()
  {
    std::sort(particles_.begin(), particles_.end(),
              [](const Particle& left, const Particle& right)
              {
                return left.cell() < right.cell();
              });
  }

  void ParticleHandler::removeFlaggedParticles()
  {
    particles_.erase(
        std::remove_if(particles_.begin(), particles_.end(),
                       [](const Particle& p)
                       {
                         return p.removeParticle();
                       }),
        particles_.end());
  }

  ParticleHandler::CellCoords ParticleHandler::initCellGridDimensions
  (const Region& domain, double support)
  {
    Vec size = domainSize(domain);
    assert(size[0]>0 && size[1]>0 && "Domain size cannot be less than"
      "zero for distance calculation");
    return {static_cast<int>(size[0] / support),
        static_cast<int>(size[1] / support)};
  }

  Vec ParticleHandler::initCellDimensions (const Region& domain) const
  {
    Vec size = domainSize(domain);
    // Cell lengths increased by smallest representable double to ensure
    // that (cell length) * (number of cells) is larger than the domain
    // length, this ensures that all particles will be allocated to a
    // cell. Without this, particles very close to the domain edge can
    // leak out of all representable cells.
    return Vec{std::nextafter((size[0] / cellGridDimensions[0]),
                              std::numeric_limits<double>::max()),
        std::nextafter((size[1] / cellGridDimensions[1]),
                       std::numeric_limits<double>::max())};
  }

  void ParticleHandler::initCells(Boundary boundType)
  {
    // number of cells
    const auto nCell = static_cast<std::size_t>(
        cellGridDimensions[0] * cellGridDimensions[1]);
    // fill array with empty cells
    for (std::size_t i{0}; i<nCell; ++i) {
      const CellCoords coords{
        cellCoordinatesFromIndex(i, cellGridDimensions[1])};
      const Vec cornerPosition{domainOrigin[0] + coords[0]*cellDimensions[0],
          domainOrigin[1] + coords[1]*cellDimensions[1]};
      cells_.push_back(
          HandlerCell{i, cornerPosition, cellDimensions});
    }
    // cell neighbors are located in 4 out of 8 directions. Only half are
    // needed because interactions are pairwise.
    const std::vector<CellCoords> offset = {{-1,-1},
                                            { 0,-1},
                                            { 1,-1},
                                            { 1, 0}};
    // set neighbors of all cells
    for (std::size_t iCell = 0; iCell < nCell; ++iCell) {
      const CellCoords baseCell {
        cellCoordinatesFromIndex(iCell, cellGridDimensions[1])};
      CellNeighbors& neighbors {cells_.at(iCell).neighbors_};
      assert(neighbors.size() == 0
             && "Cells must have no neighbors before initialization");
      // offset each cell to check where neighbors are and make neighbor list
      for (const auto &itOffset : offset) {
        // get position of neighbor cell by offsetting,
        const CellCoords offsetCell {baseCell + itOffset};
        // check if the cell is over domain edge and shift it to the
        // other side if using periodic boundaries
        CellCoords loopedCell {0,0};
        Loop loopDirections {{NeighborLoop::None, NeighborLoop::None}};
        std::tie(loopedCell, loopDirections) =
          loopCell(offsetCell, cellGridDimensions);

        // closed boundaries do not need interactions across boundary
        if (boundType == Boundary::Closed && isCellLooped(loopDirections))
          continue;

        const std::size_t loopedCellIndex {cellIndexFromCoordinates(
              loopedCell, cellGridDimensions[1])};
        neighbors.push_back(
            CellNeighbor{&cells_.at(loopedCellIndex), loopDirections});
      }
    }
  }

  std::size_t ParticleHandler::initIndex(const Particles& aParticles)
  {
    const auto& max = std::max_element(
        aParticles.begin(), aParticles.end(),
        [](const Particle& left, const Particle& right)
        {
          return left.n() < right.n();
        });
    return max->n() + 1;
  }

  void ParticleHandler::validateParticles()
  {
    // Check all particle indices are unique
    std::set<unsigned> numbers;
    for (auto& it : particles_) {
      bool notDuplicate = numbers.insert(it.n()).second;
      assert(notDuplicate && "Duplicate particle index in vector of particles \
supplied to particleHandler");
    }
  }
}
