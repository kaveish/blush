#ifndef KERNEL_HPP
#define KERNEL_HPP

#include "fwd.hpp"

namespace blush
{
  // 1D kernels
  double WWendland1DCubic(double dist_sq, const RunParams& params);
  double WGradWendland1DCubic(double dist_sq, const RunParams& params);

  double WWendland1DC2(double dist_sq, const RunParams& params);
  double WGradWendland1DC2(double dist_sq, const RunParams& params);

  double WWendland1DC4(double dist, const RunParams& params); // using
  double WGradWendland1DC4(double dist, const RunParams& params);

  // 2D kernels
  double WCubic2D(double dist, double dist_sq, const RunParams& params);
  double WGradCubic2D(double dist, double dist_sq, const RunParams& params);

  double WWendland2DC2(double dist_sq, const RunParams& params); // using
  double WGradWendland2DC2(double dist_sq, const RunParams& params); // using
}

#endif
