#include "vec.hpp"

#include <iostream>

namespace blush
{
  std::ostream& operator<<(std::ostream& os, const Vec& v)
  {
    os << "(" << v[0] << "," << v[1] << ")";
    return os;
  }
}
