#include "interact.hpp"

#include "equations.hpp"
#include "fwd.hpp"
#include "kernel.hpp"
#include "logger.hpp"
#include "particle.hpp"
#include "particlehandler.hpp"
#include "particleutils.hpp"
#include "runparams.hpp"
#include "vec.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>

using namespace blush;

namespace {
  void interact(Interactor::InteractType type, ParticleHandler& handler,
                const RunParams& params);

  bool isFluid(Particle& p) { return p.type() == ParticleType::Fluid; }
  bool isBoundary(Particle& p) { return p.type() == ParticleType::Boundary;}
  bool isFloating(Particle& p) { return p.type() == ParticleType::Floating; }
  bool isSolid(Particle& p) { return (isBoundary(p) || isFloating(p)); }

  Vec distanceStandard(const Particle& partA, const Particle& partB)
  {
    return partA.r() - partB.r();
  }

  Vec distancePeriodic(const Particle& partA, const Particle& partB,
                       const Loop& loop, const RunParams& params)
  {
    const Vec& rA = partA.r();
    const Vec& rB = partB.r();
    Vec rAB;

    for (std::size_t i=0; i<2; ++i) {
      if (loop[i] == NeighborLoop::None)
        rAB[i] = rA[i] - rB[i];
      else if (loop[i] == NeighborLoop::Backward)
        rAB[i] = rA[i] - (rB[i] - params.domainSize[i]);
      else //if (loop[i] == NeighborLoop::Forward)
        rAB[i] = rA[i] - (rB[i] + params.domainSize[i]);
    }

    return rAB;
  }

  Vec distance(const Particle& partA, const Particle& partB,
               const Loop& loop, const RunParams& params)
  {
    Vec dist;
    if (params.bound_type == Boundary::Periodic)
      dist = distancePeriodic(partA, partB, loop, params);
    else if (params.bound_type == Boundary::Closed)
      dist = distanceStandard(partA, partB);
    return dist;
  }

  void reverseLoop(Loop& loop)
  {
    for (std::size_t i=0; i<2; ++i) {
      if (loop[i] == NeighborLoop::Backward)
        loop[i] = NeighborLoop::Forward;
      else if (loop[i] == NeighborLoop::Forward)
        loop[i] = NeighborLoop::Backward;
    }
  }

  struct TimestepTmps
  {
    static double dtbTmp;
  };

  double TimestepTmps::dtbTmp;

  template< class ForwardIt >
  double maxSoundSpeed(ForwardIt first, ForwardIt last)
  {
    return std::max_element(
        first, last,
        [](const Particle& left, const Particle& right)
        {
          return left.c() < right.c();
        })->c();
  }

  // Only take maximum over particles of a given type
  template< class ForwardIt >
  double maxSoundSpeed(ForwardIt first, ForwardIt last, ParticleType type)
  {
    return std::max_element(
        first, last,
        [type](const Particle& left, const Particle& right)
        {
          return (right.type() == type)
            && (left.c() < right.c());
        })->c();
  }

  // Temp
  // template< class ForwardIt >
  // unsigned maxSpeedN(ForwardIt first, ForwardIt last, ParticleType type)
  // {
  //   Particle* p = &(*std::max_element(
  //       first, last,
  //       [type](const Particle& left, const Particle& right)
  //       {
  //         return (right.type() == type)
  //         && (particleSpeed(left) < particleSpeed(right));
  //       }));
  //   return p->n();
  // }

  void updateBoundary(ParticleHandler& handler, const RunParams& params)
  {
    // Reset pressures, velocities and summations of boundary particles
    for (auto& it : handler.particles()) {
      if (!isSolid(it))
        continue;
      it.p() = 0;
      it.v() = {0,0};
      it.kernelSum() = 0;
      it.densityPositionSum() = {0,0};
    }

    interact(Interactor::InteractType::UpdateBoundary, handler, params);

    // Divide all velocity and pressure summation by kernel sum
    for (auto& it : handler.particles()) {
      if (!isSolid(it))
        continue;

      if (it.kernelSum() == 0) {
        it.v() = {0,0};
        it.p() = 0;
      }
      else {
        it.v() = it.v() / it.kernelSum();
        it.p() = (it.p() + params.gravity.dot(it.densityPositionSum()))
                  / it.kernelSum();
      }
    }

    // For constant density fluid, density depends on pressure of boundary
    // particle
    for (auto& it : handler.particles()) {
      if (!isSolid(it))
        continue;
      // rho_b = rho_0((p_d - chi)/p_0 + 1)^(1/gamma)
      double term = (it.p() - params.background_pressure)
        / params.pressure0 + 1;
      it.rho() = params.density0 * pow(term, 1.f/7.f);
    }

    if (params.test_pois_continued_boundary) {
      for (auto& it : handler.particles()) {
        if (!isSolid(it))
          continue;

        double dp{params.h/1.5};
        double L{1 - dp};
        double r{it.r()[1]-(1.0+dp/2.0)};
        it.v()[0] = -0.1/(2*0.01)*r*(r-L);
        it.v()[1] = 0;
      }
    }

    if (params.test_zero_velocity_boundary) {
      for (auto& it : handler.particles()) {
        if (!isSolid(it))
          continue;

        it.v() = {0,0};
      }
    }

    if (params.test_const_pressure_boundary) {
      for (auto& it : handler.particles()) {
        if (!isSolid(it))
          continue;

        it.p() = params.pressure0;
        it.rho() = params.density0;
      }
    }
  }

  void updateDensityRate(ParticleHandler& handler, const RunParams& params)
  {
    for (auto& it: handler.particles()) {
      if (isFluid(it))
        it.densityRate() = 0;
    }

    interact(Interactor::InteractType::UpdateDensityRate, handler, params);

    if (params.continuity_eqn == ContinuityEquation::Adami) {
      // Multiply summation by own density
      for (auto& it: handler.particles()) {
        if (isFluid(it))
          it.densityRate() = it.densityRate() * it.rho();
      }
    }
  }

  void updateForce(ParticleHandler& handler, const RunParams& params,
                   bool updateDt, double& dt)
  {
    TimestepTmps dtTmps;// Find min of dtb_tmp=r-d, want min((r-d)/sqrt(K))
    dtTmps.dtbTmp = 1e4; // Arbitrary large value

    // Sound speeds for particles. Precalculated to avoid
    // recalculating for each interaction. Also used to calculate time
    // step.
    for (auto& it : handler.particles())
      it.c() = soundSpeed(it.rho(), params);

    for (auto& it : handler.particles()) {
      if (isFluid(it)) {
        it.forceTermTmp() = it.p() / (it.rho() * it.rho());
      }
    }

    interact(Interactor::InteractType::UpdateForce, handler, params);

    // Damps motion during initialisation to allow particles to settle.
    if (params.init_mode) {
      for (auto& it : handler.particles()) {
        if (isFluid(it))
          it.force() += -params.init_damping * it.v();
      }
    }

    // Note: other time step code should be removed when not needed.
    if (updateDt) {
      if (params.integrator == Integrator::Monaghan) {
        // Time step dtc
        // Find max of dtc_tmp=sound speed, want min(dtc=h/(2c))
        double dtcTmp = maxSoundSpeed(std::begin(handler.particles()),
                                     std::end(handler.particles()),
                                     ParticleType::Fluid);
        double dtc = params.h / (2 * dtcTmp);

        // Time step dtb
        double dtb = dtTmps.dtbTmp * params.timestep_oversqrtk;

        // Final time step
        if (dtb < dtc) {
          dt = dtb;
          // test
          if (Logger::timestepProximityWarning) {
            Logger::timestepProximityWarning = false;
            std::cout << "Timestep is now dtb<dtc, particles may be too close to"
              " boundary" << std::endl;
          }
        }
        else {
          dt = dtc;
          // test
          if (!Logger::timestepProximityWarning) {
            Logger::timestepProximityWarning = true;
            std::cout << "Timestep is now dtc<dtb." << std::endl;
          }
        }
      }
      else if (params.integrator == Integrator::Adami) {
        double dtaMaxSound = maxSoundSpeed(std::begin(handler.particles()),
                                          std::end(handler.particles()),
                                          ParticleType::Fluid);
        double dtaMaxSpeed = maxSpeed(std::begin(handler.particles()),
                                     std::end(handler.particles()),
                                     ParticleType::Fluid);
        double dta = 0.25*params.h/(dtaMaxSound+dtaMaxSpeed);

        dt = std::min({dta, params.adami_dtb, params.adami_dtc});
        // unsigned n = maxSpeedN(std::begin(handler.particles()),
        //                        std::end(handler.particles()),
        //                        ParticleType::Fluid);
        //std::cout << dtaMaxSound << std::endl << dtaMaxSpeed << std::endl << dta << std::endl << params.adami_dtb << std::endl << params.adami_dtc << std::endl;
        //std::cout << handler.lookup(n) << std::endl;
      }
    }
  }

  void updateDensity(ParticleHandler& handler, const RunParams& params)
  {
    for (auto& it: handler.particles()) {
      if (isFluid(it))
        // Start sum with mass*w for own particle = mass
        it.rho() = WWendland2DC2(0.0, params) * params.mass;
    }

    interact(Interactor::InteractType::UpdateDensity, handler, params);
  }

  void interactBoundary(Particle& particle1, Particle& particle2,
                        Loop loop, const RunParams& params)
  {
    if (!((isFluid(particle1) && isSolid(particle2))
          || (isFluid(particle2) && isSolid(particle1)))) {
      return;
    }

    Particle* pFluid;
    Particle* pSolid;
    if (isFluid(particle1) && isSolid(particle2)) {
      pFluid = &particle1;
      pSolid = &particle2;
    }
    else { // (isFluid(particle2) && isSolid(particle1))
      pFluid = &particle2;
      pSolid = &particle1;
      // If calculations are done with pLeft = p2, then rAB will be
      // calculated with respect to the particle in the neighbour
      // cell, not the origin cell. This means any periodic boundary
      // looping needs to be in the opposite direction.
      reverseLoop(loop);
    }

    Vec rAB = distance(*pFluid, *pSolid, loop, params);
    double dist_sq = rAB.squaredNorm();
    if (dist_sq >= params.h4_sq) return;

    double dist = std::sqrt(dist_sq);
    double kernel = WWendland2DC2(dist, params);

    pSolid->v() -= kernel * pFluid->v();
    pSolid->p() += kernel * pFluid->p();

    pSolid->kernelSum() += kernel;

    double density_kernel = pFluid->rho() * kernel;
    pSolid->densityPositionSum() += density_kernel * rAB;
  }

  void interactDensityRate(Particle& particle1, Particle& particle2,
                           Loop loop, const RunParams& params)
  {
    // No solid-solid density update
    if (isSolid(particle1) && isSolid(particle2))
      return;

    bool isFluidSolidInteraction = false;
    Particle* pFluid{&particle1};
    Particle* pSolid{&particle2};
    if (isSolid(particle1) || isSolid(particle2)) {
      isFluidSolidInteraction = true;
      if (!(isFluid(particle1) && isSolid(particle2))) {
        pFluid = &particle2;
        pSolid = &particle1;
        // If calculations are done with pLeft = p2, then rAB will be
        // calculated with respect to the particle in the neighbour
        // cell, not the origin cell. This means any periodic boundary
        // looping needs to be in the opposite direction.
        // TODO: Not needed because rAB goes p1->p2 which is the correct direction
        //reverseLoop(loop);
      }
    }

    Vec rAB = distance(particle1, particle2, loop, params);
    // TODO: rewrite function using pLeft pRight so this hack is not necessary.
    // Hack: reverse sign on rAB if solid particle is p1.
    if (isSolid(particle1)) {
      rAB[0] = -rAB[0];
      rAB[1] = -rAB[1];
    }

    double dist_sq = rAB.squaredNorm();
    if (dist_sq >= params.h4_sq) return;

    // Distance
    double dist = std::sqrt(dist_sq);

    // Kernel gradient.
    Vec wGrad = (WGradWendland2DC2(dist, params) / dist) * rAB;

    if (params.continuity_eqn == ContinuityEquation::Monaghan) {
      // Continuity: Monaghan, Boundary: Monaghan
      if (params.boundary_particles == BoundaryParticles::Monaghan) {

        if (isFluidSolidInteraction) {
          Vec vAB = pFluid->v() - pSolid->v();
          double densityRate = params.mass * vAB.dot(wGrad);
          pFluid->densityRate() += densityRate;
        }
        else {
          Vec vAB = particle1.v() - particle2.v();
          double densityRate = params.mass * vAB.dot(wGrad);
          particle1.densityRate() += densityRate;
          particle2.densityRate() += densityRate;
        }
      }
      // Continuity: Monaghan, Boundary: Adami
      else if (params.boundary_particles == BoundaryParticles::Adami) {
        std::cout << "Error: Adami boundary particles with Monaghan continuity"
          "eqn not implemented" << std::endl;
        std::exit(EXIT_FAILURE);
      }
    }
    else if (params.continuity_eqn == ContinuityEquation::Adami) {
      // Continuity: Adami, Boundary: Monaghan
      if (params.boundary_particles == BoundaryParticles::Monaghan) {
        if (isFluidSolidInteraction) {
          Vec vAB = pFluid->v() - pSolid->v();
          double densityRate = params.mass/pSolid->rho() * vAB.dot(wGrad);
          pFluid->densityRate() += densityRate;
        }
        else {
          // TODO check signs work out OK (+/-)
          Vec vAB = particle1.v() - particle2.v();
          double densityRate = params.mass/particle2.rho() * vAB.dot(wGrad);
          particle1.densityRate() += densityRate;
          particle2.densityRate() += densityRate;
        }
      }
      // Continuity: Adami, Boundary: Adami
      else if (params.boundary_particles == BoundaryParticles::Adami) {
        if (isFluidSolidInteraction) {
          double densityRate = params.volume0 * pFluid->v().dot(wGrad);
          pFluid->densityRate() += densityRate;
        }
        else {
          Vec vAB = particle1.v() - particle2.v();
          double densityRate = params.mass * vAB.dot(wGrad);
          particle1.densityRate() += densityRate / particle2.rho();
          particle2.densityRate() += densityRate / particle1.rho();
        }
      }
    }
  }

  void interactForceSolidMonaghan(Particle& particle1, Particle& particle2,
                                  const Loop& loop, const RunParams& params)
  {
    Vec rAB = distance(particle1, particle2, loop, params);
    double dist_sq = rAB.squaredNorm();
    if (dist_sq >= params.h4_sq) return;

    // DEBUG: Warn if particles are on top of eachother
    if (dist_sq == 0)
      std::cout << "WARNING: Multiple particles with same coordinates at "
                << particle1.r() <<  std::endl;

    // Distance
    double dist = sqrt(dist_sq);

    // Kernel gradient.
    Vec wGrad = (WGradWendland2DC2(dist, params) / dist) * rAB;

    // Pa/rhoa^2 + Pb/rhob^2
    double force_term_tmp = particle1.forceTermTmp() + particle2.forceTermTmp();

    // Tensile correction (Monaghan, 2000)
    double kernel = WWendland2DC2(dist, params);
    // Tensile
    double tensile_f = kernel * params.tensile_kernel_dp_div;
    tensile_f *= tensile_f; // tensile_f^2
    tensile_f *= tensile_f; // tensile_f^4
    double tensile = params.tensile_epsilon * tensile_f;
    double tensile_r;
    if (particle1.p() > 0 && particle2.p() > 0) {
      tensile_r = 0.01 * force_term_tmp;
    }
    else {
      tensile_r = 0;
      if (particle1.p() < 0) {
        tensile_r -= particle1.forceTermTmp();
      }
      if (particle2.p() < 0)
        tensile_r -= particle1.forceTermTmp();
    }
    tensile *= tensile_r;

    Vec vAB = particle1.v() - particle2.v();
    // (Monaghan 2009)
    double forcePi = - params.force_alpha * (particle1.c() + particle2.c())
      * vAB.dot(rAB) / ((particle1.rho() + particle2.rho()) * dist);
    double forceTerm = params.mass * (force_term_tmp + forcePi + tensile);
    Vec force = forceTerm * wGrad;
    particle1.force() += - force;
    particle2.force() += force;
  }

  void interactForceFluidMonaghan(Particle& particle1, Particle& particle2,
                                  const Loop& loop, const RunParams& params)
  {
    Vec rAB = distance(particle1, particle2, loop, params);
    double dist_sq = rAB.squaredNorm();
    if (dist_sq >= params.h4_sq) return;

    // DEBUG: Warn if particles are on top of eachother
    if (dist_sq == 0)
      std::cout << "WARNING: Multiple particles with same coordinates at "
                << particle1.r() <<  std::endl;

    // Distance
    double dist = sqrt(dist_sq);

    // Kernel gradient.
    Vec wGrad = (WGradWendland2DC2(dist, params) / dist) * rAB;

    // Pa/rhoa^2 + Pb/rhob^2
    double force_term_tmp = particle1.forceTermTmp() + particle2.forceTermTmp();

    // Tensile correction (Monaghan, 2000).
    // Kernel.
    double kernel = WWendland2DC2(dist, params);
    // Tensile.
    double tensile_f = kernel * params.tensile_kernel_dp_div;
    tensile_f *= tensile_f; // tensile_f^2
    tensile_f *= tensile_f; // tensile_f^4
    double tensile = params.tensile_epsilon * tensile_f;
    double tensile_r;
    if (particle1.p() > 0 && particle2.p() > 0) {
      tensile_r = 0.01 * force_term_tmp;
    }
    else {
      tensile_r = 0;
      if (particle1.p() < 0) {
        tensile_r -= particle1.forceTermTmp();
      }
      if (particle2.p() < 0)
        tensile_r -= particle1.forceTermTmp();
    }
    tensile *= tensile_r;

    Vec vAB = particle1.v() - particle2.v();
    // (Monaghan 2009)
    double forcePi = - params.force_alpha * (particle1.c() + particle2.c())
      * vAB.dot(rAB) / ((particle1.rho() + particle2.rho()) * dist);
    double forceTerm = params.mass * (force_term_tmp + forcePi + tensile);
    Vec force = forceTerm * wGrad;
    particle1.force() += - force;
    particle2.force() += force;
  }

  void interactForceAdami(Particle& particle1, Particle& particle2,
                          Loop loop, const RunParams& params)
  {
    bool isFluidSolidInteraction = false;
    bool solidIsFloating = false;
    Particle* pFluid;
    Particle* pSolid;
    Particle* partLeft;
    Particle* partRight;
    if (isSolid(particle1) || isSolid(particle2)) {
      isFluidSolidInteraction = true;
      if (isFluid(particle1) && isSolid(particle2)) {
        pFluid = &particle1;
        pSolid = &particle2;
      }
      else {
        pFluid = &particle2;
        pSolid = &particle1;
        // If calculations are done with pLeft = p2, then rAB will be
        // calculated with respect to the particle in the neighbour
        // cell, not the origin cell. This means any periodic boundary
        // looping needs to be in the opposite direction.
        reverseLoop(loop);
      }

      if (pFluid->type() == ParticleType::Floating)
        solidIsFloating = true;

      partLeft = pFluid;
      partRight = pSolid;
    }
    else {
      partLeft = &particle1;
      partRight = &particle2;
    }

    Vec rAB = distance(*partLeft, *partRight, loop, params);
    double distance_sq = rAB.squaredNorm();
    if (distance_sq >= params.h4_sq) return;

    // DEBUG: Warn if particles are on top of eachother
    if (distance_sq == 0)
      std::cout << "WARNING: Multiple particles with same coordinates at "
                << particle1.r() <<  std::endl;

    double distance = std::sqrt(distance_sq);

    // Kernel derivative (dW/dr)
    double dW = WGradWendland2DC2(distance, params);

    // (1/rho_a^2+1/rho_b^2)
    double A = 1/(partLeft->rho()*partLeft->rho())
      + 1/(partRight->rho()*partRight->rho());

    // rho_a + rho_b
    double B = partLeft->rho() + partRight->rho();

    // ~Pab=(rhob*Pa+rhoa*Pb)/(rhoa+rhob)
    double Pab =
      (partRight->rho()*partLeft->p() + partLeft->rho()*partRight->p()) / B;

    if (params.test_pressure_zero)
      Pab = 0;

    // va-vb
    Vec vAB = partLeft->v() - partRight->v();

    double piTerm = params.force_alpha * params.h
      * (partLeft->c() + partRight->c()) * vAB.dot(rAB)
      / (B * distance_sq);

    Vec etaTerm = A*params.eta_viscosity*vAB;

    // dv/dt=m*sum[A*eta*vab + rab*(piab-A*Pab)](dW/dr / |rab|)
    Vec force = params.mass * (etaTerm + rAB*(piTerm-A*Pab))
      * (dW/distance);

    partLeft->force() += force;
    if (!isFluidSolidInteraction || solidIsFloating)
      partRight->force() += - force;
  }

  void interactForceSolid(Particle& particle1, Particle& particle2,
                          const Loop& loop, const RunParams& params)
  {
    if (params.momentum_eqn == MomentumEquation::Monaghan)
      interactForceSolidMonaghan(particle1, particle2, loop, params);
    else if (params.momentum_eqn == MomentumEquation::Adami)
      interactForceAdami(particle1, particle2, loop, params);
  }

  void interactForceFluid(Particle& particle1, Particle& particle2,
                          const Loop& loop, const RunParams& params)
  {
    if (params.momentum_eqn == MomentumEquation::Monaghan)
      interactForceFluidMonaghan(particle1, particle2, loop, params);
    else if (params.momentum_eqn == MomentumEquation::Adami)
      interactForceAdami(particle1, particle2, loop, params);
  }

  void interactForce(Particle& particle1, Particle& particle2,
                     const Loop& loop, const RunParams& params)
  {
    if (isFluid(particle1) && isFluid(particle2)) {
      interactForceFluid(particle1, particle2, loop, params);
    }
    else if ((isFluid(particle1) && isSolid(particle2))
             || (isSolid(particle1) && isFluid(particle2))) {
      interactForceSolid(particle1, particle2, loop, params);
    }
    else if ((isBoundary(particle1) && isFloating(particle2))
             || (isFloating(particle1) && isBoundary(particle2))) {
      // Undefined behaviour (no interaction)
    }
    else if (isFloating(particle1) && isFloating(particle2)) {
      // Undefined behaviour (no interaction)
    }
    else if (isBoundary(particle1) && isBoundary(particle2)) {
      // Do nothing
    }
  }

  void interactDensity(Particle& particle1, Particle& particle2,
                       Loop loop, const RunParams& params)
  {
    // No solid-solid density update
    if (isSolid(particle1) && isSolid(particle2))
      return;

    bool isFluidSolidInteraction = false;
    Particle* pFluid;
    if (isSolid(particle1) || isSolid(particle2)) {
      isFluidSolidInteraction = true;
      if (isFluid(particle1) && isSolid(particle2)) {
        pFluid = &particle1;
      }
      else {
        pFluid = &particle2;
        // If calculations are done with pLeft = p2, then rAB will be
        // calculated with respect to the particle in the neighbour
        // cell, not the origin cell. This means any periodic boundary
        // looping needs to be in the opposite direction.
        // TODO: Not needed because rAB goes p1->p2 which is the correct direction
        //reverseLoop(loop);
      }
    }

    Vec rAB = distance(particle1, particle2, loop, params);
    // TODO: rewrite function using pLeft pRight so this hack is not necessary.
    // Hack: reverse sign on rAB if solid particle is p1.
    if (isSolid(particle1)) {
      rAB[0] = -rAB[0];
      rAB[1] = -rAB[1];
    }

    double dist_sq = rAB.squaredNorm();
    if (dist_sq >= params.h4_sq) return;

    // Distance
    double dist = std::sqrt(dist_sq);

    double w = WWendland2DC2(dist, params);

    double density = params.mass * w;

    if (isFluidSolidInteraction) {
      pFluid->rho() += density;
    }
    else {
      particle1.rho() += density;
      particle2.rho() += density;
    }
  }

  void interactPair(Interactor::InteractType type, Particle& particle1,
                    Particle& particle2, const Loop& loop,
                    const RunParams& params)
  {
    switch (type) {
    case Interactor::InteractType::UpdateBoundary:
      interactBoundary(particle1, particle2, loop, params);
      break;
    case Interactor::InteractType::UpdateDensityRate:
      interactDensityRate(particle1, particle2, loop, params);
      break;
    case Interactor::InteractType::UpdateForce:
      interactForce(particle1, particle2, loop, params);
      break;
    case Interactor::InteractType::UpdateDensity:
      interactDensity(particle1, particle2, loop, params);
      break;
    default:
      std::cout << "Unknown interaction, exiting" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  }

  void interact(Interactor::InteractType type, ParticleHandler& handler, const RunParams& params)
  {
#pragma omp parallel for
#pragma parallel
    for (auto it = handler.cells().begin(); it < handler.cells().end(); ++it) {
      // Interact with own cell
      for (auto itSelf = it->begin(); itSelf != it->end(); ++itSelf) {
        auto& self = *itSelf;
        auto itOther = itSelf;
        ++itOther;
        for (; itOther != it->end(); ++itOther) {
          auto& other = *itOther;
          // Interact
          interactPair(type, self, other,
                       Loop{NeighborLoop::None, NeighborLoop::None}, params);
        }
      }
      // Interact with neighbour cell
      for (auto& itNeigh : it->neighbors()) {
        for (auto& itSelf : *it) {
          for (auto& itOther : itNeigh.cell()) {
            // Interact - remember to depend on loop direction
            interactPair(type, itSelf, itOther, itNeigh.loop(), params);
          }
        }
      }
    }
  }
}

namespace blush
{
  void Interactor::update(Interactor::InteractType type, bool updateDt)
  {
    switch (type) {
    case InteractType::UpdateBoundary:
      updateBoundary(handler_, params_);
      break;
    case InteractType::UpdateDensityRate:
      updateDensityRate(handler_, params_);
      break;
    case InteractType::UpdateForce:
      updateForce(handler_, params_, updateDt, dt_);
      break;
    case InteractType::UpdateDensity:
      updateDensity(handler_, params_);
      break;
    default:
      std::cout << "Unknown interaction, exiting" << std::endl;
      std::exit(EXIT_FAILURE);
    }
  }
}
