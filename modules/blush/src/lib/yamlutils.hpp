#ifndef YAMLUTILS_HPP
#define YAMLUTILS_HPP

#include "boostgeometryutils.hpp"
#include "fwd.hpp"
#include "inputparams.hpp"
#include "runparams.hpp"
#include "vec.hpp"

#include "yaml-cpp/yaml.h"

#include <cassert>
#include <iostream>
#include <tuple>
#include <vector>

namespace YAML
{
  //
  // convert specialisations for blush types
  //

  // convert: Vec
  template<>
  struct convert<blush::Vec>
  {
    static Node encode(const blush::Vec& rhs)
    {
      Node node;
      node.push_back(rhs[0]);
      node.push_back(rhs[1]);
      return node;
    }

    static bool decode(const Node& node, blush::Vec& rhs)
    {
      if(!node.IsSequence() || node.size() != 2)
        return false;

      rhs[0] = node[0].as<double>();
      rhs[1] = node[1].as<double>();
      return true;
    }
  };

  // convert: Region
  template<>
  struct convert<blush::Region>
  {
    static Node encode(const blush::Region& rhs)
    {
      Node node;
      node.push_back("min");
      node.push_back("max");
      node["min"].push_back(rhs[0][0]);
      node["min"].push_back(rhs[0][1]);
      node["max"].push_back(rhs[1][0]);
      node["max"].push_back(rhs[1][1]);
      return node;
    }

    static bool decode(const Node& node, blush::Region& rhs)
    {
      if(!node.IsMap() || node.size() != 2)
        return false;

      blush::Region region{{
          {node["min"][0].as<double>(),
              node["min"][1].as<double>()},
            {node["max"][0].as<double>(),
                node["max"][1].as<double>()}
        }};

      // Malformed if minimum is greater than maximum
      if (region[0][0] > region[1][0]
          || region[0][1] > region[1][1]) {
        std::cout << "Error: YAML region minimum is greater than maximum, "
          "minimum should be the bottom left point, maximum the top right."
                  << std::endl;
        return false;
      }

      rhs = region;

      return true;
    }
  };

  // convert: Enum (base class, derive from and define mapping to
  // specialise for each enum)
  template<typename T>
  struct convertEnum
  {
    using Mapper = std::vector<std::pair<std::string,T>>;

    static Node encode(const T& rhs)
    {
      Node node;
      for (const auto &it : mapping) {
        if (rhs == it.second) {
          node = it.first;
          return node;
        }
      }
      assert(0 && "value of enum does not match any paired string, did \
you forget to update this \"convert\" struct when adding a new option?");
    }

    static bool decode(const Node& node, T& rhs)
    {
      if(!node.IsScalar())
        return false;

      for (const auto &it : mapping) {
        if (node.as<std::string>() == it.first) {
          rhs = it.second;
          return true;
        }
      }

      // else no match
      std::cout << "Error: value of YAML entry is not a valid option [";
      for (auto it=mapping.begin(); it!=mapping.end(); it++) {
        if (it != mapping.begin())
          std::cout << ", ";
        std::cout << it->first;
      }
      std::cout << "]" << std::endl;
      return false;
    }
  private:
    static const std::vector<std::pair<std::string,T>> mapping;
  };

  // convert: Lattice::Type (Enum)
  template<>
  struct convert<blush::Lattice::Type>
    : public convertEnum<blush::Lattice::Type>
  { };

  // convert: Lattice::Orientation (Enum)
  template<>
  struct convert<blush::Lattice::Orientation>
    : public convertEnum<blush::Lattice::Orientation>
  { };

  // convert: Boundary (Enum)
  template<>
  struct convert<blush::Boundary>
    : public convertEnum<blush::Boundary>
  { };

  // convert: Integrator (Enum)
  template<>
  struct convert<blush::Integrator>
    : public convertEnum<blush::Integrator>
  { };

  // convert: EquationOfState (Enum)
  template<>
  struct convert<blush::EquationOfState>
    : public convertEnum<blush::EquationOfState>
  { };

  // convert: ContinuityEquation (Enum)
  template<>
  struct convert<blush::ContinuityEquation>
    : public convertEnum<blush::ContinuityEquation>
  { };

  // convert: MomentumEquation (Enum)
  template<>
  struct convert<blush::MomentumEquation>
    : public convertEnum<blush::MomentumEquation>
  { };

  // convert: BoundaryParticles (Enum)
  template<>
  struct convert<blush::BoundaryParticles>
    : public convertEnum<blush::BoundaryParticles>
  { };

  // convert: DensityCalcultion (Enum)
  template<>
  struct convert<blush::DensityCalculation>
    : public convertEnum<blush::DensityCalculation>
  { };

  // convert: NormalPointing (Enum)
  template<>
  struct convert<blush::NormalPointing>
    : public convertEnum<blush::NormalPointing>
  { };

  // convert: RegionBlueprints
  template<>
  struct convert<blush::RegionBlueprints>
  {
    static Node encode(const blush::RegionBlueprints& rhs)
    {
      (void)rhs; // unused
      Node node;
      assert(0 && "not implemented");
      return node;
    }

    static bool decode(const Node& node, blush::RegionBlueprints& rhs)
    {
      if(!node.IsSequence())
        return false;

      for (const auto &it : node) {
        std::string type = it["type"].as<std::string>();
        if (type == "circle") {
          rhs.circles.push_back(it.as<blush::CircleBlueprint>());
        } else if (type == "polygon") {
          rhs.polygons.push_back(it.as<blush::PolygonBlueprint>());
        } else if (type == "rectangle") {
          rhs.rectangles.push_back(it.as<blush::RectangleBlueprint>());
        } else {
          std::cout << "Warning: In YAML file: Shape type \"" << type
                    << "\" is not valid, ignoring this shape" << std::endl;
        }
      }

      return true;
    }
  };

  template<typename T>
  void getOptional(T& val, const std::string& label, const Node& node)
  {
    if (node[label])
      val = node[label].as<T>();
  }

  template<typename T>
  struct convertBlueprint
  {
    static Node encode(const T& rhs)
    {
      (void)rhs; // unused
      Node node;
      assert(0 && "not implemented");
      return node;
    }

    // generic function for parameters all blueprints have
    static bool decodeBlueprint(const Node& node, T& rhs)
    {
      if(!node.IsMap())
        return false;

      getOptional(rhs.origin, "origin", node);
      getOptional(rhs.rotationCenter, "rotation center", node);
      getOptional(rhs.angle, "angle", node);
      getOptional(rhs.normal, "normal", node);

      return true;
    }
  };

  // convert: CircleBlueprints
  template<>
  struct convert<blush::CircleBlueprint>
    : public convertBlueprint<blush::CircleBlueprint>
  {
    static bool decode(const Node& node,
                       blush::CircleBlueprint& rhs)
    {
      if (!decodeBlueprint(node, rhs))
        return false;

      getOptional(rhs.radius, "radius", node);

      return true;
    }
  };

  // convert: PolygonBlueprints
  template<>
  struct convert<blush::PolygonBlueprint>
    : public convertBlueprint<blush::PolygonBlueprint>
  {
    static bool decode(const Node& node,
                                    blush::PolygonBlueprint& rhs)
    {
      if (!decodeBlueprint(node, rhs))
        return false;

      getOptional(rhs.points, "points", node);

      return true;
    }
  };

  // convert: RectangleBlueprints
  template<>
  struct convert<blush::RectangleBlueprint>
    : public convertBlueprint<blush::RectangleBlueprint>
  {
    static bool decode(const Node& node,
                                    blush::RectangleBlueprint& rhs)
    {
      if (!decodeBlueprint(node, rhs))
        return false;

      getOptional(rhs.size, "size", node);

      return true;
    }
  };

  // convert: PolygonBlueprint::Points
  template<>
  struct convert<blush::PolygonBlueprint::Points>
  {
    using Points = blush::PolygonBlueprint::Points;
    static Node encode(const Points& rhs)
    {
      (void)rhs; // unused
      Node node;
      assert(0 && "not implemented");
      return node;
    }

    static bool decode(const Node& node, Points& rhs)
    {
      if(!node.IsSequence())
        return false;

      for (const auto& it : node)
        rhs.push_back(it.as<blush::Vec>());

      return true;
    }
  };
}

namespace blush
{
  Box BoxFromYaml(YAML::detail::iterator_value yamlbox, double scale_length);
  Polygon PolygonCircleFromYaml(YAML::detail::iterator_value yamlcircle,
                                double scale_length);
  Polygon RotateYamlPolygon(YAML::detail::iterator_value yamlshape,
                            Polygon poly);
  bool NormalTypeFromYamlShape(YAML::detail::iterator_value yamlshape);
}

#endif
