#ifndef INPUTPARAMS_HPP
#define INPUTPARAMS_HPP

#include "boostgeometryutils.hpp"
#include "fwd.hpp"
#include "inputargs.hpp"
#include "runparams.hpp"
#include "vec.hpp"

#include <boost/geometry.hpp>

#include <string>
#include <vector>

namespace blush
{
  // Shapes specified in the YAML input file are read into blueprints,
  // before conversion to regions in which particles may be
  // placed. This two step process is necessary because creation of
  // the final region relies on information which is not fully present
  // in the YAML shape entry. Use of the YAML convert function
  // requires all information in creating an object of a given type to
  // be present in a single entry. Creating the region requires at
  // least the scale and alignment parameters, so the whole YAML file
  // must be processed into separate objects prior to construction of
  // the final intialisation conditions.

  // Distinguish inward-pointing and outward-pointing normal.
  enum class NormalPointing {Inward, Outward};

  struct ShapeBlueprint
  {
    Vec origin{0,0};
    Vec rotationCenter{0,0};
    double angle{0};
    NormalPointing normal{NormalPointing::Inward};
  };

  struct CircleBlueprint : public ShapeBlueprint
  {
    double radius{0};
  };

  struct PolygonBlueprint : public ShapeBlueprint
  {
    using Points = std::vector<Vec>;
    Points points;
  };

  struct RectangleBlueprint : public ShapeBlueprint
  {
    Vec size{0,0};
  };

  struct RegionBlueprints
  {
    std::vector<CircleBlueprint> circles{};
    std::vector<PolygonBlueprint> polygons{};
    std::vector<RectangleBlueprint> rectangles{};
  };

  class InputParams {
  public:
    class Zone {
    public:
      Zone(const CircleBlueprint& bp, double scale)
        : poly_(makePoly(bp, scale)), normal_(bp.normal)
      { }
      Zone(const PolygonBlueprint& bp, double scale)
        : poly_(makePoly(bp, scale)), normal_(bp.normal)
      { }
      Zone(const RectangleBlueprint& bp, double scale)
        : poly_(makePoly(bp, scale)), normal_(bp.normal)
      { }
      const Polygon& poly() const { return poly_; }
      const NormalPointing& normal() const { return normal_; }
    private:
      Polygon makePoly (const CircleBlueprint& bp, double scale) const
      {
        // Rescale parameters
        const Vec origin{scale * bp.origin};
        const double radius{scale * bp.radius};
        // Build a circle from line segments
        Polygon circle{makeCircle(convert<Point>(origin), radius)};
        // Rotate circle about the center of rotation
        circle = rotate(circle, bp.angle, convert<Point>(bp.rotationCenter));
        return circle;
      }
      Polygon makePoly (const PolygonBlueprint& bp, double scale) const
      {
        namespace bg = boost::geometry;
        // Rescale parameters
        const Vec origin{scale * bp.origin};
        // Copy blueprint points into a polygon
        Polygon polygon{};
        for(const auto &it : bp.points) {
          Vec r{scale * it};
          bg::append(polygon, convert<Point>(r));
        }
        bg::correct(polygon);
        // Rotate polygon about center of rotation
        polygon = rotate(polygon, bp.angle, convert<Point>(bp.rotationCenter));
        return polygon;
      }
      Polygon makePoly(const RectangleBlueprint& bp, double scale) const
      {
        namespace bg = boost::geometry;
        // Rescale parameters
        const Vec rectOrigin{scale * bp.origin};
        const Vec size{scale * bp.size};
        // Make a box a convert it to a polygon
        const Box box{convert<Point>(rectOrigin),
            convert<Point>(rectOrigin+size)};
        Polygon polygon{[&](){
            Polygon newPoly;
            bg::convert(box, newPoly);
            return newPoly; }()};
        // Rotate polygon about center of rotation
        polygon = rotate(polygon, bp.angle, convert<Point>(bp.rotationCenter));
        return polygon;
      }
      const Polygon poly_{};
      const NormalPointing normal_{NormalPointing::Inward};
    };

    // Wraps a vector of Zone, wrapper is used to provide a
    // constructor creating Zones from a set of blueprints.
    class Zones {
    public:
      using VecZones = std::vector<Zone>;
      Zones(const RegionBlueprints& bps, double scale)
        : zones_(makeZones(bps, scale))
      { }
      // Wrap vector
      using iterator = VecZones::iterator;
      using const_iterator = VecZones::const_iterator;
      iterator begin() { return zones_.begin(); };
      const_iterator begin() const { return zones_.begin(); };
      const_iterator cbegin() const { return zones_.cbegin(); }
      iterator end() { return zones_.end(); }
      const_iterator end() const { return zones_.end(); }
      const_iterator cend() const { return zones_.cend(); }
    private:
      const VecZones makeZones(const RegionBlueprints& bps, double scale)
      {
        VecZones z{};
        for (const auto &it : bps.circles)
          z.push_back({it, scale});
        for (const auto &it : bps.polygons)
          z.push_back({it, scale});
        for (const auto &it : bps.rectangles)
          z.push_back({it, scale});
        return z;
      }
      VecZones zones_{};
    };

    struct BoundaryRegion {
      Polygon poly;
      bool normal_external;
    };

    InputParams(const InputArgs& args);
    InputParams() = default;

    std::string input_file;
    std::string directory_output; // Data file output location (No relative paths)
    double time_output{0.0}; // File is output every time_output seconds.

    double density0{0.0}; // Reference density.
    double overdensity{0.0}; // Factor to increase starting density to apply pressure.
    double background_pressure{0.0}; // Factor of P_0 to add to equation of state
    double eta_viscosity{0.0}; // Viscosity in Adami implementation
    double force_alpha{0.0}; // Alpha term in artificial viscosity.
    double artificial_viscosity{0.0}; // Kinematic viscosity which is converted to alpha.
    // From SPHysics, = 1.225, others use 1.25.
    double h_scale{0.0}; // Smoothing length h = h_scale * dp. Commonly ~1.5

    Region domain; // corners of domain
    double scaleLength{0.0}; // Multiply all coordinates by this
    double time_run{0.0}; // Time to run simulation.
    Boundary bound_type; // Boundary condition 0: periodic, 1: remove particles.
    Vec gravity; // Gravity defined as acceleration in positive direction.
    double v_max{0.0}; // Maximum expected velocity in simulation.
    int bound_beta{0}; // Constant in boundary interaction (Monaghan, 2009).
    bool aligned_placement{false}; // Toggle aligning particle boxes and lines with grid.
    bool fluid_fills_domain{false}; // Toggle filling whole domain with fluid.

    Lattice::Type latticeType; // Change lattice on which particles are placed.
    Lattice::Orientation latticeOrientation; // Align vertical or horizontal.
    int latticeNumberDensity{0}; // Control total particle number.
    double latticeXSpacing{0.0}; // Align exactly in x direction.
    double latticeYSpacing{0.0}; // Align exactly in y direction.
    double latticeSquareSpacing{0.0}; // Equivalent square lattice spacing
                                      // controls number density.
    double latticeSpacing{0.0}; // Manual control of grid spacing.

    Integrator integrator; // Sets time integrator type.
    EquationOfState eqn_of_state; // Sets equation of state type.
    ContinuityEquation continuity_eqn; // Sets continuity equation type
    MomentumEquation momentum_eqn; // Sets momentum equation type.
    BoundaryParticles boundary_particles; // Sets boundary particle type.
    DensityCalculation densityCalculation; // Sets type of density calculation

    double init_time_run{0.0};
    double init_v_max{0.0};
    double init_damping{0.0};
    Vec init_gravity;
    std::string init_file;
    double init_background_pressure{0.0};
    bool init_mode{false};

    std::vector<Polygon> fluid_regions;
    std::vector<BoundaryRegion> boundary_regions;

    bool test_pois_continued_boundary;
    bool test_zero_velocity_boundary;
    bool test_pressure_zero;
    bool test_const_pressure_boundary;
  };

  std::ostream& operator<<(std::ostream& os, const InputParams& params);
}

#endif
