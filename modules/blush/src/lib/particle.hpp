#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include "fwd.hpp"

#include <ostream>

namespace blush
{
  enum class ParticleType { Fluid, Boundary, Floating };

  class Particle
  {
  public:
    // Access particle number (n)
    std::size_t n() const { return n_; }
    std::size_t& n() { return n_; }
    // Access cell (read only) TODO: should be private
    std::size_t cell() const { return cell_; }
    std::size_t& cell() { return cell_; }

    // Access position (r)
    const Vec& r() const { return r_; }
    Vec& r() { return r_; }
    // Access velocity (v)
    const Vec& v() const { return v_; }
    Vec& v() { return v_; }
    // Access density (rho)
    double rho() const { return rho_; }
    double& rho() { return rho_; }
    // Access pressure (p)
    double p() const { return p_; }
    double& p() { return p_; }
    // Access sound speed (c)
    double c() const { return c_; }
    double& c() { return c_; }

    // Access force (force)
    const Vec& force() const { return force_; }
    Vec& force() { return force_; }
    // Access density rate (dRho/dT) (densityRate)
    double densityRate() const { return densityRate_; }
    double& densityRate() { return densityRate_; }

    // Access densityPositionSum (used in updating Adami boundaries)
    const Vec& densityPositionSum() const { return densityPositionSum_; }
    Vec& densityPositionSum() { return densityPositionSum_; }
    // Access kernelSum (used in updating Adami boundaries)
    double kernelSum() const { return kernelSum_; }
    double& kernelSum() { return kernelSum_; }
    // Access forceTermTmp (used in all force calculations)
    double forceTermTmp() const { return forceTermTmp_; }
    double& forceTermTmp() { return forceTermTmp_; }
    // Access particle type (type)
    ParticleType type() const { return type_; }
    ParticleType& type() { return type_; }
    // Acces remove particle flag
    bool removeParticle() const { return removeParticle_; }
    bool& removeParticle() { return removeParticle_; }

  private:
    std::string print() const;

    std::size_t cell_ = 0; // cell to which particle belongs
    std::size_t n_ = 0; // particle number

    Vec r_ = {0.f,0.f};     // position
    Vec v_ = {0.f,0.f};     // velocity
    double rho_ = 0; // density
    double p_ = 0; // pressure
    double c_ = 0; // sound speed

    Vec force_ = {0.f,0.f}; // force (acceleration)
    double densityRate_ = 0; // dRho/dt

    Vec densityPositionSum_ = {0.f,0.f};
    double kernelSum_ = 0;
    double forceTermTmp_ = 0;

    ParticleType type_= ParticleType::Fluid;

    bool removeParticle_ = false;

    // TODO: Access cell (should be private only)
    //std::size_t& cell { return cell_; }

    friend std::ostream& operator<<(std::ostream& stream, const Particle& p);
    friend class ParticleAccess;
  };

  std::ostream& operator<<(std::ostream& os, const Particle& p);

  // Attorney class (Attorney-Client idiom)
  class ParticleAccess
  {
  private:
    static std::size_t& cell(Particle& p) { return p.cell(); }
  };
}
#endif
