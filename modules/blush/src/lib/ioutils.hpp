#ifndef IOUTILS_HPP
#define IOUTILS_HPP

#include <string>

namespace blush
{
  bool existsFile (const std::string& name);
}

#endif
