#include "pointgrid.hpp"

#include "fwd.hpp"
#include "lattice.hpp"

#include <eigen3/Eigen/Dense>

#include <cassert>
#include <iostream>

namespace
{
  using namespace blush;

  PointGrid::Points getPoints(const Region& region, const Lattice& lattice)
  {
    assert(region[0][0] <= region[1][0] && region[0][1] <= region[1][1]
           && "PointGrid region minimum point is greater than maximum point");

    const double width{region[1][0] - region[0][0]};
    const double height{region[1][1] - region[0][1]};

    const Vec& origin{region[0]};

    // Without adding 1 to this number, the final point in each row is
    // not placed. This is needed for periodic boundaries to work.
    const int nRows{static_cast<int>(
          std::round(height / lattice.vertSpacing()))};
    const int nPointsInRow{static_cast<int>(
          std::round(width / lattice.horzSpacing()))};

    PointGrid::Points points;

    bool offsetRow{false};
    for (int r=0; r<nRows; ++r) {
      double y{origin[1] + r*lattice.ySpacing};
      for (int p=0; p<nPointsInRow; ++p) {
        double x{origin[0] + p*lattice.xSpacing};
        if (offsetRow)
          x += lattice.offset;
        if (lattice.orientation == Lattice::Orientation::Horizontal)
          points.push_back({x,y});
        else if (lattice.orientation == Lattice::Orientation::Vertical)
          points.push_back({y,x});
      }
      offsetRow = offsetRow ? false : true;
    }

    return points;
  }
}

namespace blush
{
  // Constructors
  PointGrid::PointGrid(const Region& region, const Lattice& lattice)
    : points_{getPoints(region, lattice)}
  { }

  // Order in space is:
  // For horizontal orientation:
  // . . . .
  // 5 6 7 8
  // 1 2 3 4
  // For vertical orientation:
  // 4 8 .
  // 3 7 .
  // 2 6 .
  // 1 5 .
  Vec PointGrid::pointPosition(std::size_t index) const
  {
    return points_.at(index);
  }
}
