#ifndef FWD_HPP
#define FWD_HPP

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <eigen3/Eigen/Dense>

#include <vector>

namespace blush
{
  // boostgeometryutils.hpp
  using Point = boost::geometry::model::d2::point_xy<double>;
  using Segment = boost::geometry::model::segment<Point>;
  using Box = boost::geometry::model::box<Point>;
  using Polygon = boost::geometry::model::polygon<Point>;
  using Ring = boost::geometry::model::ring<Point>;

  // inputargs.hpp
  class InputArgs;

  // inputparams.hpp
  struct ShapeBlueprint;
  struct CircleBlueprint;
  struct PolygonBlueprint;
  struct RectangleBlueprint;
  struct RegionBlueprints;
  class InputParams;

  // interact.hpp
  class Interactor;

  // lattice.hpp
  class Lattice;

  // logger.hpp
  struct Logger;

  // particle.hpp
  enum class ParticleType;
  class Particle;

  // particlehandler.hpp
  enum class NeighborLoop;
  class CellNeighbor;
  class HandlerCell;
  class ParticleHandler;
  using Particles = std::vector<Particle>;
  using Loop = std::array<NeighborLoop,2>;
  using CellNeighbors = std::vector<CellNeighbor>;

  // pointgrid.hpp
  class PointGrid;

  // runparams.hpp
  class RunParams;

  // vec.hpp
  using Vec = Eigen::Vector2d;

  // geometryutils.hpp
  using Region = std::array<Vec, 2>;
}

#endif
