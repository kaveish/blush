#include "runparams.hpp"

#include "inputparams.hpp"
#include "kernel.hpp"
#include "lattice.hpp"
#include "optionprinter.hpp"

#include <array>
#include <cassert>
#include <cmath>
#include <iostream>

namespace
{
  using namespace blush;

  struct ConstParams {
    const double hcoef = 0.866025; // Value from DualSPHysics.
    const double tensile_epsilon = 0.4; // Monaghan, 2000
    ConstParams() {} // User-defined constructor so struct is not POD
  };

  // Read input options to calculate spacing.
  Lattice getLattice(
      const blush::Lattice::Type type, const blush::Lattice::Orientation o,
      const int numberDensity, const double xSpacing, const double ySpacing,
      const double squareSpacing, const double spacing)
  {
    // Validation
    {
      // Only one option should be non zero.
      const int numOptions{(numberDensity!=0) + (xSpacing!=0) + (ySpacing!=0)
          + (squareSpacing!=0) + (spacing!=0)};
      // Check if all options are zero.
      if (numOptions == 0) {
        std::cout << "Error: You need to specify a lattice size, choose one of: "
          "\"lattice x spacing\", \"lattice y spacing\", "
          "\"lattice square spacing\", or \"lattice spacing\"" << std::endl;
        throw std::exception();
      }
      // Check if more than one option is non-zero.
      if (numOptions != 1) {
        std::cout << "Error: I'm afraid you can only specify one type of lattice "
          "size, choose one of: "
          "\"lattice x spacing\", \"lattice y spacing\", "
          "\"lattice square spacing\", or \"lattice spacing\"" << std::endl;
        throw std::exception();
      }

      // Options cannot be negative
      if (numberDensity<0 || xSpacing<0.0 || ySpacing<0.0 || squareSpacing<0.0
          || spacing<0.0) {
        std::cout << "Error: Lattice specifications (number density, spacing "
          "etc.) must be positive." << std::endl;
        throw std::exception();
      }
    }

    if (numberDensity!=0)
      return Lattice{Lattice::NumberDensity{}, type, o, numberDensity};
    else if (xSpacing!=0)
      return Lattice{Lattice::XSpacing{}, type, o, xSpacing};
    else if (ySpacing!=0)
      return Lattice{Lattice::YSpacing{}, type, o, ySpacing};
    else if (squareSpacing!=0)
      return Lattice{Lattice::SquareSpacing{}, type, o, squareSpacing};
    else if (spacing!=0)
      return Lattice{Lattice::Spacing{}, type, o, spacing};
    assert(false && "Reached end without returning an option, logic fault");
  }
}

namespace blush
{
  RunParams::RunParams(const InputParams& in)
    : lattice{
    getLattice(
        in.latticeType, in.latticeOrientation, in.latticeNumberDensity,
        in.latticeXSpacing, in.latticeYSpacing, in.latticeSquareSpacing,
        in.latticeSpacing)}
  {
    const ConstParams const_params;

    init_mode = in.init_mode;

    // Todo: If calculation of a param depends on another run param then
    // that previous value should be stored locally as double to avoid
    // compounding floating point errors.

    // Constants required from input file
    directory_output = in.directory_output + "/";
    directory_init = directory_output + "init/";
    directory_run = directory_output + "run/";

    time_output = in.time_output;
    density0 = in.density0;

    if (init_mode)
      time_run = in.init_time_run;
    else
      time_run = in.time_run;
    if (init_mode) {
      gravity = in.init_gravity;
    }
    else {
      gravity = in.gravity;
    }
    domain = in.domain;
    // round domain to nearest particle placement "grid
    // point", important for periodic boundaries
    {
      const double width{domain[1][0] - domain[0][0]};
      const double height{domain[1][1] - domain[0][1]};
      domain[1][0] = domain[0][0] +
        lattice.horzPeriod() * std::round(width/lattice.horzPeriod());
      domain[1][1] = domain[0][1] +
        lattice.vertPeriod() * std::round(height/lattice.vertPeriod());
    }
    if (init_mode)
      v_max = in.init_v_max;
    else
      v_max = in.v_max;

    bound_type = in.bound_type;

    // Calculated constants.
    const double dMass = [&](){
      switch (lattice.type) {
      case Lattice::Type::Square:
        return in.density0 * std::pow(lattice.spacing,2);
      case Lattice::Type::Hex:
        return in.density0 * std::sqrt(3.0)/2.0 * std::pow(lattice.spacing,2);
      default:
        return 0.0;
      }
    }();
    mass = dMass;
    h = in.h_scale * lattice.spacing;
    h2 = 2.0 * h;
    support = h2;
    h_sq = h*h;
    h4_sq = 4.0*h_sq;
    const double w_cubic_coef_tmp = 10.0 / (7.0 * M_PI * h_sq);
    // Morris 1997 cubic spline.
    w_cubic_const_1 = 1.5 * w_cubic_coef_tmp;
    w_cubic_const_2 = 0.25 * w_cubic_coef_tmp;
    w_cubic_grad_const_1 = -0.75 * w_cubic_coef_tmp;
    w_cubic_grad_const_2 = -3.0 * w_cubic_coef_tmp;
    w_wendland_const = 15.0 / (128.0 * h);
    w_wendland_grad_const = -15.0 / (32.0 * h);
    w_wendland1dc2_const = 3.0;
    w_wendland1dc2_grad_const = -12.0;
    w_wendland1dc4_const = 1.77 * 5.0;
    w_wendland1dc4_grad_const = 1.77 * -56.0;
    w_wendland2dc2_const = 7.0 / (M_PI * h_sq);
    w_wendland2dc2_grad_const = 17.5 / (M_PI * std::pow(h,3));
    eta_sq = 0.01 * h_sq;
    c0 = 10.0 * v_max; // Speed of sound.
    sound_const = c0 / pow(in.density0, 3);
    const double dPressure0 = c0*c0 * in.density0 / 7;
    pressure0 = dPressure0;
    pressure_const_morris = c0*c0;
    if (init_mode)
      background_pressure = dPressure0 * in.init_background_pressure;
    else
      background_pressure = dPressure0 * in.background_pressure;
    eta_viscosity = in.eta_viscosity;
    bound_k = 0.01 * c0*c0;
    bound_const = 2.0 * bound_k / in.bound_beta;
    timestep_d = lattice.spacing / in.bound_beta;
    timestep_oversqrtk = 1.0 / sqrt(bound_k);
    tensile_epsilon = const_params.tensile_epsilon;
    // be careful here, Wendland function needs this RunParams object,
    // but it is not yet fully initialized
    tensile_kernel_dp_div =
      1.0 / (WWendland2DC2(lattice.spacing, *this) * lattice.spacing);

    // If alpha is specified, use that, if not, convert artificial_viscosity to alpha
    if (in.force_alpha != 0)
      force_alpha = in.force_alpha;
    else
      force_alpha = 8*in.artificial_viscosity/(h*c0);

    // Calculated run parameters.
    domainSize = domain[1] - domain[0];

    integrator = in.integrator;
    double viscosity = 1/8 * force_alpha * h * c0;
    adami_dtb = 0.125 * h_sq / viscosity;
    adami_dtc = 0.25 * std::sqrt(h / gravity.norm());
    volume0 = dMass / in.density0;

    eqn_of_state = in.eqn_of_state;
    continuity_eqn = in.continuity_eqn;
    momentum_eqn = in.momentum_eqn;
    boundary_particles = in.boundary_particles;
    densityCalculation = in.densityCalculation;

    init_damping = in.init_damping;
    init_file = in.init_file;

    test_pois_continued_boundary = in.test_pois_continued_boundary;
    test_zero_velocity_boundary = in.test_zero_velocity_boundary;
    test_pressure_zero = in.test_pressure_zero;
    test_const_pressure_boundary = in.test_const_pressure_boundary;
  }

  std::ostream& operator<<(std::ostream& os, const RunParams& params)
  {
    OptionPrinter printer;

    printer.add("domain", [&]()->std::string{
          std::stringstream ss;
          ss << params.domain[0] << " " << params.domain[1];
          return ss.str();}())
      .add("mass", params.mass)
      .add("h", params.h)
      .add("support", params.support)
      .add("ref. sound speed", params.c0)
      .add("pressure0", params.pressure0)
      .add("k (Monaghan boundary)", params.bound_k)
      .add("lattice type", params.lattice.type)
      .add("lattice spacing", params.lattice.spacing)
      .add("lattice number density", params.lattice.numberDensity)
      .add("lattice x spacing", params.lattice.xSpacing)
      .add("lattice y spacing", params.lattice.ySpacing);

    os << printer.print();
    return os;
  }
}
