#ifndef VEC_HPP
#define VEC_HPP

#include "fwd.hpp"

#include <eigen3/Eigen/Dense>
#include <type_traits>

namespace blush
{
  using Vec = Eigen::Vector2d;

  template<class T, class Enable = void>
  struct ConvertImpl;

  template<class T>
  struct ConvertImpl<T, typename std::enable_if<
                     std::is_same<Point,T>::value, void>::type>
  {
    static T convert(Vec v)
    {
      return T{v[0], v[1]};
    }
  };

  template<class T>
  T convert(Vec v) { return ConvertImpl<T>::convert(v); }

  std::ostream& operator<<(std::ostream& os, const Vec& v);
}

#endif
