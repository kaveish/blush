#ifndef INTERACT_HPP
#define INTERACT_HPP

#include "fwd.hpp"

namespace blush
{
  class Interactor
  {
  public:
    Interactor(ParticleHandler& handler, const RunParams& params):
      handler_(handler) ,
      params_(params) { }

    enum class InteractType { UpdateBoundary, UpdateDensityRate, UpdateForce ,
        UpdateDensity};

    void update(InteractType type, bool updateDt);
    double dt() const { return dt_; }
  private:
    ParticleHandler& handler_;
    const RunParams& params_;
    double dt_;
  };
}

#endif
