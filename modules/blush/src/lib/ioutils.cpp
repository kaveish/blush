#include "ioutils.hpp"

#include <fstream>
#include <string>

namespace blush
{
  bool existsFile (const std::string& name) {
    std::ifstream f(name.c_str());
    if (f.good()) {
      f.close();
      return true;
    } else {
      f.close();
      return false;
    }
  }
}
