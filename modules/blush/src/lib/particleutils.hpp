#ifndef PARTICLEUTILS_HPP
#define PARTICLEUTILS_HPP

#include "fwd.hpp"

namespace blush
{
  using Particles = std::vector<Particle>;

  double speed(const Particle& p);

  // returns speed^2 for comparison use
  double comparableSpeed(const Particle& p);

  // Only take maximum over particles of a given type
  template< class ForwardIt >
  double maxSpeed(ForwardIt first, ForwardIt last, ParticleType type);
}

#endif
