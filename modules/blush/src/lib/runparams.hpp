#ifndef RUNPARAMS_HPP
#define RUNPARAMS_HPP

#include "fwd.hpp"
#include "lattice.hpp"

#include <array>
#include <iostream>
#include <string>

namespace blush
{
  enum class Boundary { Closed, Periodic };
  enum class Integrator { Monaghan, Adami };
  enum class EquationOfState { Monaghan, Adami };
  enum class ContinuityEquation { Monaghan, Adami };
  enum class MomentumEquation { Monaghan, Adami };
  enum class BoundaryParticles { Monaghan, Adami };
  enum class DensityCalculation { Estimator, Integrate };

  class RunParams
  {
  public:
    RunParams(const InputParams& inputParams);
    // This is a hack. Lattice has no default constructor because it
    // doesn't make sense so we have to explcitly initialise it like
    // so. Ideally RunParams should have no default constructor at all
    // (I think I only use the default for tests, should probably use
    // mocking instead).
    RunParams()
      : lattice{Lattice::Spacing{}, Lattice::Type::Square,
                                      Lattice::Orientation::Horizontal, 0.0}
    { }

    std::string directory_output; // Data file output location (No relative paths)
    std::string directory_init; // Init particle files output location
    std::string directory_run; // Run particle files output location
    double time_output; // File is output every time_output seconds.
    double density0; // Reference density.
    double force_alpha; // Alpha term in artificial viscosity.
    double time_run; // Time to run simulation.
    Vec gravity; // Gravity defined as acceleration in positive direction.
    Region domain;
    double v_max;
    Boundary bound_type; // Boundary condition 0: periodic, 1: remove particles.

    double mass; // Mass of all fluid particles.
    double h; // Kernel smoothing length.
    double h2; // 2x smoothing length.
    double support; // Kernel support radius.
    double h_sq; // h^2
    double h4_sq; // 4h^2
    double w_cubic_const_1; // Cubic spline constant.
    double w_cubic_const_2; // Cubic spline constant.
    double w_cubic_grad_const_1; // Cubic spline constant.
    double w_cubic_grad_const_2; // Cubic spline constant.
    double w_wendland_const;
    double w_wendland_grad_const;
    double w_wendland1dc2_const;
    double w_wendland1dc2_grad_const;
    double w_wendland1dc4_const;
    double w_wendland1dc4_grad_const;
    double w_wendland2dc2_const;
    double w_wendland2dc2_grad_const;
    double eta_sq; // Eta^2, used in force + time step calculation.
    double c0; // [NOT USED] Speed of sound
    double sound_const; // Constant in speed of sound calculation.
    double pressure0; // Constant in pressure calculation.
    double pressure_const_morris;
    double background_pressure; // Added to equation of state
    double eta_viscosity; // Viscosity in Adami implementation
    double bound_k; // [NOT USED] Used in Monaghan boundaries
    double bound_const; // 2*K/(beta).
    double timestep_d; // d=dp/beta for timestep calculation.
    double timestep_oversqrtk; // = 1/sqrt(K) for timestep calculation.
    double tensile_epsilon; // epsilon term in tensile correction.
    double tensile_kernel_dp_div; // 1/kernel value at distance of initial spacing.

    Vec domainSize; // Computation domain size

    Integrator integrator;

    // Required for Adami method only
    double adami_dtb; // Timestep constraint constants
    double adami_dtc;
    double volume0; // Initial volume of fluid particles

    Lattice lattice; // Controls lattice type, spacing etc.

    EquationOfState eqn_of_state;
    ContinuityEquation continuity_eqn;
    MomentumEquation momentum_eqn;
    BoundaryParticles boundary_particles;
    DensityCalculation densityCalculation;

    bool init_mode;
    double init_damping;
    std::string init_file;

    bool test_pois_continued_boundary;
    bool test_zero_velocity_boundary;
    bool test_pressure_zero;
    bool test_const_pressure_boundary;
  };

  std::ostream& operator<<(std::ostream& os, const RunParams& params);
}

#endif
