#include "lattice.hpp"

#include <cmath>

namespace
{
  using namespace blush;

  double getSpacing(Lattice::NumberDensity, Lattice::Type t, int n)
  {
    switch (t) {
    case Lattice::Type::Square:
      return 1 / std::sqrt(n);
    case Lattice::Type::Hex:
      {
        const double c{std::sqrt(2.0)*std::pow(3.0,-1.0/4.0)};
        return c / std::sqrt(n);
      }
    default:
      return 0.0;
    }
  }
  double getSpacing(Lattice::XSpacing, Lattice::Type t, double xS)
  {
    (void)t;
    return xS;
  }
  double getSpacing(Lattice::YSpacing, Lattice::Type t, double yS)
  {
    switch (t) {
    case Lattice::Type::Square:
      return yS;
    case Lattice::Type::Hex:
      {
        const double c{2.0/sqrt(3.0)};
        return c * yS;
      }
    default:
      return 0.0;
    }
  }
  double getSpacing(Lattice::SquareSpacing, Lattice::Type t, double sS)
  {
    switch (t) {
    case Lattice::Type::Square:
      return sS;
    case Lattice::Type::Hex:
      {
        const double c{std::sqrt(2.0)*std::pow(3.0,-1.0/4.0)};
        return c * sS;
      }
    default:
      return 0.0;
    }
  }
  int getNumberDensity(Lattice::Type t, double s)
  {
    switch (t) {
    case Lattice::Type::Square:
      return std::round(1.0/std::pow(s,2));
    case Lattice::Type::Hex:
      {
        const double c{std::pow(std::sqrt(2.0)*std::pow(3.0,-1.0/4.0),2)};
        return std::round(c/std::pow(s,2));
      }
    default:
      return 0;
    }
  }
  double getXSpacing(Lattice::Type t, double s)
  {
    (void)t;
    return s;
  }
  double getYSpacing(Lattice::Type t, double s)
  {
    switch (t) {
    case Lattice::Type::Square:
      return s;
    case Lattice::Type::Hex:
      {
        const double c{2.0/sqrt(3.0)};
        return s / c;
      }
    default:
      return 0.0;
    }
  }
  double getSquareSpacing(Lattice::Type t, double s)
  {
    switch (t) {
    case Lattice::Type::Square:
      return s;
    case Lattice::Type::Hex:
      {
        const double c{std::sqrt(2.0)*std::pow(3.0,-1.0/4.0)};
        return s / c;
      }
    default:
      return 0.0;
    }
  }
  double getOffset(Lattice::Type t, double xS)
  {
    switch (t) {
    case Lattice::Type::Square:
      return 0.0;
    case Lattice::Type::Hex:
      // Move every other row by half of the x spacing
      return xS/2.0;
    default:
      return 0.0;
    }
  }
  double getYPeriod(Lattice::Type t, double yS)
  {
    switch (t) {
    case Lattice::Type::Square:
      return yS;
    case Lattice::Type::Hex:
      // Hex grid repeats every two rows
      return 2.0 * yS;
    default:
      return 0.0;
    }
  }
}

namespace blush
{
  // Constructors
  Lattice::Lattice(Type t, Orientation o, double aSpacing)
    : type{t},
      spacing{aSpacing},
      numberDensity{getNumberDensity(t,aSpacing)},
      xSpacing{getXSpacing(t,aSpacing)},
      ySpacing{getYSpacing(t,aSpacing)},
      squareSpacing{getSquareSpacing(t,aSpacing)},
      orientation{o},
      offset{getOffset(t,xSpacing)},
      xPeriod{xSpacing},
      yPeriod{getYPeriod(t,ySpacing)}
  { }
  Lattice::Lattice(Lattice::NumberDensity, Type t, Orientation o, int n)
    : Lattice(t, o, getSpacing(Lattice::NumberDensity{},t,n))
  { }
  Lattice::Lattice(Lattice::XSpacing, Type t, Orientation o, double xS)
    : Lattice(t, o, getSpacing(Lattice::XSpacing{},t,xS))
  { }
  Lattice::Lattice(Lattice::YSpacing, Type t, Orientation o, double yS)
    : Lattice(t, o, getSpacing(Lattice::YSpacing{},t,yS))
  { }
  Lattice::Lattice(Lattice::SquareSpacing, Type t, Orientation o, double sS)
    : Lattice(t, o, getSpacing(Lattice::SquareSpacing{},t,sS))
  { }
  Lattice::Lattice(Lattice::Spacing, Type t, Orientation o, double s)
    : Lattice(t, o, s)
  { }

  // Spacing and period functions
  double Lattice::horzSpacing() const
  {
    switch (orientation) {
    case Orientation::Horizontal:
      return xSpacing;
    case Orientation::Vertical:
      return ySpacing;
    default:
      return 0.0;
    }
  }
  double Lattice::vertSpacing() const
  {
    switch (orientation) {
    case Orientation::Horizontal:
      return ySpacing;
    case Orientation::Vertical:
      return xSpacing;
    default:
      return 0.0;
    }
  }
  double Lattice::horzPeriod() const
  {
    switch (orientation) {
    case Orientation::Horizontal:
      return xPeriod;
    case Orientation::Vertical:
      return yPeriod;
    default:
      return 0.0;
    }
  }
  double Lattice::vertPeriod() const
  {
    switch (orientation) {
    case Orientation::Horizontal:
      return yPeriod;
    case Orientation::Vertical:
      return xPeriod;
    default:
      return 0.0;
    }
  }

  // ostream insertion operator
  std::ostream& operator<<(std::ostream& os, const Lattice::Type t)
  {
    if (t == Lattice::Type::Square)
      os << "square";
    else if (t == Lattice::Type::Hex)
      os << "hex";

    return os;
  }
}
