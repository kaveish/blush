#ifndef INPUTARGS_HPP
#define INPUTARGS_HPP

#include <string>

namespace blush
{
  class InputArgs
  {
  public:
    InputArgs(int argc, char* argv[])
      // comma operator throws away result of left argument
      : filename( (validateArgs(argc) , argv[1]) ) ,
        init_mode(static_cast<std::string>(argv[2]) != "0")
    { }

    void validateArgs(int argc);
    std::string filename;
    bool init_mode;
  };
}

#endif
