#include "particle.hpp"

#include <sstream>
#include <string>

namespace
{
  template <typename T>
  std::string print2dVector(const T& v)
  {
    std::stringstream string;
    string.precision(1);
    string << std::scientific
           << "(" << v[0] << "," << v[1] << ")";
    return string.str();
  }
}

namespace blush
{
  std::string Particle::print() const
  {
    std::stringstream string;
    string.precision(1);
    string << std::scientific
           << "n " << n_
           << " t " << static_cast<int>(type_)
           << " r " << print2dVector(r_)
           << " v " << print2dVector(v_)
           << " rho " << rho_
           << " p " << p_
           << " f " << print2dVector(force_)
           << " drho " << densityRate_
           << " c " << c_;
    return string.str();
  }

  std::ostream& operator<<(std::ostream& os, const Particle& p)
  {
    return os << p.print();
  }
}
