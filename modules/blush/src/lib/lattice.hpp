#ifndef LATTICE_HPP
#define LATTICE_HPP

#include <iostream>

namespace blush
{
  class Lattice
  {
  public:
    struct NumberDensity{};
    struct XSpacing{};
    struct YSpacing{};
    struct SquareSpacing{};
    struct Spacing{};

    enum class Type { Square, Hex };
    enum class Orientation { Vertical, Horizontal };

    Lattice(Type t, Orientation o, double aSpacing);
    Lattice(NumberDensity, Type t, Orientation o, int n);
    Lattice(XSpacing, Type t, Orientation o, double xS);
    Lattice(YSpacing, Type t, Orientation o, double yX);
    Lattice(SquareSpacing, Type t, Orientation o, double sS);
    Lattice(Spacing, Type t, Orientation o, double s);

    double horzSpacing() const;
    double vertSpacing() const;
    double horzPeriod() const;
    double vertPeriod() const;

    const Type type{Type::Square}; // Lattice on which particles are placed.
    const double spacing{0.0};     // Manual control of grid spacing.
    const int numberDensity{0};    // Control total particle number.
    const double xSpacing{0.0};    // Align exactly in x direction.
    const double ySpacing{0.0};    // Align exactly in y direction.
    const double squareSpacing{0.0}; // Equivalent square lattice spacing
                                     // controls number density.
    const Orientation orientation{Orientation::Horizontal};
    const double offset{0.0};
    const double xPeriod{0.0};
    const double yPeriod{0.0};
  };

  std::ostream& operator<<(std::ostream& os, const Lattice::Type t);
}

#endif
