#ifndef EQUATIONS_HPP
#define EQUATIONS_HPP

#include "fwd.hpp"

namespace blush
{
  double soundSpeed(double density, const RunParams& params);
  double pressureStd(double density, const RunParams& params);
  double pressureMorris(double density, const RunParams& params);
}

#endif
