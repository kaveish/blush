#ifndef BOOSTGEOMETRYUTILS_HPP
#define BOOSTGEOMETRYUTILS_HPP

#include "fwd.hpp"

#include <polyclipping/clipper.hpp>
#include <yaml-cpp/yaml.h>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>

#include <vector>

namespace blush
{
  using Point = boost::geometry::model::d2::point_xy<double>;
  using Segment = boost::geometry::model::segment<Point>;
  using Box = boost::geometry::model::box<Point>;
  using Polygon = boost::geometry::model::polygon<Point>;
  using Ring = boost::geometry::model::ring<Point>;

  std::ostream& operator<<(std::ostream& os, const Point& p);

  std::vector<Polygon> PolygonsFromClipperPolyTree(
      const ClipperLib::PolyTree& polytree, double gridsize);

  ClipperLib::Paths ClipperPathFromPolygon(const Polygon& poly,
                                           const double gridsize);

  // Offsets a boost::geometry polygon using clipper, returning a polygon array
  std::vector<Polygon> OffsetPolygon(const Polygon& poly, double offset,
                                     double gridsize);

  Point AlignPointToGrid(const Point& point, const Vec& origin,
                         const Lattice& lattice);

  Point UnitVectorFromSegment(const Segment& segment);

  std::vector<Polygon> ExpandedPolygons(const std::vector<Polygon>& in_polys,
                                        double gridsize);

  // Offsets a boost::geometry polygon using clipper, returning a polygon array
  std::vector<Polygon> ExtrudePolygon(const Polygon& poly, double offset,
                                      double gridsize);

  // Create circle as polygon from many line segments. Boost geometry
  // currently does not have a circle type.
  Polygon makeCircle(Point p, double radius);

  Polygon rotate(const Polygon &poly, double angle, const Point& rotationCenter);
}

#endif
