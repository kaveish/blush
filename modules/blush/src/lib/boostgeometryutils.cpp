#include "boostgeometryutils.hpp"

#include "lattice.hpp"
#include "vec.hpp"

#include <boost/foreach.hpp>
#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>

namespace blush
{
  namespace bg = boost::geometry;

  std::ostream& operator<<(std::ostream& os, const Point& p)
  {
    os << "(" << bg::get<0>(p) << "," << bg::get<1>(p) << ")";
    return os;
  }

  std::vector<Polygon> PolygonsFromClipperPolyTree(
      const ClipperLib::PolyTree& polytree, double gridsize)
  {
    std::vector<Polygon> polygons;
    ClipperLib::PolyNode* polynode = polytree.GetFirst();
    while (polynode) {
      // Convert polynode contour to bg::ring
      Ring ring;
      for (auto it_point : polynode->Contour) {
        bg::append(ring, bg::make<Point>(it_point.X * gridsize,
                                         it_point.Y * gridsize));
      }
      bg::correct(ring);

      if (polynode->IsHole()) {
        polygons.back().inners().push_back(ring);
      }
      else {
        polygons.push_back(Polygon());
        polygons.back().outer() = ring;
      }

      polynode = polynode->GetNext();
    }

    return polygons;
  }

  ClipperLib::Paths ClipperPathFromPolygon(const Polygon& poly,
                                           const double gridsize)
  {
    ClipperLib::Paths paths;

    {
      ClipperLib::Path path;
      for (const auto& it : poly.outer()) {
        // Coordinates must be integer in clipper so define conversion by gridsize
        path << ClipperLib::IntPoint(
            static_cast<int>(std::round(bg::get<0>(it) / gridsize)),
            static_cast<int>(std::round(bg::get<1>(it) / gridsize)));
      }
      paths.push_back(path);
    }

    for (const auto& it_poly : poly.inners()) {
      ClipperLib::Path path;
      BOOST_REVERSE_FOREACH(Point pt, it_poly) {
        path << ClipperLib::IntPoint(
            static_cast<int>(std::round(bg::get<0>(pt) / gridsize)),
            static_cast<int>(std::round(bg::get<1>(pt) / gridsize)));
      }
      paths.push_back(path);
    }

    return paths;
  }

  // Offsets a boost::geometry polygon using clipper, returning a polygon array
  std::vector<Polygon> OffsetPolygon(const Polygon& poly, double offset,
                                     double gridsize)
  {
    // Convert to clipper path
    ClipperLib::Paths poly_in = ClipperPathFromPolygon(poly, gridsize);

    // Offset path
    ClipperLib::PolyTree polytree_offset;
    ClipperLib::ClipperOffset clipper_offset;
    clipper_offset.AddPaths(poly_in, ClipperLib::jtSquare, ClipperLib::etClosedPolygon);
    clipper_offset.Execute(polytree_offset, offset / gridsize);

    // Convert back to boost::geometry polygons
    return PolygonsFromClipperPolyTree(polytree_offset, gridsize);
  }

  Point AlignPointToGrid(const Point& point, const Vec& origin,
                         const Lattice& lattice)
  {
    Point new_point;
    bg::set<0>(new_point,
               origin[0] + lattice.horzPeriod() *
               std::round((bg::get<0>(point)-origin[0])/lattice.horzPeriod()));
    bg::set<1>(new_point,
               origin[1] + lattice.vertPeriod() *
               std::round((bg::get<1>(point)-origin[1])/lattice.vertPeriod()));

    return new_point;
  }

  Point UnitVectorFromSegment(const Segment& segment)
  {
    Point point = std::get<1>(segment);
    bg::subtract_point(point, std::get<0>(segment));
    bg::divide_value(point, bg::length(segment));
    return point;
  }

  std::vector<Polygon> ExpandedPolygons(const std::vector<Polygon>& in_polys,
                                        double gridsize)
  {
    // Additional small offset expansion to avoid numerical errors
    std::vector<Polygon> out_polys;
    for (const auto& it : in_polys) {
      std::vector<Polygon> polys = OffsetPolygon(it, gridsize*2, gridsize);
      out_polys.insert(out_polys.end(), polys.begin(), polys.end());
    }

    return out_polys;
  }

  // Offsets a boost::geometry polygon using clipper, returning a polygon array
  std::vector<Polygon> ExtrudePolygon(const Polygon& poly, double offset,
                                      double gridsize)
  {
    // Convert to clipper path
    ClipperLib::Paths poly_in = ClipperPathFromPolygon(poly, gridsize);

    // Offset path
    ClipperLib::Paths poly_offset;
    ClipperLib::ClipperOffset clipper_offset;
    clipper_offset.AddPaths(poly_in, ClipperLib::jtSquare, ClipperLib::etClosedPolygon);
    clipper_offset.Execute(poly_offset, offset / gridsize);

    // Clip with original path
    ClipperLib::PolyTree polytree_clipped;
    ClipperLib::Clipper clipper;
    clipper.AddPaths(poly_in, ClipperLib::ptSubject, true);
    clipper.AddPaths(poly_offset, ClipperLib::ptClip, true);
    clipper.Execute(ClipperLib::ctXor, polytree_clipped, ClipperLib::pftNonZero,
                    ClipperLib::pftNonZero);

    // Convert back to boost::geometry polygons
    return PolygonsFromClipperPolyTree(polytree_clipped, gridsize);
  }

  Polygon makeCircle(Point p, double radius)
  {
    bg::strategy::buffer::point_circle point_strategy(360);

    bg::strategy::buffer::distance_symmetric<double> distance_strategy(radius);
    bg::strategy::buffer::join_round join_strategy;
    bg::strategy::buffer::end_round end_strategy;
    bg::strategy::buffer::side_straight side_strategy;

    bg::model::multi_polygon<Polygon> result;
    bg::buffer(p, result, distance_strategy, side_strategy, join_strategy,
               end_strategy, point_strategy);

    Polygon poly = result[0];

    return poly;
  }

  Polygon rotate(const Polygon &poly, double angle, const Point& rotationCenter)
  {
    namespace trans = bg::strategy::transform;
    // To rotate about a point, translate so the center of rotation is
    // at the origin, rotate about the origin, then translate back.
    trans::translate_transformer<double, 2, 2>
      translate1{-bg::get<0>(rotationCenter), -bg::get<1>(rotationCenter)};
    trans::rotate_transformer<bg::degree, double, 2, 2>
      rotate{angle};
    trans::translate_transformer<double, 2, 2>
      translate2{bg::get<0>(rotationCenter), bg::get<1>(rotationCenter)};
    // Combine transformations into matrix
    trans::ublas_transformer<double, 2, 2>
      rotateTrans{prod(translate2.matrix(),rotate.matrix())};
    trans::ublas_transformer<double, 2, 2>
      transRotateTrans{prod(rotateTrans.matrix(),translate1.matrix())};
    // Apply matrix transform
    Polygon polyRotated;
    bg::transform(poly, polyRotated, transRotateTrans);
    return polyRotated;
  }
}
