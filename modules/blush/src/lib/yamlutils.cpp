#include "yamlutils.hpp"

#include "yaml-cpp/yaml.h"

#include <boost/geometry.hpp>

namespace
{
  namespace bg = boost::geometry;
}

namespace YAML
{
  template<>
  const convertEnum<blush::Lattice::Type>::Mapper
  convertEnum<blush::Lattice::Type>::mapping = {
    {"square", blush::Lattice::Type::Square},
    {"hex", blush::Lattice::Type::Hex}};

  template<>
  const convertEnum<blush::Lattice::Orientation>::Mapper
  convertEnum<blush::Lattice::Orientation>::mapping = {
    {"horizontal", blush::Lattice::Orientation::Horizontal},
    {"vertical", blush::Lattice::Orientation::Vertical}};

  template<>
  const convertEnum<blush::Boundary>::Mapper
  convertEnum<blush::Boundary>::mapping = {
    {"closed", blush::Boundary::Closed},
      {"periodic", blush::Boundary::Periodic}};

  template<>
  const convertEnum<blush::Integrator>::Mapper
  convertEnum<blush::Integrator>::mapping = {
    {"adami", blush::Integrator::Adami},
    {"monaghan", blush::Integrator::Monaghan}};

  template<>
  const convertEnum<blush::EquationOfState>::Mapper
  convertEnum<blush::EquationOfState>::mapping = {
    {"adami", blush::EquationOfState::Adami},
    {"monaghan", blush::EquationOfState::Monaghan}};

  template<>
  const convertEnum<blush::ContinuityEquation>::Mapper
  convertEnum<blush::ContinuityEquation>::mapping = {
    {"adami", blush::ContinuityEquation::Adami},
    {"monaghan", blush::ContinuityEquation::Monaghan}};

  template<>
  const convertEnum<blush::MomentumEquation>::Mapper
  convertEnum<blush::MomentumEquation>::mapping = {
    {"adami", blush::MomentumEquation::Adami},
    {"monaghan", blush::MomentumEquation::Monaghan}};

  template<>
  const convertEnum<blush::BoundaryParticles>::Mapper
  convertEnum<blush::BoundaryParticles>::mapping = {
    {"adami", blush::BoundaryParticles::Adami},
    {"monaghan", blush::BoundaryParticles::Monaghan}};

  template<>
  const convertEnum<blush::DensityCalculation>::Mapper
  convertEnum<blush::DensityCalculation>::mapping = {
    {"estimator", blush::DensityCalculation::Estimator},
    {"integrate", blush::DensityCalculation::Integrate}};

  template<>
  const convertEnum<blush::NormalPointing>::Mapper
  convertEnum<blush::NormalPointing>::mapping = {
    {"inward", blush::NormalPointing::Inward},
    {"outward", blush::NormalPointing::Outward}};
}

namespace blush
{
  Box BoxFromYaml(YAML::detail::iterator_value yamlbox, double scale_length) {
    double x_min = scale_length * yamlbox["origin"][0].as<double>();
    double y_min = scale_length * yamlbox["origin"][1].as<double>();
    double x_max = x_min + scale_length * yamlbox["size"][0].as<double>();
    double y_max = y_min + scale_length * yamlbox["size"][1].as<double>();

    Box box(Point(x_min,y_min), Point(x_max,y_max));

    return box;
  }

  Polygon PolygonCircleFromYaml(YAML::detail::iterator_value yamlcircle,
                                double scale_length) {
    double x = scale_length * yamlcircle["origin"][0].as<double>();
    double y = scale_length * yamlcircle["origin"][1].as<double>();
    double radius = scale_length * yamlcircle["radius"].as<double>();

    Point point(x,y);

    // Create circle from point
    bg::strategy::buffer::point_circle point_strategy(360);

    bg::strategy::buffer::distance_symmetric<double> distance_strategy(radius);
    bg::strategy::buffer::join_round join_strategy;
    bg::strategy::buffer::end_round end_strategy;
    bg::strategy::buffer::side_straight side_strategy;

    bg::model::multi_polygon<Polygon> result;
    bg::buffer(point, result, distance_strategy, side_strategy, join_strategy,
               end_strategy, point_strategy);

    Polygon poly = result[0];

    return poly;
  }

  Polygon RotateYamlPolygon(YAML::detail::iterator_value yamlshape,
                            Polygon poly) {
    namespace trans = bg::strategy::transform;

    if (!yamlshape["rotation center"]) {
      std::cout
        << "Error: Rotation center must be defined for shape to be rotated"
        << std::endl;
      std::exit(EXIT_FAILURE);
    }
    double rotation = yamlshape["rotation"].as<double>();
    Point rotation_center(yamlshape["rotation center"][0].as<double>(),
                          yamlshape["rotation center"][1].as<double>());

    trans::translate_transformer<double, 2, 2>
      translate_1(-bg::get<0>(rotation_center), -bg::get<1>(rotation_center));
    trans::rotate_transformer<bg::degree, double, 2, 2>
      rotate(rotation);
    trans::translate_transformer<double, 2, 2>
      translate_2(bg::get<0>(rotation_center), bg::get<1>(rotation_center));

    trans::ublas_transformer<double, 2, 2>
      rotate_trans(prod(translate_2.matrix(),rotate.matrix()));

    trans::ublas_transformer<double, 2, 2>
      rotate_about(prod(rotate_trans.matrix(),translate_1.matrix()));

    Polygon poly_rotated;
    bg::transform(poly, poly_rotated, rotate_about);

    return poly_rotated;
  }

  bool NormalTypeFromYamlShape(YAML::detail::iterator_value yamlshape) {
    bool normal_external;
    if (yamlshape["normal"].as<std::string>() == "external") {
      normal_external = true;
    }
    else if (yamlshape["normal"].as<std::string>() == "internal") {
      normal_external = false;
    }
    else {
      std::cout << "Error: No normal direction defined for shape"
                << std::endl;
      std::exit(EXIT_FAILURE);
    }

    return normal_external;
  }
}
