#include "equations.hpp"

#include "runparams.hpp"

namespace blush
{
  double soundSpeed(double density, const RunParams& params) {
    return params.sound_const * density * density * density;
  }

  // Pressure equation of state.
  double pressureStd(double density, const RunParams& params) {
    // equiv. pressure_const * (pow(density / density0, 7) - 1);
    double density_div = density / params.density0;
    double density_div_3 = density_div*density_div*density_div;
    double pressure;
    if (params.eqn_of_state == EquationOfState::Monaghan) {
      pressure = params.pressure0 *
        (density_div_3*density_div_3 * density_div - 1.0);
    }
    else { // Adami
      pressure = params.pressure0 *
        (density_div_3*density_div_3 * density_div - 1.0)
        + params.background_pressure;
    }
    return pressure;
  }

  // Pressure equation of state (Morris, 1997).
  double pressureMorris(double density, const RunParams& params) {
    // equiv. c0^2 * density;
    return params.pressure_const_morris * density;
  }
}
