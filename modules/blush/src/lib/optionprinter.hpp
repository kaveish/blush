#ifndef OPTIONPRINTER_HPP
#define OPTIONPRINTER_HPP

#include <tuple>
#include <string>
#include <vector>

#include <algorithm>
#include <iomanip>
#include <sstream>

namespace blush
{
  class OptionPrinter
  {
  public:
    using Element = std::pair<std::string,std::string>;
    using Table = std::vector<Element>;

    template<typename T> OptionPrinter & add(std::string s, T t);
    std::string print();
  private:
    Table table_;
    std::size_t width_=80;
    std::size_t precision_=5;
  };

  template<typename T>
  OptionPrinter & OptionPrinter::add(std::string s, T t)
  {
    std::stringstream tss;
    tss << std::setprecision(precision_) << t;
    table_.push_back({s,tss.str()});
    return *this;
  }
}

#endif
