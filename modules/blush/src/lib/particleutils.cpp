#include "particleutils.hpp"

#include "fwd.hpp"
#include "particle.hpp"

#include <algorithm>
#include <cmath>
#include <iostream>

namespace blush
{
  double speed(const Particle& p)
  {
    return p.v().norm();
  }

  double comparableSpeed(const Particle& p)
  {
    return p.v().squaredNorm();
  }

  // Only take maximum over particles of a given type
  template< class ForwardIt >
  double maxSpeed(ForwardIt first, ForwardIt last, ParticleType type)
  {
    // Check if there are particles of correct type
    first = std::find_if(first, last,
                         [type](const Particle& p)
                         {
                           return p.type() == type;
                         });
    if (first == last) {
      std::cout << "Warning: maxSpeed found no particles of type "
                << static_cast<int>(type)
                << std::endl;
      return 0.f;
    }

    auto largest = first;
    double maxSpeed = comparableSpeed(*first);
    ++first;
    // Find max in full range
    for (; first != last; ++first) {
      if (first->type() == type) {
        double speed = comparableSpeed(*first);
        if (maxSpeed < speed) {
          maxSpeed = speed;
          largest = first;
        }
      }
    }

    return speed(*largest);
  }
  template double maxSpeed<Particles::iterator>
  (Particles::iterator first, Particles::iterator last, ParticleType type);
}
