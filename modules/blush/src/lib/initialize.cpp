#include "initialize.hpp"

#include "boostgeometryutils.hpp"
#include "inputparams.hpp"
#include "equations.hpp"
#include "fwd.hpp"
#include "kernel.hpp"
#include "particle.hpp"
#include "particleutils.hpp"
#include "pointgrid.hpp"
#include "runparams.hpp"

#include <polyclipping/clipper.hpp>
#include <yaml-cpp/yaml.h>

#include <boost/filesystem.hpp>
#include <boost/geometry.hpp>
#include <eigen3/Eigen/Dense>

#include <algorithm>
#include <cassert>
#include <fstream>
#include <vector>

using namespace blush;

namespace
{
  namespace bg = boost::geometry;

  template<class T>
  void ParticleFillFromRegions(const std::vector<T>& regions,
                               std::vector<Particle>& part,
                               const PointGrid& grid)
  {
    for (const auto &itPoint : grid) {
      for (const auto &it : regions) {
        if (bg::covered_by(Point{itPoint[0],itPoint[1]}, it)) {
          part.push_back(Particle{});
          part.back().r() = itPoint;
          break;
        }
      }
    }
  }

  template<class Tf, class Tb>
  void ParticleFillDomain(const std::vector<Tf>& fluid_regions,
                          const std::vector<Tb>& boundary_regions,
                          std::vector<Particle>& fluid_part,
                          std::vector<Particle>& bound_part,
                          const PointGrid& grid,
                          const InputParams& input_params,
                          const RunParams& run_params) {
    // Check all points on grid in domain
    for (const auto &itPoint : grid) {
      const Point point{itPoint[0],itPoint[1]};

      bool place = false;

      // Place particle if it's inside a boundary with an internal
      // facing normal
      for (const auto &it : boundary_regions) {
        if (!it.normal_external) {
          if (bg::covered_by(point, it.poly)) {
            place = true;
            break;
          }
        }
      }
      if (!place)
        continue;

      if (!input_params.fluid_fills_domain) {
        // Place particle if it's inside any fluid region
        place = false;
        for (const auto& it : fluid_regions) {
          if (bg::covered_by(point, it)) {
            place = true;
            break;
          }
        }
        if (!place)
          continue;
      }

      // Do not place particle if it's also inside a boundary with an
      // external normal
      for (const auto &it : boundary_regions) {
        if (it.normal_external) {
          if (bg::covered_by(point, it.poly)) {
            place = false;
            break;
          }
        }
      }

      if (place) {
        fluid_part.push_back(Particle{});
        fluid_part.back().r() = itPoint;
      }
    }

    // Remove all fluid particles which are too close to boundary particles
    double spacing = 0.99 * std::pow(run_params.lattice.spacing,2);
    for (auto it=fluid_part.begin(); it!=fluid_part.end();) {
      Point fluid(it->r()[0], it->r()[1]);
      bool erased = false;
      for (const auto &it_bound : bound_part) {
        Point bound(it_bound.r()[0], it_bound.r()[1]);
        if (bg::comparable_distance(fluid, bound) < spacing) {
          fluid_part.erase(it);
          erased = true;
          break;
        }
      }
      if (!erased)
        ++it;
    }
  }

  void OutputSvgFromPlacement(const std::vector<Polygon>& fluid_regions,
                              const std::vector<Polygon>& boundary_regions,
                              const std::vector<Particle>& fluid_part,
                              const std::vector<Particle>& bound_part,
                              const PointGrid& grid,
                              const RunParams& run_params) {
    // Need domain as a bg type
    RectangleBlueprint domainBp;
    domainBp.origin = {run_params.domain[0][0], run_params.domain[0][1]};
    double domainWidth = run_params.domain[1][0]-run_params.domain[0][0];
    double domainHeight = run_params.domain[1][1]-run_params.domain[0][1];
    domainBp.size = {domainWidth,domainHeight};
    Polygon domain{InputParams::Zone{domainBp,1.0}.poly()};

    // We want to view a minimum border around the domain size, so we
    // expand the domain by 10% and add it to the svg for the purpose
    // of scaling.
    double maxOfDomainWidthHeight = std::max(domainWidth, domainHeight);
    Polygon expandedDomain = OffsetPolygon(domain, maxOfDomainWidthHeight/10.0,
                                           run_params.lattice.spacing/100.0)[0];

    // Create svg mapper
    // The final svg dimensions are unpredictable, so for now we use
    // the maximum of the domain width and height as BOTH the width
    // and height. This results in a reasonably square image.
    unsigned svgScale{30};
    unsigned svgWidth{
      static_cast<unsigned>(maxOfDomainWidthHeight/run_params.lattice.spacing)*svgScale};
std::ofstream svg(run_params.directory_output + "placement.svg");
    boost::geometry::svg_mapper<Point> mapper(svg, svgWidth, svgWidth);

    // Need particles as bg types
    std::vector<Point> bound_points;
    for (auto it : bound_part) {
      bound_points.push_back(Point{it.r()[0],it.r()[1]});
    }
    std::vector<Point> fluid_points;
    for (auto it : fluid_part) {
      fluid_points.push_back(Point{it.r()[0],it.r()[1]});
    }
    std::vector<Point> svggrid;
    for (const auto &itPoint : grid) {
      svggrid.push_back(Point{itPoint[0],itPoint[1]});
    }

    //
    // Add objects to svg map for image size purposes
    //

    // Add domain to svg map
    mapper.add(domain);

    //
    // Insert svg elements
    //

    // Insert domain into svg
    mapper.map(domain,"fill:none;stroke:rgb(64,64,64);stroke-width:2");
    // Insert boundary regions into svg
    for (auto it : boundary_regions)
      mapper.map(it,"fill:none;stroke:rgb(218,124,48);stroke-width:2");
    // Insert fluid regions into svg
    for (auto it : fluid_regions)
      mapper.map(it,"fill:none;stroke:rgb(57,106,177);stroke-width:2");
    // Insert grid into svg
    for (auto it : svggrid)
      mapper.map(it,"fill:rgb(0,0,0);stroke:rgb(0,0,0);stroke-width:0", 2);
    // Insert fluid particle points into svg
    for (auto it : bound_points)
      mapper.map(it,"fill:rgb(218,124,48);stroke:rgb(218,124,48);stroke-width:0"
                 , 4);
    // Insert boundary particle points into svg
    for (auto it : fluid_points)
      mapper.map(it,"fill:rgb(57,106,177);stroke:rgb(57,106,177);stroke-width:0"
                 , 4);
  }

  // Function does not need access to all of input_params, could restrict?
  void ParticleInitFromInputParams(std::vector<Particle>& fluid_part,
                                   std::vector<Particle>& bound_part,
                                   const InputParams& input_params,
                                   const RunParams& run_params)
  {
    PointGrid grid(run_params.domain, run_params.lattice);
    // Place boundary particles

    double bound_dp;
    if (run_params.boundary_particles == BoundaryParticles::Monaghan)
      bound_dp = run_params.lattice.spacing / input_params.bound_beta;
    else {// Adami
      bound_dp = run_params.lattice.spacing;
    }

    auto boundary_regions = input_params.boundary_regions;
    auto fluid_regions = input_params.fluid_regions;

    // If initilialising all fluid regions need surrounding with
    // boundary particles
    if (run_params.init_mode) {
      std::vector<Polygon> expanded_regions;
      for (const auto& it : fluid_regions) {
        auto new_polys = OffsetPolygon(it, run_params.lattice.spacing,
                                       run_params.lattice.spacing/100);
        expanded_regions.insert(expanded_regions.end(), new_polys.begin(),
                                new_polys.end());
      }
      for (auto& it : expanded_regions)
        boundary_regions.push_back(InputParams::BoundaryRegion{it, 0});
    }

    if (input_params.aligned_placement) {
      // Align start and end of lines to particle spacing.
      for (auto &it : boundary_regions) {
        // Iterate through points on polygon
        for (auto &it_point : bg::exterior_ring(it.poly))
          it_point = AlignPointToGrid(it_point, run_params.domain[0],
                                      run_params.lattice);
      }
    }

    std::vector<Polygon> boundary_regions_out;
    for (auto it : boundary_regions)
      boundary_regions_out.push_back(it.poly);

    if (run_params.boundary_particles == BoundaryParticles::Monaghan) {
      // Place boundary particles along lines.
      for (auto it : boundary_regions) {
        auto ring = bg::exterior_ring(it.poly);

        // Check if any segments in polygon are shorter than 3dp, if so
        // we'll treat the boundary as a continuous line for placement purposes.
        bool short_segment = false;
        for (auto it_point = ring.begin(); it_point != std::prev(ring.end());
             ++it_point) {
          Segment line (*it_point, *(it_point+1));
          if (bg::length(line) < 3.0*run_params.lattice.spacing) {
            short_segment = true;
            break;
          }
        }

        if (short_segment) {
          // loop through all points except last on polygon

          // Get separation from total length of polygon
          double poly_length = bg::perimeter(ring);
          int n_part = static_cast<int>((poly_length+bound_dp/2.0)/bound_dp);
          double spacing = poly_length / n_part;

          // Remaining distance the particle must be moved along line
          // before it is placed
          double dist_remaining = 0;
          int tot_part = 0;

          for (auto it_point = ring.begin(); it_point != std::prev(ring.end());
               ++it_point) {
            Segment line (*it_point, *(it_point+1));
            double length = bg::length(line);
            Point unit_vector = UnitVectorFromSegment(line);

            // position along line segment to place next particle
            double place_position = dist_remaining;
            while (1) {
              if (place_position > length) {
                dist_remaining = place_position - length;
                break;
              }
              if (tot_part > n_part)
                break;

              Point particle = unit_vector;
              bg::multiply_value(particle, place_position);
              bg::add_point(particle, std::get<0>(line));
              bound_part.push_back(Particle{});
              bound_part.back().r() = {bg::get<0>(particle),
                                       bg::get<1>(particle)};
              ++tot_part;

              place_position += spacing;
            }
          }
        }
        else {
          // loop through all points except last on polygon
          for (auto it_point = ring.begin(); it_point != std::prev(ring.end());
               ++it_point) {
            Segment line (*it_point, *(it_point+1));
            double length = bg::length(line);
            int n_part = static_cast<int>((length+bound_dp/2.0)/bound_dp);
            double spacing = length / n_part;
            Point unit_vector = UnitVectorFromSegment(line);

            for (int i=0; i<n_part; ++i) {
              Point particle = unit_vector;
              bg::multiply_value(particle, i*spacing);
              bg::add_point(particle, std::get<0>(line));
              bound_part.push_back(Particle{});
              bound_part.back().r() = {bg::get<0>(particle),
                                       bg::get<1>(particle)};
            }
          }
        }
      }

      // Remove duplicated boundary particles
      for (auto it_self=bound_part.begin(); it_self != bound_part.end(); ++it_self) {
        for (auto it_other=std::next(it_self); it_other != bound_part.end();) {
          if (it_self->r() == it_other->r())
            bound_part.erase(it_other);
          else
            ++it_other;
        }
      }

      // Remove boundary particles outside of simulation domain
      Box domain(Point(run_params.domain[0][0],run_params.domain[0][1]),
                 Point(run_params.domain[1][0],run_params.domain[1][1]));

      for (auto it=bound_part.begin(); it != bound_part.end();) {
        if (!bg::covered_by(Point(it->r()[0],it->r()[1]), domain))
          bound_part.erase(it);
        else
          ++it;
      }
    }
    else { // Adami
      // Expand regions to kernel support radius away from fluid side of boundary
      double gridsize_bound = bound_dp / 100; // clipper coordinates are on integer grid
      std::vector<Polygon> boundary_regions_offset;
      for (auto it : boundary_regions) {
        double offset;
        if (it.normal_external)
          offset = -run_params.support;
        else
          offset = run_params.support;

        std::vector<Polygon> polys
          = ExtrudePolygon(it.poly, offset, gridsize_bound);

        boundary_regions_offset.insert(boundary_regions_offset.end(),
                                       polys.begin(), polys.end());
      }

      // Additional small offset expansion to avoid numerical errors
      boundary_regions_offset = ExpandedPolygons(boundary_regions_offset,
                                                 gridsize_bound);


      ParticleFillFromRegions(boundary_regions_offset, bound_part, grid);

      boundary_regions_out = boundary_regions_offset;
    }

    // DEBUG: Check that boundary particles do not overlap
    for (auto it_self=bound_part.begin(); it_self!=bound_part.end(); ++it_self){
      for (auto it_other=std::next(it_self); it_other != bound_part.end();
           ++it_other) {
        if (it_self->r() == it_other->r())
          std::cout << "Warning: Boundary particles placed in same position"
                    << std::endl;
      }
    }

    // Place fluid particles

    if (input_params.aligned_placement) {
      // Align start and end of lines to particle spacing = dp/beta.
      for (auto &it : fluid_regions) {
        // Iterate through points on polygon
        for (auto &it_point : bg::exterior_ring(it))
          it_point = AlignPointToGrid(it_point, run_params.domain[0],
                                      run_params.lattice);
      }
    }

    // Additional small offset expansion to avoid numerical errors
    double gridsize = run_params.lattice.spacing / 100; // clipper coordinates are int
    fluid_regions = ExpandedPolygons(fluid_regions, gridsize);

    ParticleFillDomain(fluid_regions, boundary_regions, fluid_part, bound_part,
                       grid, input_params, run_params);

    OutputSvgFromPlacement(fluid_regions, boundary_regions_out, fluid_part,
                           bound_part, grid, run_params);

    // Set particle initial densities and pressures.
    for (auto& it : fluid_part) {
      if (run_params.eqn_of_state == EquationOfState::Monaghan) {
        // over density
        it.rho() = input_params.overdensity * input_params.density0;
      }
      else if (run_params.eqn_of_state == EquationOfState::Adami) {
        it.rho() = input_params.density0;
        it.p() = pressureStd(it.rho(), run_params);
      }
    }
    for (auto& it : bound_part) {
      it.rho() = input_params.density0;
      it.p() = pressureStd(it.rho(), run_params);
    }

    // Number particles
    std::size_t iPart=0;
    for (auto& it : fluid_part) {
      it.n() = iPart;
      ++iPart;
    }
    for (auto& it : bound_part) {
      it.n() = iPart;
      ++iPart;
    }

    // Label particles
    for (auto& it : fluid_part)
      it.type() = ParticleType::Fluid;
    for (auto& it : bound_part)
      it.type() = ParticleType::Boundary;
  }

  void ParticleLoadFromInitFile(std::vector<Particle>& fluid_part,
                                const RunParams& params) {
    std::ostringstream file_name;
    file_name << params.init_file;
    std::ifstream file (file_name.str());

    if (!file.is_open()) {
      std::cout << "Error: Init particle file does not exist" << std::endl;
      std::exit(EXIT_FAILURE);
    }

    for (int i=0; i<4; ++i)
      file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::string line;
    int i=0;
    while(getline(file, line)) {
      std::istringstream iss(line);
      Vec r, v;
      double density, pressure;
      if (!(iss >> r[0] >> r[1] >> v[0] >> v[1] >> density >> pressure)) {
        std::cout << "Error: init.dat file is not in correct format."
                  << std::endl;
        std::exit(EXIT_FAILURE);
      }
      fluid_part[i].r() = r;
      fluid_part[i].v() = v;
      fluid_part[i].rho() = density;
      fluid_part[i].p() = pressure;
      ++i;
    }
  }
}

namespace blush
{
  RunParams StateInitFromFile(InputArgs& args,
                              Particles& particles) {
    assert(particles.size() == 0); // array must be empty

    const InputParams input_params(args);
    const RunParams run_params(input_params);

    boost::filesystem::create_directories(run_params.directory_init);
    boost::filesystem::create_directories(run_params.directory_run);

    // Hack, fix later
    Particles fluid_part, bound_part;

    ParticleInitFromInputParams(fluid_part, bound_part, input_params,
                                run_params);

    if (fluid_part.size()==0) {
      std::cout << "Error: No fluid particles loaded" << std::endl;
      std::exit(EXIT_FAILURE);
    }

    if (!input_params.init_mode)
      ParticleLoadFromInitFile(fluid_part, run_params);

    // Hack
    particles.insert(particles.end(), fluid_part.begin(), fluid_part.end());
    particles.insert(particles.end(), bound_part.begin(), bound_part.end());

    std::cout << "## Run mode ##" << std::endl;
    if (input_params.init_mode) std::cout << "  -> Initialize" << std::endl;
    else std::cout << "  -> Run" << std::endl;
    std::cout << "## Input options ##" << std::endl << input_params
              << "## Run parameters ##" << std::endl << run_params;
    std::cout << "## Particles ##" << std::endl;
    std::cout <<   "  fluid    " << std::setw(12) << std::right
              << fluid_part.size()
              << "\n  boundary " << std::setw(12) << std::right
              << bound_part.size()
              << "\n  total    " << std::setw(12) << std::right
              << fluid_part.size() + bound_part.size()
              << std::endl;
    std::cout << "## Running simulation ##" << std::endl;

    return run_params;
  }
}
