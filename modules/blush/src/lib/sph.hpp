#ifndef SPH_HPP
#define SPH_HPP

#include <vector>

#include "fwd.hpp"

namespace blush
{
  void Run(const Particles& particles, const RunParams& params);
}

#endif
