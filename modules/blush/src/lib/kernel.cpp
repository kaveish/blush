#include "kernel.hpp"

#include "runparams.hpp"

namespace blush
{
  // 1D cubic wendland (Wendland, 1995)
  double WWendland1DCubic(double dist, const RunParams& params) {
    double q = dist / params.h;

    double qtmp = 2.0 - q;
    return params.w_wendland_const * (2.0/3.0 + q) * qtmp*qtmp*qtmp;
  }

  double WGradWendland1DCubic(double dist, const RunParams& params) {
    double q = dist / params.h;

    double qtmp = 2.0 - q;
    return (params.w_wendland_grad_const * q * qtmp*qtmp);
  }

  // 1D Wendland C2 (quartic)
  // Normalised to 1 at r=0, only for boundary interaction use, not fluid.
  double WWendland1DC2(double dist, const RunParams& params) {
    double r = dist / params.h2;

    double rtmp = 1.0 - r;
    return params.w_wendland1dc2_const * (1.0/3.0 + r) * rtmp*rtmp*rtmp;
  }

  // Normalised to 1 at r=0, only for boundary interaction use, not fluid.
  double WGradWendland1DC2(double dist, const RunParams& params) {
    double r = dist / params.h2;

    double rtmp = r - 1.0;
    return params.w_wendland1dc2_grad_const * rtmp*rtmp * r;
  }

  // 1D Wendland C4 (quintic)
  // Normalised to 1 at r=0, only for boundary interaction use, not fluid.
  double WWendland1DC4(double dist, const RunParams& params) {
    double r = dist / params.h2;

    double rtmp = 1.0 - r;
    double rtmp_2 = rtmp * rtmp;
    return params.w_wendland1dc4_const * (1.0/5.0 + r + 8.0/5.0 * r*r)
      * rtmp_2*rtmp_2*rtmp;
  }

  // Normalised to 1 at r=0, only for boundary interaction use, not fluid.
  double WGradWendland1DC4(double dist, const RunParams& params) {
    double r = dist / params.h2;

    double rtmp = r - 1.0;
    double rtmp_2 = rtmp * rtmp;
    return params.w_wendland1dc4_grad_const * rtmp_2*rtmp_2 * r * (r + 0.25f);
  }

  // Cubic spline , Morris 1997.
  double WCubic2D(double dist, double dist_sq, const RunParams& params) {
    double q = dist / params.h;

    if (q >= 1.0) {
      double qtmp = 2.0 - q;
      return params.w_cubic_const_1 * qtmp*qtmp*qtmp;
    }
    else {
      double q_sq = dist_sq / params.h_sq;
      return params.w_cubic_const_2 * (2.0/3.0 - q_sq + 0.5f * q * q_sq);
    }
  }

  double WGradCubic2D(double dist, double dist_sq, const RunParams& params) {
    double q = dist / params.h;

    if (q >= 1.0) {
      double qtmp = 2.0 - q;
      return params.w_cubic_grad_const_1 * qtmp*qtmp;
    }
    else {
      double q_sq = dist_sq / params.h_sq;
      return params.w_cubic_grad_const_2 * (q - 0.75f * q_sq);
    }
  }

  // 2D Wendland C2 (quartic)
  double WWendland2DC2(double dist, const RunParams& params) {
    double r = dist / params.h2;

    double rtmp = 1.0 - r;
    double rtmp2 = rtmp*rtmp;
    return params.w_wendland2dc2_const * (1.0/4.0 + r) * rtmp2*rtmp2;
  }

  double WGradWendland2DC2(double dist, const RunParams& params) {
    double r = dist / params.h2;

    double rtmp = r - 1.0;
    return params.w_wendland2dc2_grad_const * rtmp*rtmp*rtmp * r;
  }
}
