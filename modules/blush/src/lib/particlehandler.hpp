#ifndef PARTICLEHANDLER_HPP
#define PARTICLEHANDLER_HPP

#include "fwd.hpp"
#include "particle.hpp"
#include "runparams.hpp"

#include <array>
#include <vector>

namespace blush
{
  class CellNeighbor;
  class HandlerCell;
  class ParticleHandler;
  enum class NeighborLoop;

  using Particles = std::vector<Particle>;
  using Loop = std::array<NeighborLoop,2>;
  using CellNeighbors = std::vector<CellNeighbor>;

  enum class NeighborLoop { None, Forward, Backward };

  class CellNeighbor
  {
  public:
    CellNeighbor(HandlerCell* aCell, Loop aLoop) : cell_(aCell), loop_(aLoop) {}
    HandlerCell& cell() { return *cell_; }
    const HandlerCell& cell() const { return *cell_; }
    const Loop& loop() const { return loop_; }
  private:
    HandlerCell* cell_;
    Loop loop_;
  };

  class HandlerCell
  {
    friend ParticleHandler;
    friend std::ostream& operator<<(std::ostream& os, const HandlerCell& cell);
  public:
    HandlerCell(const std::size_t index, const Vec& cornerPosition,
                const Vec& size);

    Particles::iterator begin() { return begin_; }
    const Particles::iterator begin() const { return begin_; }
    Particles::iterator end() { return end_; }
    const Particles::iterator end() const { return end_; }
    CellNeighbors& neighbors() { return neighbors_; }
    const CellNeighbors& neighbors() const { return neighbors_; }
  private:
    Particles::iterator begin_;
    Particles::iterator end_;
    CellNeighbors neighbors_;

    const std::size_t index_;
    const Vec cornerPosition_;
    const Vec size_;
    bool hasContained_{false};
    unsigned numContained_{0};

    void setNumContained(int n);
  };

  std::ostream& operator<<(std::ostream& os, const HandlerCell& cell);

  // TODO: Compiles but is unfinished and untested
  // template <int T>
  // class FilterParticles
  // {
  // private:
  //   static auto IsType = [](const Particle& p) {
  //     return static_cast<int>(p.type()) == T; };
  //   typedef boost::filter_iterator<decltype(IsType),Particles::iterator> iterator;
  //   typedef boost::filter_iterator<decltype(IsType),Particles::const_iterator>
  //   const_iterator;
  // public:
  //   FilterParticles(Particles& particles) : particles_(particles) { }
  //
  //   iterator begin() {
  //     return iterator(particles_.begin(), particles_.end()); }
  //   iterator end() {
  //     return iterator(particles_.end(), particles_.end()); }
  //   const_iterator begin() const {
  //     return const_iterator(particles_.begin(), particles_.end()); }
  //   const_iterator end() const {
  //     return const_iterator(particles_.end(), particles_.end()); }
  // private:
  //   Particles& particles_;
  // };

  // Requirements:
  // * All particles are uniquely numbered, new particles get a new
  // number and do not recycle numbers from removed particles.
  // * Modifying access to particle numbers should be private. TODO
  class ParticleHandler
  {
  public:
    using Cells = std::vector<HandlerCell>;
    using CellCoords = Eigen::Vector2i;
    // Input requirements:
    // * Particles in array must be uniquely numbered.
    ParticleHandler(const Particles& aParticles, const Region domain,
                    double supportRadius, Boundary boundType)
      : particles_(aParticles),
        domainOrigin{domain[0]},
      cellGridDimensions(
          initCellGridDimensions(domain, supportRadius)),
      cellDimensions{initCellDimensions(domain)},
      index_(initIndex(aParticles))
    {
      validateParticles();
      initCells(boundType);
      update();
    }

    Cells& cells() { return cells_; }
    const Cells& cells() const { return cells_; }
    Particles& particles() { return particles_; }
    const Particles& particles() const { return particles_; }

    Particle& lookup(std::size_t n);
    bool contains(std::size_t n);
    void update();
    void insert(Particle particle);
  private:
    Particles particles_;
    const Vec domainOrigin;
    const CellCoords cellGridDimensions;
    const Vec cellDimensions;
    std::size_t index_=0; // Unique index to give to new particles
    Cells cells_;

    void updateCells();
    void updateContainingCells();
    void sort();
    void removeFlaggedParticles();
    CellCoords initCellGridDimensions(const Region& domain, double support);
    Vec initCellDimensions(const Region& domain) const;
    void initCells(Boundary boundType);
    std::size_t initIndex(const Particles& particles);
    void validateParticles();
  };
}

#endif
