#ifndef INITIALIZE_HPP
#define INITIALIZE_HPP

#include "fwd.hpp"

#include <string>
#include <vector>

namespace blush
{
  RunParams StateInitFromFile(InputArgs& args,
                              Particles& particles);
}

#endif
