#ifndef POINTGRID_HPP
#define POINTGRID_HPP

#include "fwd.hpp"

#include <eigen3/Eigen/Dense>

#include <vector>

namespace blush
{
  class PointGrid
  {
  public:
    using Points = std::vector<Vec>;

    PointGrid(const Region& region, const Lattice& lattice);

    unsigned nPoints() const { return points_.size(); }
    Vec pointPosition(std::size_t index) const;

    // Wrap vector
    using const_iterator = Points::const_iterator;
    const_iterator begin() const { return points_.begin(); }
    const_iterator cbegin() const { return points_.cbegin(); }
    const_iterator end() const { return points_.end(); }
    const_iterator cend() const { return points_.cend(); }
  private:
    const Points points_;
  };
}

#endif
