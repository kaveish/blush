#ifndef GEOMETRYUTILS_HPP
#define GEOMETRYUTILS_HPP

#include "fwd.hpp"

#include <array>

namespace blush
{
  using Region = std::array<Vec,2>;
}

#endif
