#include "optionprinter.hpp"

#include <algorithm>
#include <iomanip>
#include <sstream>

namespace blush
{
  std::string OptionPrinter::print()
  {
    // calculate maximum width of labels
    std::size_t maxWidth{std::max_element(
        std::begin(table_),std::end(table_),
        [](const Element &left, const Element &right){
          return left.first.length() < right.first.length();
        })->first.length()};
    std::size_t freeLineSpace{width_ - precision_ - 3};
    if (maxWidth > freeLineSpace)
      maxWidth = freeLineSpace;
    std::stringstream ss;
    for (auto &it : table_) {
      ss << "  " << std::left << std::setw(maxWidth+3)
         << it.first << it.second << "\n";
    }
    return ss.str();
  }
}
