#include "inputparams.hpp"

#include "boostgeometryutils.hpp"
#include "ioutils.hpp"
#include "inputargs.hpp"
#include "inputfilereader.hpp"
#include "optionprinter.hpp"
#include "runparams.hpp"
#include "yamlutils.hpp"
#include "vec.hpp"

#include <iostream>

namespace
{
  namespace bg = boost::geometry;
}

namespace blush
{
  InputParams::InputParams(const InputArgs& args) : input_file(args.filename),
                                                    init_mode(args.init_mode)
  {
    InputFileReader reader{input_file};

    directory_output         = reader.get<std::string>("output directory");
    time_output              = reader.get<double>("output period");

    density0                 = reader.get<double>("density");
    overdensity              = reader.get<double>("overdensity");
    background_pressure      = reader.get<double>("background pressure");
    eta_viscosity            = reader.get<double>("eta viscosity");
    artificial_viscosity     = reader.get<double>("artificial viscosity", 0.0);
    force_alpha              = reader.get<double>("force alpha", 0.0);
    h_scale                  = reader.get<double>("h scale");

    domain                   = reader.get<Region>("limits");
    scaleLength              = reader.get<double>("scale length", 1.0);
    time_run                 = reader.get<double>("run time");
    bound_type               = reader.get<Boundary>("limit treatment");
    gravity                  = reader.get<Vec>("gravity");
    v_max                    = reader.get<double>("max velocity");
    bound_beta               = reader.get<int>("boundary beta");
    aligned_placement        = reader.get<bool>("aligned placement");
    fluid_fills_domain       = reader.get<bool>("fluid fills domain");

    latticeType              = reader.get<Lattice::Type>("lattice type",
                                                         Lattice::Type::Square);
    latticeOrientation       = reader.get<Lattice::Orientation>(
        "lattice orientation", Lattice::Orientation::Horizontal);
    latticeNumberDensity     = reader.get<int>("lattice number density", 0);
    latticeXSpacing          = reader.get<double>("lattice x spacing", 0.0);
    latticeYSpacing          = reader.get<double>("lattice y spacing", 0.0);
    latticeSquareSpacing     = reader.get<double>("lattice square spacing",0.0);
    latticeSpacing           = reader.get<double>("lattice spacing", 0.0);

    integrator               = reader.get<Integrator>("time integrator");
    eqn_of_state             = reader.get<EquationOfState>("eqnofstate");
    continuity_eqn           = reader.get<ContinuityEquation>("continuity eqn");
    momentum_eqn             = reader.get<MomentumEquation>("momentum eqn");
    boundary_particles       = reader.get<BoundaryParticles>("boundary particles");
    densityCalculation       = reader.get<DensityCalculation>("density calc");

    init_time_run            = reader.get<double>("init time");
    init_v_max               = reader.get<double>("init max velocity");
    init_damping             = reader.get<double>("init damping");
    init_gravity             = reader.get<Vec>("gravity");
    init_file                = reader.get<std::string>(
        "init filename", directory_output + "/init.dat");
    init_background_pressure = reader.get<double>("init background pressure");
    init_mode                = reader.get<bool>("init mode", false);
    // For now, init_mode is completely overwritten by cmd line arg
    init_mode = args.init_mode;

    const RegionBlueprints fluidBps{reader.get<RegionBlueprints>("fluid", {})};
    const RegionBlueprints boundaryBps{reader.get<RegionBlueprints>("boundary", {})};

    test_pois_continued_boundary = reader.get<bool>(
        "test_pois_continued_boundary", false);
    test_zero_velocity_boundary = reader.get<bool>(
        "test_zero_velocity_boundary", false);
    test_pressure_zero = reader.get<bool>(
        "test_pressure_zero", false);
    test_const_pressure_boundary = reader.get<bool>(
        "test_const_pressure_boundary",false);

    Zones fluidZones{fluidBps, scaleLength};
    Zones boundaryZones{boundaryBps, scaleLength};
    for (const auto &it : fluidZones) {
      fluid_regions.push_back(it.poly());
    }
    for (const auto &it : boundaryZones) {
      boundary_regions.push_back({it.poly(), static_cast<bool>(it.normal())});
    }

    domain[0] *= scaleLength;
    domain[1] *= scaleLength;
    latticeSpacing *= scaleLength;
    latticeXSpacing *= scaleLength;
    latticeYSpacing *= scaleLength;
    latticeSquareSpacing *= scaleLength;
    latticeSpacing *= scaleLength;
  }

  std::ostream& operator<<(std::ostream& os, const InputParams& params)
  {
    OptionPrinter printer;
    printer.add("input file", params.input_file)
      .add("directory", params.directory_output)
      .add("output file period", params.time_output)
      .add("ref density", params.density0)
      .add("overdensity", params.overdensity)
      .add("background pressure", params.background_pressure)
      .add("eta (viscosity)", params.eta_viscosity)
      .add("alpha (viscosity)", params.force_alpha)
      .add("h/dp", params.h_scale)
      .add("domain", [&]()->std::string{
          std::stringstream ss;
          ss << params.domain[0] << " " << params.domain[1];
          return ss.str();}())
      .add("sim run time", params.time_run)
      .add("gravity", params.gravity)
      .add("max velocity", params.v_max)
      .add("beta (Monaghan boundary)", params.bound_beta)
      .add("aligned placement?", params.aligned_placement)
      .add("fluid fills domain?", params.fluid_fills_domain);

    os << printer.print();
    return os;
  }
}
