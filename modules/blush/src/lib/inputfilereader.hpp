#ifndef INPUTFILEREADER_HPP
#define INPUTFILEREADER_HPP

#include "yamlutils.hpp"

#include "yaml-cpp/yaml.h"

#include <exception>
#include <iostream>
#include <string>
#include <typeinfo>

namespace blush
{
  struct YamlEntryMissingException : public std::exception
  {
    const std::string label;
    YamlEntryMissingException(std::string aLabel) : label(aLabel) {}
    ~YamlEntryMissingException() throw () {}
    //const char* what() const throw() { return s.c_str(); };
  };

  class InputFileReader
  {
  public:
    InputFileReader(std::string filename);
    // Option is required, no default provided
    template<typename T> T get(std::string label);
    // Default provided if option not specified
    template<typename T> T get(std::string label, T defaultVal);
  private:
    std::string filename_;
    YAML::Node node_;
    std::string getFilename(std::string filename);
  };

  InputFileReader::InputFileReader(std::string filename)
    : filename_(getFilename(filename)) ,
      node_(YAML::LoadFile(filename))
  { }

  template<typename T>
  T InputFileReader::get(std::string label)
  {
    try {
      // True if label is missing
      if (node_[label].Type() == 0)
        throw YamlEntryMissingException(label);
      else
        return node_[label].as<T>();
    } catch(YamlEntryMissingException &e) {
      std::cout << "Error: missing required YAML entry \"" << e.label << "\""
                << std::endl;
      throw;
    } catch(YAML::TypedBadConversion<T> &e) {
      std::cout << "Error: cannot read YAML entry \"" << label << "\""
                << std::endl;
      throw;
    }
  }

  template<typename T>
  T InputFileReader::get(std::string label, T defaultVal)
  {
    try {
      // True if label is missing
      if (node_[label].Type() == 0)
        return defaultVal;
      else
        return node_[label].as<T>();
    } catch(YAML::TypedBadConversion<T> &e) {
      std::cout << "Error: cannot read YAML entry \"" << label << "\""
                << std::endl;
      throw;
    }
  }

  std::string InputFileReader::getFilename(std::string filename)
  {
    if (!existsFile(filename)) {
      std::cout << "Error: YAML case file does not exist" << std::endl;
      throw std::exception();
    }
    return filename;
  }

}

#endif
