#include "inputargs.hpp"

#include <iostream>

namespace blush
{
  void InputArgs::validateArgs(int argc)
  {
    if (argc != 3) {
      std::cout << "Error: Wrong number of arguments "
        "(usage: ./bin/main file.yaml init[0,1])"
                << std::endl;
      std::exit(EXIT_FAILURE);
    }
  }
}
