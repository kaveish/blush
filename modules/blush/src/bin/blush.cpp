#include "initialize.hpp"
#include "inputargs.hpp"
#include "particle.hpp"
#include "runparams.hpp"
#include "sph.hpp"

#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using namespace blush;

int main (int argc, char * argv[]) {
  InputArgs args(argc, argv);

  Particles particles;

  const RunParams params = StateInitFromFile(args, particles);

  std::cout << std::setw(4) << "file"
            << std::setw(6) << "steps"
            << std::setw(8) << "totstep"
            << std::setw(10) << "dt"
            << std::setw(10) << "vmax"
            << std::setw(10) << "pmin"
            << std::setw(10) << "simtime"
            << std::setw(9) << "totrun t"
            << std::setw(9) << "remain t"
            << std::endl;

  Run(particles, params);

  std::cout << "Done." << std::endl;

  return 0;
}
