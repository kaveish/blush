#include "pointgrid.hpp"

#include "fwd.hpp"
#include "lattice.hpp"

#include "gtest/gtest.h"

using namespace blush;

// Tests currently too simple, need updating

TEST(PointGrid, Basic)
{
  Region region{{{1,2},{8,7}}};
  double spacing{0.5};
  Lattice lattice{Lattice::Spacing{}, Lattice::Type::Square,
                  Lattice::Orientation::Horizontal, spacing};
  PointGrid grid(region, lattice);

  EXPECT_EQ(140, grid.nPoints()); // 14*10
}

TEST(PointGrid, BadRegionInput)
{
  double spacing{0.5};
  Lattice lattice{Lattice::Spacing{}, Lattice::Type::Square,
                  Lattice::Orientation::Horizontal, spacing};
  // Both coordinates lower
  Region region = {{{5,6},{1,3}}};
  EXPECT_DEATH(PointGrid grid(region, lattice),
               "PointGrid region minimum point is greater than maximum point");
  // x coordinate lower
  region = {{{1,2},{0,3}}};
  EXPECT_DEATH(PointGrid grid(region, lattice),
               "PointGrid region minimum point is greater than maximum point");
  // y coordinate lower
  region = {{{1,2},{2,1}}};
  EXPECT_DEATH(PointGrid grid(region, lattice),
               "PointGrid region minimum point is greater than maximum point");
}

TEST(PointGrid, UpperEdgeCase)
{
  Region region{{{1,2},{8,7}}};
  double spacing{0.5};
  Lattice lattice{Lattice::Spacing{}, Lattice::Type::Square,
                  Lattice::Orientation::Horizontal, spacing};
  PointGrid grid(region, lattice);

  EXPECT_EQ(140, grid.nPoints()); // 14*10
}
