#include "gtest/gtest.h"

#include "particle.hpp"
#include "particlehandler.hpp"
#include "vectorutils.hpp"

#include <array>
#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <sstream>

using namespace blush;
using std::vector;
using std::ostream;

// Necessary because for some reason clang fails to find the insertion
// operator for vector when used with Google Test. I think this has
// something to do with the EXPECT..() macro using the std namespace
// explicitly and hindering argument dependent lookup. It works fine
// in gcc.
template<typename T>
std::string print(std::vector<T> v)
{
  std::stringstream ss;
  ss << v;
  return ss.str();
}

class HandlerTest : public testing::Test
{
protected:
  HandlerTest() :
    particles_(initParticles()),
    params_(initParams()),
    handler_(particles_, params_.domain, params_.support, params_.bound_type)
  { }

  Particles initParticles();
  RunParams initParams();

  Particles particles_;
  RunParams params_;
  ParticleHandler handler_;
};

Particles HandlerTest::initParticles()
{
  Particles particles;
  // Arrange particles in diagonal formation
  //
  // |   ..
  // |  X
  // | X
  // |X_____
  //
  // |   |   |   |   |9 10|
  // |   |   |   |7 8|    |
  // |   |   |5 6|   |    |
  // |   |3 4|   |   |    |
  // |1 2|   |   |   |    |
  for (int i=0; i<10; ++i) {
    particles.push_back(Particle());
    particles.back().r() = {i,i};
    particles.back().n() = i;
  }
  return particles;
}

RunParams HandlerTest::initParams()
{
  RunParams params;
  // Use 5x5 grid from 0,0 to 10,10, cells width 2
  params.domain = {{{0,0},{10,10}}};
  params.support = 2;
  params.bound_type = Boundary::Periodic;

  return params;
}

// Count cells to check number
TEST_F(HandlerTest, NumberOfCells)
{
  EXPECT_EQ(handler_.cells().size(), 25);
}

// Check lookup points to correct particles
TEST_F(HandlerTest, LookupToCorrectParticle)
{
  for (unsigned i=0; i<10; ++i)
    EXPECT_EQ(handler_.lookup(i).n(), i);
}

// Particles should know they are in correct cell
TEST_F(HandlerTest, CorrectParticleContainingCell)
{
  // Particles will be in the lower cell when on a border because
  // cells are expanded slightly to account for floating point precision
  EXPECT_EQ(handler_.lookup(0).cell(), 0);
  for (unsigned i=1; i<10; ++i) {
    EXPECT_EQ(handler_.lookup(i).cell(), (i-1)/2*6);
  }
}

// Check that cells contain the correct particles
TEST_F(HandlerTest, ParticlesInCorrectCells)
{
  for (std::size_t i=0; i<25; ++i) {
    std::array<int,2> particles;
    switch (i)
      {
      case 0:
        particles = {0,1};
      case 6:
        particles = {2,3};
      case 12:
        particles = {4,5};
      case 18:
        particles = {6,7};
      case 24:
        particles = {8,9};
      default:
        continue;
      }
    auto it_particle = handler_.cells().at(i).begin();
    EXPECT_EQ(it_particle->n(), particles.at(0));
    ++it_particle;
    EXPECT_EQ(it_particle->n(), particles.at(1));
  }
}

TEST_F(HandlerTest, CellsCorrectNeighbours)
{
  // Add 14 more particles (10 already placed), positions not
  // important as they will be overwritten.
  for (unsigned i=10; i<25; ++i) {
    handler_.insert(Particle{});
  }

  // Place particles on grid in the middle of each cell.
  // |4|9|14|19|24|
  // |3|8|13|18|23|
  // |2|7|12|17|22|
  // |1|6|11|16|21|
  // |0|5|10|15|20|
  for (auto& it : handler_.particles()) {
    it.r() = {1 + 2 * (it.n()/5),
              1 + 2 * (it.n()%5)};
  }

  handler_.update();

  // Store all actual interactions by particle numbers.
  using ParticleNums = std::vector<unsigned>;

  std::map<unsigned, ParticleNums> neighborParticles;

  for (const auto& it : handler_.cells()) {
    for (const auto& itNeigh : it.neighbors()) {
      for (const auto& itSelf : it) {
        for (const auto& itOther : itNeigh.cell()) {
          neighborParticles[itSelf.n()].push_back(itOther.n());
          neighborParticles[itOther.n()].push_back(itSelf.n());
        }
      }
    }
  }

  for (auto& it : neighborParticles)
    std::sort(it.second.begin(), it.second.end());

  // Check a few samples to make sure particles interact with the
  // correct neighbor particles.
  std::map<unsigned, ParticleNums> expectNeighbors;
  // Corners
  expectNeighbors[0]  = ParticleNums{ 1, 4, 5, 6, 9,20,21,24};
  expectNeighbors[4]  = ParticleNums{ 0, 3, 5, 8, 9,20,23,24};
  expectNeighbors[20] = ParticleNums{ 0, 1, 4,15,16,19,21,24};
  expectNeighbors[24] = ParticleNums{ 0, 3, 4,15,18,19,20,23};
  // Middle
  expectNeighbors[12] = ParticleNums{ 6, 7, 8,11,13,16,17,18};

  for (const auto& it : expectNeighbors) {
    unsigned i{0};
    bool match{true};
    // Check number of neighbors
    if (neighborParticles[it.first].size() != it.second.size())
      match = false;
    for (const auto& it_num : it.second) {
      if (match == false)
        break;
      if(it_num != neighborParticles[it.first].at(i))
        match = false;
      ++i;
    }
    EXPECT_TRUE(match)
      << "Particle " << it.first << " has incorrect neighbor particles\n"
      << "  Actual neighbours: " << print(neighborParticles[it.first]) << "\n"
      << "Expected neighbours: " << print(it.second);
  }
}

TEST_F(HandlerTest, CellsLoop)
{
  for (unsigned i=10; i<25; ++i) {
    handler_.insert(Particle{});
  }

  // Place particles on grid in the middle of each cell
  for (auto& it : handler_.particles()) {
    it.r() = {1 + 2 * (it.n()/5),
              1 + 2 * (it.n()%5)};
  }

  handler_.update();

  using ParticleNumLoopPair = std::pair<unsigned, Loop>;
  using ParticleNumLoopPairs = std::vector<ParticleNumLoopPair>;

  std::map<unsigned, ParticleNumLoopPairs> neighborParticles;

  for (const auto& it : handler_.cells()) {
    for (const auto& itNeigh : it.neighbors()) {
      for (const auto& itSelf : it) {
        for (const auto& itOther : itNeigh.cell()) {
          neighborParticles[itSelf.n()].push_back(
              ParticleNumLoopPair{itOther.n(), itNeigh.loop()});
          Loop loop;
          {
            unsigned i=0;
            for (const auto& itLoop : itNeigh.loop()) {
              if (itLoop == NeighborLoop::Forward)
                loop.at(i) = NeighborLoop::Backward;
              else if (itLoop == NeighborLoop::Backward)
                loop.at(i) = NeighborLoop::Forward;
              else
                loop.at(i) = NeighborLoop::None;
              ++i;
            }
          }
          neighborParticles[itOther.n()].push_back(
              ParticleNumLoopPair{itSelf.n(), loop});
        }
      }
    }
  }

  // TODO: COmpiler error: pointer wraparound
  for (auto& it : neighborParticles) {
    std::sort(it.second.begin(), it.second.end(),
              [](const ParticleNumLoopPair& left,
                 const ParticleNumLoopPair& right)
              {
                return left.first < right.first;
              });
  }

  std::map<unsigned, ParticleNumLoopPairs> expectNeighbors;

  {
    auto f = NeighborLoop::Forward;
    auto b = NeighborLoop::Backward;
    auto n = NeighborLoop::None;

    // Corners
    expectNeighbors[0]  = ParticleNumLoopPairs{{ 1,Loop{n,n}},
                                               { 4,Loop{n,b}},
                                               { 5,Loop{n,n}},
                                               { 6,Loop{n,n}},
                                               { 9,Loop{n,b}},
                                               {20,Loop{b,n}},
                                               {21,Loop{b,n}},
                                               {24,Loop{b,b}}};
    expectNeighbors[4]  = ParticleNumLoopPairs{{ 0,Loop{n,f}},
                                               { 3,Loop{n,n}},
                                               { 5,Loop{n,f}},
                                               { 8,Loop{n,n}},
                                               { 9,Loop{n,n}},
                                               {20,Loop{b,f}},
                                               {23,Loop{b,n}},
                                               {24,Loop{b,n}}};
    expectNeighbors[20] = ParticleNumLoopPairs{{ 0,Loop{f,n}},
                                               { 1,Loop{f,n}},
                                               { 4,Loop{f,b}},
                                               {15,Loop{n,n}},
                                               {16,Loop{n,n}},
                                               {19,Loop{n,b}},
                                               {21,Loop{n,n}},
                                               {24,Loop{n,b}}};
    expectNeighbors[24] = ParticleNumLoopPairs{{ 0,Loop{f,f}},
                                               { 3,Loop{f,n}},
                                               { 4,Loop{f,n}},
                                               {15,Loop{n,f}},
                                               {18,Loop{n,n}},
                                               {19,Loop{n,n}},
                                               {20,Loop{n,f}},
                                               {23,Loop{n,n}}};
    // Middle
    expectNeighbors[12] = ParticleNumLoopPairs{{ 6,Loop{n,n}},
                                               { 7,Loop{n,n}},
                                               { 8,Loop{n,n}},
                                               {11,Loop{n,n}},
                                               {13,Loop{n,n}},
                                               {16,Loop{n,n}},
                                               {17,Loop{n,n}},
                                               {18,Loop{n,n}}};
  }

  for (const auto& it : expectNeighbors) {
    unsigned i=0;
    for (const auto& itPair : it.second) {
      const ParticleNumLoopPair& actualPair = neighborParticles[it.first].at(i);
      const ParticleNumLoopPair& expectPair = itPair;

      EXPECT_EQ(actualPair.first, expectPair.first);
      EXPECT_EQ(actualPair.second, expectPair.second)
        << "for particle " << it.first << ":"
        << "\nexpected neighbour part " << expectPair.first
        << " loop " << static_cast<int>(expectPair.second.at(0))
        << " " << static_cast<int>(expectPair.second.at(1))
        << "\nactual   neighbour part " << actualPair.first
        << " loop " << static_cast<int>(actualPair.second.at(0))
        << " " << static_cast<int>(actualPair.second.at(1))
        << std::endl;

      ++i;
    }
  }

}
