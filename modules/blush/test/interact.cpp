#include "interact.hpp"

#include "equations.hpp"
#include "fwd.hpp"
#include "inputparams.hpp"
#include "kernel.hpp"
#include "particle.hpp"
#include "particlehandler.hpp"

#include <array>
#include <cmath>
#include <functional>
#include <iostream>
#include <vector>

#include <eigen3/Eigen/Dense>

#include "gtest/gtest.h"

using namespace blush;

Vec kernelGrad(const Vec& rAB, double h);
double modKernelGrad(const Vec& rAB, double h);
double kernel(double dist, double h);
void forceAdamiFluidFluid(Particle& p1, Particle& p2, const RunParams& params);
void forceAdamiFluidBoundary(Particle& p1, Particle& p2,
                             const RunParams& params);
void forceAdamiBoundaryFluid(Particle& p1, Particle& p2,
                             const RunParams& params);
double error(double value);
void compareParticles(const Particle& p1, const Particle& p2);
void loopPeriodicBoundary(Particle& p1, Particle& p2);
template <typename FuncType>
void compareInteraction(Particle p1, Particle p2, const RunParams& params,
                        Interactor::InteractType interactType,
                        FuncType interactFunc);
void densityRateAdamiFluidFluid(Particle& p1, Particle& p2,
                                const RunParams& params);
void densityRateAdamiFluidBoundary(Particle& p1, Particle& p2,
                                   const RunParams& params);
void densityRateAdamiBoundaryFluid(Particle& p1, Particle& p2,
                                   const RunParams& params);
void densityFluidFluid(Particle& p1, Particle& p2,
                       const RunParams& params);
void densityFluidBoundary(Particle& p1, Particle& p2,
                          const RunParams& params);
void densityBoundaryFluid(Particle& p1, Particle& p2,
                          const RunParams& params);
void boundaryFluidBoundary(Particle& p1, Particle& p2,
                           const RunParams& params);
void boundaryBoundaryFluid(Particle& p1, Particle& p2,
                           const RunParams& params);

class InteractTest : public testing::Test
{
protected:
  using ParticlePair = std::array<Vec,2>;

  InteractTest();

  RunParams initParams();

  RunParams params_;
  Particle p1_, p2_;
  std::vector<ParticlePair> positions_;
};

InteractTest::InteractTest() : params_(initParams())
{
  p1_.type() = ParticleType::Fluid;
  p1_.v() = {0.56,0.22};
  p1_.rho() = 1000.0;
  p1_.p() = pressureStd(p1_.rho(), params_);
  p1_.c() = soundSpeed(p1_.rho(), params_);
  p1_.n() = 1;

  p2_.v() = {-0.14,0.27};
  p2_.rho() = 1000.1;
  p2_.p() = pressureStd(p2_.rho(), params_);
  p2_.c() = soundSpeed(p2_.rho(), params_);
  p2_.n() = 2;

  // Set of particle positions. Particle 1 is placed in 4 locations, particle 2
  // is then placed surrounding that particle to cover all cases of call
  // interaction: same cell, neighbour cell and periodic neighbour cell.
  std::vector<ParticlePair> positions{
    // Central point
    {{{1.5,1.5},{1.6,1.5}}}
    ,{{{1.5,1.5},{0.9,2.1}}}
    ,{{{1.5,1.5},{1.5,2.1}}}
    ,{{{1.5,1.5},{2.1,2.1}}}
    ,{{{1.5,1.5},{0.9,1.5}}}
    ,{{{1.5,1.5},{2.1,1.5}}}
    ,{{{1.5,1.5},{0.9,0.9}}}
    ,{{{1.5,1.5},{1.5,0.9}}}
    ,{{{1.5,1.5},{2.1,0.9}}}
    // Top left point
    ,{{{0.5,3.5},{3.9,3.5}}}
    ,{{{0.5,3.5},{3.9,2.9}}}
    ,{{{0.5,3.5},{0.5,0.1}}}
    ,{{{0.5,3.5},{1.1,0.1}}}
    ,{{{0.5,3.5},{3.9,0.1}}}
    // Top right point
    ,{{{3.5,3.5},{0.1,3.5}}}
    ,{{{3.5,3.5},{0.1,2.9}}}
    ,{{{3.5,3.5},{0.1,0.1}}}
    ,{{{3.5,3.5},{2.9,0.1}}}
    ,{{{3.5,3.5},{3.5,0.1}}}
    // Bottom left point
    ,{{{0.5,0.5},{0.5,3.9}}}
    ,{{{0.5,0.5},{1.1,3.9}}}
    ,{{{0.5,0.5},{3.9,3.9}}}
    ,{{{0.5,0.5},{3.9,1.1}}}
    ,{{{0.5,0.5},{3.9,0.5}}}
    // Bottom right point
    ,{{{3.5,0.5},{0.1,3.9}}}
    ,{{{3.5,0.5},{2.9,3.9}}}
    ,{{{3.5,0.5},{3.5,3.9}}}
    ,{{{3.5,0.5},{0.1,1.1}}}
    ,{{{3.5,0.5},{0.1,0.5}}}
  };

  positions_ = positions;
}

RunParams InteractTest::initParams()
{
  InputParams in{};
  in.density0 = 1000.0;
  in.overdensity = 1.0;
  in.background_pressure = 0.0;
  in.eta_viscosity = 10;
  in.force_alpha = 0.1;
  in.h_scale = 1.5;

  in.v_max = 5;

  in.domain = {{{0,0},{4,4}}};
  in.bound_type = Boundary::Periodic;
  in.gravity = {0,0};

  in.latticeType = Lattice::Type::Square;
  in.latticeSpacing = 1.0/3.0;

  in.bound_beta = 2;
  in.aligned_placement = false;
  in.fluid_fills_domain = false;

  in.integrator = Integrator::Adami;
  in.eqn_of_state = EquationOfState::Adami;
  in.continuity_eqn = ContinuityEquation::Adami;
  in.momentum_eqn = MomentumEquation::Adami;
  in.boundary_particles = BoundaryParticles::Adami;

  in.init_mode = false;

  RunParams runParams(in);

  return runParams;
}

// WGradWendland2DC2 kernel gradient
Vec kernelGrad(const Vec& rAB, double h)
{
  const double pi = 3.141592653589793238463;
  const double modR = rAB.norm();
  const Vec unitR = rAB.normalized();
  const double q = modR / (2*h);
  return 17.5 / (pi * std::pow(h,3)) * q * std::pow(q-1.0,3) * unitR;
}

double modKernelGrad(const Vec& rAB, double h)
{
  const double pi = 3.141592653589793238463;
  const double modR = rAB.norm();
  const double q = modR / (2*h);
  return 17.5 / (pi * std::pow(h,3)) * q * std::pow(q-1.0,3);
}

// WGradWendland2DC2 kernel
double kernel(double dist, double h)
{
  const double pi = 3.141592653589793238463;
  const double r = dist / (2*h);
  return 1.75/(pi*std::pow(h,2))*std::pow(1-r,4)*(1+4*r);
}

// todo: update particle values rather than outputting resulting acceleration
void forceAdamiFluidFluid(Particle& p1, Particle& p2, const RunParams& params)
{
  Vec rAB = p1.r() - p2.r();
  Vec vAB = p1.v() - p2.v();
  double modR = rAB.norm();

  double tildeP = (p2.rho()*p1.p() + p1.rho()*p2.p()) / (p1.rho() + p2.rho());
  double piTerm = -params.force_alpha * params.h * 0.5*(p1.c()+p2.c())
    * vAB.dot(rAB) / (0.5*(p1.rho()+p2.rho()) * std::pow(modR,2));

  Vec gradW = kernelGrad(rAB, params.h);
  double modGradW = modKernelGrad(rAB, params.h);

  double volumeA = params.mass / p1.rho();
  double volumeB = params.mass / p2.rho();

  Vec aPressure = -1/params.mass * tildeP
    * (std::pow(volumeA,2)+std::pow(volumeB,2))
    * gradW;
  Vec aViscous = 1/params.mass * params.eta_viscosity
    * (std::pow(volumeA,2)+std::pow(volumeB,2))
    * vAB/modR * modGradW;
  Vec aArtVisc = - params.mass * piTerm * gradW;

  Vec acceleration = aPressure + aViscous + aArtVisc;

  p1.force() = acceleration;
  p2.force() = - acceleration;
}

void forceAdamiFluidBoundary(Particle& p1, Particle& p2,
                             const RunParams& params)
{
  /*
    Same as fluid-fluid interaction but no $\Pi_{ab}$ term.
  */
  Vec rAB = p1.r() - p2.r();
  Vec vAB = p1.v() - p2.v();
  double modR = rAB.norm();

  double tildeP = (p2.rho()*p1.p() + p1.rho()*p2.p()) / (p1.rho() + p2.rho());
  double piTerm = -params.force_alpha * params.h * 0.5*(p1.c()+p2.c())
    * vAB.dot(rAB) / (0.5*(p1.rho()+p2.rho()) * std::pow(modR,2));

  Vec gradW = kernelGrad(rAB, params.h);
  double modGradW = modKernelGrad(rAB, params.h);

  double volumeA = params.mass / p1.rho();
  double volumeB = params.mass / p2.rho();

  Vec aPressure = -1/params.mass * tildeP
    * (std::pow(volumeA,2)+std::pow(volumeB,2))
    * gradW;
  Vec aViscous = 1/params.mass * params.eta_viscosity
    * (std::pow(volumeA,2)+std::pow(volumeB,2))
    * vAB/modR * modGradW;
  // Adami strictly ignores artificial viscosity for boundary
  Vec aArtVisc = - params.mass * piTerm * gradW;

  Vec acceleration = aPressure + aViscous + aArtVisc;

  p1.force() = acceleration;
}

void forceAdamiBoundaryFluid(Particle& p1, Particle& p2,
                             const RunParams& params)
{
  // Interaction is in opposite sense (p2 fluid, p1 boundary)
  forceAdamiFluidBoundary(p2, p1, params);
}

// produces value 1% of that given
double error(double value)
{
  return std::abs(value/1e7);
}

void compareParticles(const Particle& pTest, const Particle& pCode)
{
  // Required comparisons:
  // UpdateForce -> Force
  // UpdateDensityRate -> DensityRate
  // UpdateBoundary -> Velocity, Pressure, Density

  // Error is 1% from test value
  EXPECT_NEAR(pTest.force()[0], pCode.force()[0], error(pTest.force()[0]));
  EXPECT_NEAR(pTest.force()[1], pCode.force()[1], error(pTest.force()[1]));

  EXPECT_NEAR(pTest.densityRate(), pCode.densityRate(), error(pTest.densityRate()));

  EXPECT_NEAR(pTest.v()[0], pCode.v()[0],
              error(pTest.v()[0]));
  EXPECT_NEAR(pTest.v()[1], pCode.v()[1],
              error(pCode.v()[1]));

  EXPECT_NEAR(pTest.p(), pCode.p(), error(pTest.p()));

  EXPECT_NEAR(pTest.rho(), pCode.rho(), error(pTest.rho()));
}

// Very simple periodic boundary simulator, only works when p2 needs to be
// repositioned in terms of p1
void loopPeriodicBoundary(Particle& p1, Particle& p2)
{
  double domainSize = 4.f;
  for (unsigned i=0; i<2; ++i) {
    double r = p2.r()[i] - p1.r()[i];
    if (r > 1.f) p2.r()[i] -= domainSize;
    if (r < -1.f) p2.r()[i] += domainSize;
  }
}

template <typename FuncType>
void compareInteraction(Particle p1, Particle p2, const RunParams& params,
                        Interactor::InteractType interactType,
                        FuncType interactFunc)
{
  Particles particles;
  particles.push_back(p1);
  particles.push_back(p2);

  ParticleHandler handler(particles, params.domain, params.support,
                          params.bound_type);
  Interactor interactor(handler, params);

  interactor.update(interactType, false);

  loopPeriodicBoundary(p1, p2);

  interactFunc(p1, p2, params);
  compareParticles(p1, handler.lookup(1));
  compareParticles(p2, handler.lookup(2));
}

void densityRateAdamiFluidFluid(Particle& p1, Particle& p2, const RunParams& params)
{
  /*
    For constant mass and a prescribed wall velocity of zero
    $$\mathbf r_{ab}=\mathbf r_a-\mathbf r_b\\
    \dot{\mathbf\rho_a}=\rho_aV_0\mathbf v_a
    \cdot(\nabla_aW_{ab}\cdot\mathbf r_{ab})$$
  */
  // http://www.texpaste.com/n/zolnjs5
  Vec rAB = p1.r() - p2.r();
  Vec vAB = p1.v() - p2.v();
  Vec aTermP1 = (p1.rho() * params.mass/p2.rho()) * vAB;
  Vec aTermP2 = (p2.rho() * params.mass/p1.rho()) * vAB;
  Vec wGrad = kernelGrad(rAB, params.h);

  p1.densityRate() = aTermP1.dot(wGrad);
  p2.densityRate() = aTermP2.dot(wGrad);
}

void densityRateAdamiFluidBoundary(Particle& p1, Particle& p2,
                                   const RunParams& params)
{
  /*
    For constant mass and a prescribed wall velocity of zero
    $$\mathbf r_{ab}=\mathbf r_a-\mathbf r_b\\
    \mathbf e_{ab}=\mathbf r_b-\mathbf r_a\\
    \dot{\mathbf\rho_a}=\rho_a\frac{m_b}{\rho_b}\mathbf v_{ab}
    \cdot(\nabla_aW_{ab}\cdot\mathbf e_{ab})\\
    \dot{\mathbf\rho_a}=\rho_aV_0\mathbf v_a
    \cdot(\nabla_aW_{ab}\cdot\mathbf e_{ab})
    $$
    which we split into
    $$\mathbf A = \rho_a\frac{m_b}{\rho_b}\mathbf v_{ab}\\
    \dot{\mathbf\rho_a}=\mathbf A\cdot(\nabla_aW_{ab}\cdot\mathbf e_{ab})$$
  */
  // http://www.texpaste.com/n/m216wrh8

  Vec rAB = p1.r().cast<double>() - p2.r().cast<double>();
  Vec vA = p1.v().cast<double>();
  //Vec aTerm (p1.rho() * params.mass/p2.rho() * vAB[0],
  //                       p1.rho() * params.mass/p2.rho() * vAB[1]);
  Vec gradW = kernelGrad(rAB, params.h);
  //double densityRate = aTerm[0] * wGrad*rAB[0] + aTerm[1] * wGrad*rAB[1]

  // In Adami 2012 volume0 is used in this equation, regardless of the
  // actual density of the boundary particle.
  double densityRate = p1.rho() * params.volume0 * vA.dot(gradW);
  p1.densityRate() = densityRate;
}

void densityRateAdamiBoundaryFluid(Particle& p1, Particle& p2,
                                   const RunParams& params)
{
  densityRateAdamiFluidBoundary(p2, p1, params);
}


void densityFluidFluid(Particle& p1, Particle& p2,
                       const RunParams& params)
{
  /*
    $$ W_{ab} = W(\mathbf r_a - \mathbf r_b,h)\ \
    \rho_a = \sum_bm_bW_{ab}$$
  */
  // http://www.texpaste.com/n/kb5t2n79

  Vec rAB = p1.r().cast<double>() - p2.r().cast<double>();
  double d = rAB.norm();
  double w = kernel(d, params.h);
  p1.rho() = params.mass * w;
  p2.rho() = params.mass * w;
}

void densityFluidBoundary(Particle& p1, Particle& p2,
                          const RunParams& params)
{
  /*
    $$ W_{ab} = W(\mathbf r_a - \mathbf r_b,h)\ \
    \rho_a = \sum_bm_bW_{ab}$$
  */
  // http://www.texpaste.com/n/kb5t2n79

  Vec rAB = p1.r().cast<double>() - p2.r().cast<double>();
  double d = rAB.norm();
  double w = kernel(d, params.h);
  p1.rho() = params.mass * w;
}

void densityBoundaryFluid(Particle& p1, Particle& p2,
                          const RunParams& params)
{
  densityFluidBoundary(p2, p1, params);
}

void boundaryFluidBoundary(Particle& p1, Particle& p2,
                           const RunParams& params)
{
  Vec rAB = p1.r().cast<double>() - p2.r().cast<double>();
  double d = rAB.norm();
  double w = kernel(d, params.h);
  (void)w; // unused for now due to simplified eqn

  p2.v()=-p1.v();
  p2.p()=p1.p();
  p2.rho()=params.density0*std::pow((p2.p()/params.pressure0+1.0),1.0/7.0);
}

void boundaryBoundaryFluid(Particle& p1, Particle& p2,
                           const RunParams& params)
{
  boundaryFluidBoundary(p2, p1, params);
}

// Setup two particles in known positions, interact them, then check
// the resultant force on each particle is correct.
TEST_F(InteractTest, UpdateForceAdamiFluidFluid)
{
  // Choose equations
  params_.eqn_of_state = EquationOfState::Adami;
  params_.continuity_eqn = ContinuityEquation::Adami;
  params_.momentum_eqn = MomentumEquation::Adami;
  params_.boundary_particles = BoundaryParticles::Adami;

  p1_.type() = ParticleType::Fluid;
  p2_.type() = ParticleType::Fluid;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateForce,
                       forceAdamiFluidFluid);
  }
}

TEST_F(InteractTest, UpdateForceAdamiFluidBoundary)
{
  // Choose equations
  params_.eqn_of_state = EquationOfState::Adami;
  params_.continuity_eqn = ContinuityEquation::Adami;
  params_.momentum_eqn = MomentumEquation::Adami;
  params_.boundary_particles = BoundaryParticles::Adami;

  p1_.type() = ParticleType::Fluid;
  p2_.type() = ParticleType::Boundary;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateForce,
                       forceAdamiFluidBoundary);
  }
}

TEST_F(InteractTest, UpdateForceAdamiBoundaryFluid)
{
  // Choose equations
  params_.eqn_of_state = EquationOfState::Adami;
  params_.continuity_eqn = ContinuityEquation::Adami;
  params_.momentum_eqn = MomentumEquation::Adami;
  params_.boundary_particles = BoundaryParticles::Adami;

  p1_.type() = ParticleType::Boundary;
  p2_.type() = ParticleType::Fluid;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateForce,
                       forceAdamiBoundaryFluid);
  }
}

TEST_F(InteractTest, UpdateDensityRateAdamiFluidFluid)
{
  // Choose equations
  params_.eqn_of_state = EquationOfState::Adami;
  params_.continuity_eqn = ContinuityEquation::Adami;
  params_.momentum_eqn = MomentumEquation::Adami;
  params_.boundary_particles = BoundaryParticles::Adami;

  p1_.type() = ParticleType::Fluid;
  p2_.type() = ParticleType::Fluid;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateDensityRate,
                       densityRateAdamiFluidFluid);
  }
}

TEST_F(InteractTest, UpdateDensityRateAdamiFluidBoundary)
{
  // Choose equations
  params_.eqn_of_state = EquationOfState::Adami;
  params_.continuity_eqn = ContinuityEquation::Adami;
  params_.momentum_eqn = MomentumEquation::Adami;
  params_.boundary_particles = BoundaryParticles::Adami;

  p1_.type() = ParticleType::Fluid;
  p2_.type() = ParticleType::Boundary;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateDensityRate,
                       densityRateAdamiFluidBoundary);
  }
}

TEST_F(InteractTest, UpdateDensityRateAdamiBoundaryFluid)
{
  // Choose equations
  params_.eqn_of_state = EquationOfState::Adami;
  params_.continuity_eqn = ContinuityEquation::Adami;
  params_.momentum_eqn = MomentumEquation::Adami;
  params_.boundary_particles = BoundaryParticles::Adami;

  p1_.type() = ParticleType::Boundary;
  p2_.type() = ParticleType::Fluid;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateDensityRate,
                       densityRateAdamiBoundaryFluid);
  }
}

TEST_F(InteractTest, UpdateDensityFluidFluid)
{
  // Choose equations
  // Implementation same for all

  p1_.type() = ParticleType::Fluid;
  p2_.type() = ParticleType::Fluid;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateDensity,
                       densityFluidFluid);
  }
}

TEST_F(InteractTest, UpdateDensityFluidBoundary)
{
  // Choose equations
  // Implementation same for all

  p1_.type() = ParticleType::Fluid;
  p2_.type() = ParticleType::Boundary;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateDensity,
                       densityFluidBoundary);
  }
}

TEST_F(InteractTest, UpdateDensityBoundaryFluid)
{
  // Choose equations
  // Implementation same for all

  p1_.type() = ParticleType::Boundary;
  p2_.type() = ParticleType::Fluid;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateDensity,
                       densityBoundaryFluid);
  }
}

TEST_F(InteractTest, UpdateBoundaryFluidBoundary)
{
  // Choose equations
  // Implementation same for all

  p1_.type() = ParticleType::Fluid;
  p2_.type() = ParticleType::Boundary;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateBoundary,
                       boundaryFluidBoundary);
  }
}

TEST_F(InteractTest, UpdateBoundaryBoundaryFluid)
{
  // Choose equations
  // Implementation same for all

  p1_.type() = ParticleType::Boundary;
  p2_.type() = ParticleType::Fluid;

  // Test interactions with particles in all positions
  for (const auto& it : positions_) {
    p1_.r() = it[0];
    p2_.r() = it[1];
    compareInteraction(p1_, p2_, params_,
                       Interactor::InteractType::UpdateBoundary,
                       boundaryBoundaryFluid);
  }
}
