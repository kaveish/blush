BLUSH_NAME := blush
MOD_$(BLUSH_NAME)_SRCLIB := $(wildcard modules/$(BLUSH_NAME)/src/lib/*.cpp)
MOD_$(BLUSH_NAME)_SRCBIN := $(wildcard modules/$(BLUSH_NAME)/src/bin/*.cpp)
MOD_$(BLUSH_NAME)_SRCTEST := $(wildcard modules/$(BLUSH_NAME)/test/*.cpp)
LDLIBS += -lyaml-cpp -lboost_filesystem -lboost_system -lpolyclipping

MOD_$(BLUSH_NAME)_OBJLIB := $(call fObjFromSrc,$(MOD_$(BLUSH_NAME)_SRCLIB))
MOD_$(BLUSH_NAME)_OBJBIN := $(call fObjFromSrc,$(MOD_$(BLUSH_NAME)_SRCBIN))
MOD_$(BLUSH_NAME)_OBJTEST := $(call fObjFromSrc,$(MOD_$(BLUSH_NAME)_SRCTEST))
