#!/usr/bin/env python

import argparse
import os
import sys
import yaml
from subprocess import call

def extant_file(x):
    """
    'Type' for argparse - checks that file exists but does not open.
    """
    if not os.path.exists(x):
        raise argparse.ArgumentError("{0} does not exist".format(x))
    return x

# Read in input arguments.
parser = argparse.ArgumentParser(description='''
        Convert placement svg to png and view.

        Example:
            view-placement.py file.yaml
        '''
)
parser.add_argument(
    'input_file', metavar='FILE.yaml', default='local',
    type=extant_file,
    help='''
        YAML configuration file.
        '''
)
args = parser.parse_args()

# Get directory from YAML file.
filename = os.path.abspath(args.input_file)
if not os.path.isfile(filename):
    print "YAML file does not exist"
    sys.exit(0)
file = open(filename)
node = yaml.safe_load(file)
file.close()
directory = node['output directory']

svg_file = directory + "/placement.svg"
png_file = directory + "/placement.png"

call(['convert', svg_file, png_file])
call(['xdg-open', png_file])
