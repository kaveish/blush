#!/bin/bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd $SCRIPTDIR
mkdir -p ../out/jpg
convert ../out/images/*.png ../out/jpg/%05d.jpg
ffmpeg -r 25 -qscale 2 -i ../out/jpg/%05d.jpg ../out/out.mp4
popd
