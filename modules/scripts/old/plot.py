#!/usr/bin/env python

import os
import matplotlib.pyplot as plt
import numpy as np
import sys
import glob

os.chdir(os.path.dirname(sys.argv[0]))
directory = '../out/images'
if not os.path.exists(directory):
    os.makedirs(directory)

bound = np.loadtxt('../out/data/out_bound_0000.dat')
fluid_files = [np.loadtxt(f) for f in sorted(glob.glob('../out/data/out_fluid_*.dat'))]

x_pix = 1280
y_pix = 640

x_size = 3 #inches
y_size = x_size/x_pix*y_pix

highlight = False
if highlight:
    h_part = 1153

for (count,f) in enumerate(fluid_files):
    figure=plt.figure()
    ax = figure.add_subplot(111)
    ax.set_aspect('equal')
    figure.set_dpi(100)
    figure.set_size_inches(12.80, 6.40)
    plt.scatter(f[:,0], f[:,1], c=f[:,2], s=20, edgecolors='none')
    plt.scatter(bound[:,0], bound[:,1], c='black', s=20, edgecolors='none')
    if highlight:
        plt.scatter(f[:,0][h_part], f[:,1][h_part], c='black', s=40, edgecolors='none')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 0.5)
    #plt.xlim(0.1, 0.2)
    #plt.ylim(0.05, 0.1)
    plt.savefig('../out/images/out_{:04d}.png'.format(count))
    print('{:4d}/{:4d}'.format(count+1,len(fluid_files)))
