#!/usr/bin/env python

from subprocess import call
import os
import sys
import yaml

os.chdir(os.path.dirname(sys.argv[0]))
basedir = '../'

# Get directory from YAML file.
if len(sys.argv) != 2:
    print "Error: Wrong number of arguments (usage: plot.py file.yaml)"
    sys.exit(0)
filename = basedir + sys.argv[1]
if not os.path.isfile(filename):
    print "YAML file does not exist"
    sys.exit(0)
file = open(filename)
node = yaml.safe_load(file)
file.close()
directory = node['output directory'] + '/images/'

call(['eog', directory + 'out_0000.png'])
