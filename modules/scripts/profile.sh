#!/bin/bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd $SCRIPTDIR
valgrind --tool=callgrind --dump-instr=yes --simulate-cache=yes --collect-jumps=yes ../bin/main
popd
