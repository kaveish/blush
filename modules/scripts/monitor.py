#!/usr/bin/env python

import argparse
import os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.spines as spines
import numpy as np
import sys
import glob
import yaml
import time
import collections

from scipy.interpolate import griddata
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.colors import colorConverter
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection

## Range class
Range = collections.namedtuple('Range', ['minval', 'maxval'])

## Globals

control_manual = 0
control_forward = 0
control_backward = 0
particles = None
boundary = None
cells = None
args = None
time_text = None
fluid_files = None
boundary_files = None
cells_files = None
color_range = None
colormap = None
output_period = None

def extant_file(x):
    """
    'Type' for argparse - checks that file exists but does not open.
    """
    if not os.path.exists(x):
        raise argparse.ArgumentError("{0} does not exist".format(x))
    return x

def colored_variable(f):
    variable = []
    if (args.colored_variable == 'speed'):
        variable = np.sqrt(f[:,2]*f[:,2] + f[:,3]*f[:,3])
    elif (args.colored_variable == 'pressure'):
        variable = f[:,5]
    elif (args.colored_variable == 'density'):
        variable = f[:,4]
    elif (args.colored_variable == 'force'):
        variable = np.sqrt(f[:,6]*f[:,6] + f[:,7]*f[:,7])
    elif (args.colored_variable == 'densityrate'):
        variable = f[:,8]
    elif (args.colored_variable == 'vx'):
        variable = f[:,3]
    elif (args.colored_variable == 'vy'):
        variable = f[:,4]
    elif (args.colored_variable == 'forcex'):
        variable = f[:,6]
    elif (args.colored_variable == 'forcey'):
        variable = f[:,7]
    return variable

def get_global_data_range(fs):
    data_range = []
    for f in fs:
        data = np.loadtxt(f)
        variable = colored_variable(data).tolist()
        data_range = get_min_max(variable + list(data_range))
    return data_range

def get_min_max(list):
    minval = min(list)
    maxval = max(list)
    return Range(minval,maxval)

def colors_from_data(data, data_range):
    diff = data_range.maxval - data_range.minval
    colors = (data - data_range.minval) / diff
    colors_rgba = colormap(colors)
    return colors_rgba

# Called at start of animation to draw a blank frame, rather than the
# default which leaves the first frame of particles permanently
# showing.
def init():
    global time_text
    particles.set_offsets([])
    boundary.set_offsets([])
    if (args.color == 'constant'):
        particles.set_facecolor('white')
        boundary.set_facecolor('white')
    if not (args.boundary):
        boundary.set_facecolor('white')
    time_text.set_text('')
    return particles, time_text, boundary, cells

def animate(i):
    global color_max, color_min, color_range

    # Fluid and boundary particles: Load data, then color and set positions

    f_fluid = np.loadtxt(fluid_files[i])
    f_boundary = np.loadtxt(boundary_files[i])
    data_fluid = colored_variable(f_fluid)
    data_boundary = colored_variable(f_boundary)

    # get maximum color
    if (args.color == 'local'):
        color_range = get_min_max(np.concatenate([data_fluid,data_boundary]))
        if (args.scale == 'zero'):
            color_range = Range(0, color_range.maxval)
    if not (args.color == 'constant'):
        fluid_colors = colors_from_data(data_fluid, color_range)
        particles.set_facecolor(fluid_colors)
        if (args.boundary):
            boundary_colors = colors_from_data(data_boundary, color_range)
            boundary.set_facecolor(boundary_colors)

    particles.set_offsets(list(zip(f_fluid[:, 0], f_fluid[:, 1])))
    boundary.set_offsets(list(zip(f_boundary[:, 0], f_boundary[:, 1])))

    # Cells: Load data, then color and set positions

    if args.cells:
        f_cells = np.loadtxt(cells_files[i])

        cells_colors = []
        for cell in f_cells:
            if cell[5] == 1:
                cells_colors.append(colorConverter.to_rgba('#b1d7b3'))
            else:
                cells_colors.append(colorConverter.to_rgba('#d7b1b1'))

        cellCollection = []
        for rx, ry, sx, sy, contains in zip(f_cells[:,1], f_cells[:,2], f_cells[:,3], f_cells[:,4], f_cells[:,5]):
            if contains == 1:
                col = colorConverter.to_rgba('green')
            else:
                col = colorConverter.to_rgba('red')
            cellCollection.append(Rectangle((rx,ry),sx,sy,edgecolor="none",facecolor="none"))
        cells.set_paths(cellCollection)
        cells.set_edgecolor(cells_colors)
        cells.set_facecolor('none')

    # Time: print time string

    time_text.set_text('file = %d  time = %.3f' % (i,i*output_period))

    return particles, time_text, boundary, cells

def press(event):
    global control_manual, control_forward, control_backward
    control_manual = 1
    sys.stdout.flush()
    if event.key=='right':
        control_forward = 1
    if event.key=='left':
        control_backward = 1

def counter():
    global control_manual, control_forward, control_backward
    count = 0
    global fluid_files, boundary_files
    pause = 30
    while True:
        if control_manual:
            break
        count += 1
        if count == len(fluid_files) and pause != 0:
            count -= 1
            pause -= 1
        elif pause == 0:
            count = 0
            pause = 30
            fluid_files = sorted(glob.glob(directory_in + 'out_fluid_*.dat'))
            boundary_files = sorted(glob.glob(directory_in + 'out_bound_*.dat'))
        yield count
    while True:
        if control_forward:
            if (count == len(fluid_files)-1):
                count = 0
            else:
                count += 1
                control_forward = 0
        if control_backward:
            if (count == 0):
                count = len(fluid_files)-1
            else:
                count -= 1
                control_backward = 0
        yield count

# Read in command line arguments
def read_args():
    parser = argparse.ArgumentParser(description='''
            Live display of BLUSH output data.

            Example:
                monitor.py file.yaml --color global
            '''
                                     )
    parser.add_argument(
        'input_file', metavar='FILE.yaml', default='local',
        type=extant_file,
        help='''
            YAML configuration file used for the simulation you wish to read.
            '''
        )
    parser.add_argument(
        '-i', '--init', dest='init', action='store_true',
        help='''
            view initialisation run output rather than main run (default false)
            '''
        )
    parser.add_argument(
        '-c', '--color', metavar='OPTION', dest='color', default='local',
        choices=['local','global','constant','user'],
        help='''
            specify how color is used to represent variables. Options:
            local (default): color is scaled to the maximum value of the
            variable for each individual output file. global: color is
            scaled to the maximum value of the variable in all output
            files. constant: color does not change. user: must provide
            --colorrange argument with min/max values.
            '''
        )
    parser.add_argument(
        '--colorrange', metavar='MIN MAX', nargs=2,
        help='''
            min/max in color range.
            '''
        )
    parser.add_argument(
        '-d', '--directory', metavar='DIR', dest='directory',
        default=0,
        help='''
            load files from different directory to specified in YAML file.
            '''
        )
    parser.add_argument(
        '-f', '--frames', metavar='FRAMES', dest='frames', default=0, type=int,
        help='''
            number of frames to output.
            '''
        )
    parser.add_argument(
        '--fps', metavar='FRAMES', dest='fps', default=30, type=int,
        help='''
            frames per second to play back, output video will be resampled to 30fps.
            '''
        )
    parser.add_argument(
        '--outfps', metavar='FRAMES', dest='outfps', default=30, type=int,
        help='''
            frames per second to output at, default 30fps.'''
        )
    parser.add_argument(
        '-v', '--variable', metavar='OPTION', dest='colored_variable',
        default='speed', choices=['speed','pressure','density','force',
                                  'densityrate','vx','vy','forcex','forcey'],
        help='''
            specify which variable should be represented by a change in
            color [speed (default), pressure, density].
            '''
        )
    parser.add_argument(
        '-s', '--scale', metavar='OPTION', dest='scale',
        default='zero', choices=['zero','stretch'],
        help='''
            on what scale should variables be represented, zero: 0-max,
            stretch: min-max [zero, stretch].
            '''
        )
    parser.add_argument(
        '-m', '--nomargins', dest='nomargins', action='store_true',
        help='''
            hide margins.
            '''
        )
    parser.add_argument(
        '--cells', dest='cells', action='store_true',
        help='''
            show cell grid.
            '''
        )
    parser.add_argument(
        '-p', '--pointsize',  metavar='SIZE', dest='pointsize',
        default=20, type=int,
        help='''
            set size of particles.
            '''
        )
    parser.add_argument(
        '-o', '--output', metavar='FILENAME.mp4', dest='output',
        help='''
            create a video output file FILENAME.mp4 instead of displaying output.
            '''
        )
    parser.add_argument(
        '--vres', metavar='PIXELS', dest='vres', default='1280', type=int,
        help='''
            output video at this horizontal resolution
            '''
        )
    parser.add_argument(
        '--hres', metavar='PIXELS', dest='hres', default='720', type=int,
        help='''
            output video at this horizontal resolution
            '''
        )
    parser.add_argument(
        '--single', metavar='NUMBER', dest='single', default=-1, type=int,
        help='''
            output a single frame only, provide which frame to output as argument.
            '''
        )
    parser.add_argument(
        '--contour', dest='contour', action='store_true',
        help='''
            contour plot.
            '''
        )
    parser.add_argument(
        '--boundary', dest='boundary', action='store_true',
        help='''
            Show boundary properties as well as fluid.
            '''
        )
    parser.add_argument(
        '--region', metavar='XMIN YMIN XMAX YMAX', dest='region',
        type=float, nargs=4,
        help='''
            Plot this region instead of whole domain.
            '''
        )
    args = parser.parse_args()
    return args

def main():
    global particles, boundary, args, time_text, fluid_files, boundary_files, cells, cells_files
    global color_range, colormap, output_period
    global directory_in

    start_time = time.clock()

    args = read_args()

    # Get directory from YAML file.
    file = open(args.input_file)
    node = yaml.safe_load(file)
    file.close()
    directory_in = node['output directory'] + '/'
    if args.directory:
        directory_in = args.directory + '/'
    if not os.path.isdir(directory_in):
        print "Input directory does not exist"
        sys.exit()
    if args.init:
        directory_in += 'init/'
    else:
        directory_in += 'run/'

    output_period = node['output period']

    scale = node["scale length"]
    if args.region is None:
        x_min = scale * node['limits']['min'][0]
        x_max = scale * node['limits']['max'][0]
        y_min = scale * node['limits']['min'][1]
        y_max = scale * node['limits']['max'][1]
    else:
        x_min = args.region[0]
        y_min = args.region[1]
        x_max = args.region[2]
        y_max = args.region[3]

    domainSize = [x_max - x_min, y_max - y_min]
    domainCentre = [x_min + domainSize[0]/2.0, y_min + domainSize[1]/2.0]
    domainMaxWidthHeight = max(domainSize)
    halfWidth = domainMaxWidthHeight / 2.0
    plotRegion = [(domainCentre[0]-halfWidth,domainCentre[0]+halfWidth),(domainCentre[1]-halfWidth,domainCentre[1]+halfWidth)]

    x_pix = 1280
    y_pix = 720

    highlight = False
    if highlight:
        h_part = 1153

    # bound = np.loadtxt(directory_in + 'out_bound_0000.dat')

    # set up figure and animation
    fig = plt.figure(num=None, figsize=(10, 10), dpi=80, facecolor='#171f32', edgecolor='white')
    ax = fig.add_subplot(111, aspect='equal',
                         xlim=plotRegion[0], ylim=plotRegion[1], axisbg='#171f32')

    # set all axis colors to white
    ax.spines['bottom'].set_color('w')
    ax.spines['top'].set_color('w')
    ax.spines['left'].set_color('w')
    ax.spines['right'].set_color('w')
    ax.xaxis.label.set_color('w')
    ax.tick_params(axis='x', colors='w')
    ax.tick_params(axis='y', colors='w')

    if args.nomargins:
        plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
        fig.patch.set_facecolor('#171f32')
    else:
        plt.subplots_adjust(left=0.05, right=0.98, top=0.95, bottom=0.05)
    fig.set_dpi(50)
    fig.set_size_inches(x_pix/100.0, y_pix/100.0)

    fluid_files = sorted(glob.glob(directory_in + 'out_fluid_*.dat'))
    boundary_files = sorted(glob.glob(directory_in + 'out_bound_*.dat'))
    cells_files = sorted(glob.glob(directory_in + 'out_cells_*.dat'))
    if (args.frames):
        fluid_files = fluid_files[0:args.frames]
        boundary_files = boundary_files[0:args.frames]
        cells_files = cells_files[0:args.frames]

    # Grid to demonstrate particle placement
    # Should be added as option
    # dp = scale * node['particle spacing']
    # x_size = x_max - x_min
    # y_size = y_max - y_min
    # n_part_x = int(x_size / dp)
    # n_part_y = int(y_size / dp)
    # n_part = n_part_x * n_part_y
    # points_x = np.arange(x_min,x_max,dp)
    # points_y = np.arange(y_min,y_max,dp)
    # plot_x = [None] * n_part
    # plot_y = [None] * n_part
    # for x in range(n_part_x):
    #     for y in range(n_part_y):
    #         plot_x[y*n_part_x+x] = points_x[x]
    #         plot_y[y*n_part_x+x] = points_y[y]
    # plt.scatter(plot_x, plot_y, c='gray', s=5, edgecolors='none')

    # particles holds the locations of the particles
    particles = ax.scatter([], [], s=args.pointsize, edgecolors='none')
    ax.add_collection(particles)

    boundary = ax.scatter([], [], s=args.pointsize/2, edgecolors='none')
    ax.add_collection(boundary)

    cells = PatchCollection([])
    ax.add_collection(cells)

    #plt.scatter(bound[:,0], bound[:,1], s=args.pointsize, c='white',
    #            edgecolors='none')

    time_text = ax.text(0.01, 0.97, '', transform=ax.transAxes, color='white')

    colormap = plt.get_cmap('coolwarm')

    # get max color in all files
    if (args.color == 'global'):
        # Get the maximum value of the colored variable across all files
        color_range = get_global_data_range(fluid_files + boundary_files)
        if (args.scale == 'zero'):
            color_range = Range(0, color_range.maxval)
    if (args.color == 'user'):
        color_range = Range(float(args.colorrange[0]),
                            float(args.colorrange[1]))

    # Single plot
    if (args.single != -1):
        ax = fig.add_subplot(111, aspect='equal',
                             xlim=(x_min, x_max), ylim=(y_min, y_max), axisbg='White')

        f = np.loadtxt(fluid_files[args.single])
        fb = np.loadtxt(boundary_files[args.single])
        if (args.contour):
            #xi = np.linspace(min(f[:,0]), max(f[:,0]),100)
            #yi = np.linspace(min(f[:,1]), max(f[:,1]),100)
            grid_x, grid_y = np.mgrid[x_min:x_max:50j, y_min:y_max:10j]
            zi = griddata((f[:,0], f[:,1]), np.sqrt(f[:,2]*f[:,2] + f[:,3]*f[:,3]), (grid_x, grid_y), method='cubic')
            print zi
            xi = grid_x
            yi = grid_y
            plt.contour(xi, yi, zi, 15, colors='k')
            plt.contourf(xi, yi, zi, 15, cmap=plt.cm.jet)
        else:
            scale = max(np.sqrt(f[:,2]**2+f[:,3]**2)) * 10 / (args.pointsize/20.0)
            if args.colored_variable == 'force':
                lenX = 6
                lenY = 7
            if args.colored_variable == 'speed':
                lenX = 2
                lenY = 3
            if args.colored_variable == 'vx':
                lenX = 3
                lenY = 8
                f[:,8] = [0] * len(f[:,3])
                fb[:,8] = [0] * len(fb[:,3])
            if args.colored_variable == 'vy':
                lenX = 8
                lenY = 4
                f[:,8] = [0] * len(f[:,4])
                fb[:,8] = [0] * len(fb[:,4])
            if args.colored_variable == 'forcex':
                lenX = 6
                lenY = 8
                f[:,8] = [0] * len(f[:,6])
                fb[:,8] = [0] * len(fb[:,6])
            if args.colored_variable == 'forcey':
                lenX = 8
                lenY = 7
                f[:,8] = [0] * len(f[:,7])
                fb[:,8] = [0] * len(fb[:,7])
            plt.quiver(f[:,0], f[:,1], f[:,lenX], f[:,lenY],
                       pivot='mid', scale=scale, headwidth=2, minlength=0, headlength=3)
            plt.quiver(fb[:,0], fb[:,1], fb[:,lenX], fb[:,lenY],
                       pivot='mid', scale=scale, headwidth=2, minlength=0, headlength=3, color='b')

        #qk = quiverkey(Q, 0.5, 0.92, 2, r'$2 \frac{m}{s}$', labelpos='W',
        #               fontproperties={'weight': 'bold'})
        #l,r,b,t = axis()
        #dx, dy = r-l, t-b
        #axis([l-0.05*dx, r+0.05*dx, b-0.05*dy, t+0.05*dy])
        plt.show()
        sys.exit()

    if args.output:
        if args.frames:
            nframes = args.frames
        else:
            nframes = min(len(fluid_files),len(cells_files))
        print "Outputting " + repr(nframes) + " frames, approx time " + \
            repr(nframes*0.274) + " seconds"
        anim = animation.FuncAnimation(fig, animate, interval=33.33,
                                       blit=True, init_func=init,
                                       frames=nframes)
    else:
        anim = animation.FuncAnimation(fig, animate, counter, interval=33.33,
                                       blit=True, init_func=init)

    if args.output:
        out_file = args.output + '.mp4'
        anim.save(out_file, fps=args.fps,
                  extra_args=['-vcodec', 'libx264', '-r', str(args.outfps)])
        #savefig_kwargs={'facecolor':fig.get_facecolor()})
    else:
        fig.canvas.mpl_connect('key_press_event', press)
        plt.show()

    end_time = time.clock()
    print "time (s) = " , end_time - start_time

if __name__ == '__main__':
    main()
