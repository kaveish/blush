#!/usr/bin/env python

from subprocess import call
import argparse
import os
import sys

basedir = '../'

# Read in input arguments.
parser = argparse.ArgumentParser(description='''
        Runs BLUSH simulation, clearing any previous data.

        Example:
            run.py file.yaml --init
        '''
                                 )
parser.add_argument(
    'input_file', metavar='FILE.yaml', default='local',
    help='''
        YAML configuration file used for the simulation you wish to run.
        '''
    )
parser.add_argument(
    '-i', '--init', dest='init_mode', action='store_const', const=1, default=0,
    help='''
        set simulation to run in init mode.
        '''
    )
parser.add_argument(
    '--parallel', dest='parallel', action='store_const', const=1, default=0,
    help='''
        run parallel version of program.
        '''
    )
args = parser.parse_args()

input_file = os.path.abspath(args.input_file)

os.chdir(os.path.dirname(sys.argv[0]))

# Run simulation
if args.parallel:
    call(['./' + basedir + 'bin/blush_parallel', input_file, str(args.init_mode)])
else:
    call(['./' + basedir + 'bin/blush_serial', input_file, str(args.init_mode)])
