#!/usr/bin/env python

import argparse
import os
import sys
import yaml
import shutil, errno
import time, datetime

def extant_file(x):
    """
    'Type' for argparse - checks that file exists but does not open.
    """
    if not os.path.exists(x):
        raise argparse.ArgumentError("{0} does not exist".format(x))
    return x

# Read in input arguments.
parser = argparse.ArgumentParser(description='''
        Backup case data folder. Defaults to dated backup in same
        folder as original.

        Example:
            backup.py file.yaml
        '''
                                 )
parser.add_argument(
    'input_file', metavar='FILE.yaml', default='local',
    type=extant_file,
    help='''
        YAML configuration file.
        '''
    )
parser.add_argument(
    '-o', metavar='FOLDER', dest='output_folder',
    help='''
        specifies backup folder location.
        '''
    )
args = parser.parse_args()

# Get directory from YAML file.
filename = os.path.abspath(args.input_file)
if not os.path.isfile(filename):
    print "YAML file does not exist"
    sys.exit(0)
file = open(filename)
node = yaml.safe_load(file)
file.close()
directory = node['output directory']

if args.output_folder:
    destination = args.output_folder
else:
    ts = time.time()
    timestamp = datetime.datetime.fromtimestamp(ts).strftime(
        '%Y-%m-%d-%H%M%S')
    destination = directory + "/../" + "bak-" + timestamp

shutil.copytree(directory, destination)
