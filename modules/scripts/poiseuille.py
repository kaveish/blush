#!/usr/bin/env python3.4

import argparse
import matplotlib.pyplot as plt
import numpy as np
import glob

def read_args():
    parser = argparse.ArgumentParser(description='''
            Plot Poiseuille analytical curve against data
            '''
                                     )
    parser.add_argument(
        'input_dir', metavar='DIR',
        help='''
            Directory containing files to read
            '''
        )
    parser.add_argument(
        '-w', '--window', dest='window', action='store_true',
        help='''
            Display plotting window
            '''
        )
    parser.add_argument(
        '-f', '--file', dest='file', action='store_true',
        help='''
            Output to plot.png in same directory as input files
            '''
        )
    parser.add_argument(
        '--dp', dest='dp', default=0,
        help='''
        Particle spacing from wall
        '''
    )
    parser.add_argument(
        '-o', dest='out',
        help='''
        Output file
        '''
    )
    parser.add_argument(
        '--fittest', dest='fittest', action='store_true',
        help='''
            Run a chi squared test of goodness of fit on data.
            '''
    )
    args = parser.parse_args()
    return args

def poiseuilleTerm2(n, t, F, visc, L, x):
    "return poiseuille flow profile at a given time"
    return ((4*F*L**2)/(visc*np.pi**3*(2*n+1)**3) *
            np.sin(np.pi*x/L*(2*n+1))*np.exp(-(2*n+1)**2*np.pi**2*visc*t/L**2))

def poiseuille(inX, L, xMin, t):
    "return poiseuille flow profile at a given time"
    F = 0.1
    visc = 0.01
    x = inX - xMin
    term1 = F/(2*visc)*x*(x-L)
    return -(term1 + poiseuilleTerm2(0, t, F, visc, L, x)
             + poiseuilleTerm2(1, t, F, visc, L, x)
             + poiseuilleTerm2(2, t, F, visc, L, x)
             + poiseuilleTerm2(3, t, F, visc, L, x)
             + poiseuilleTerm2(4, t, F, visc, L, x)
             + poiseuilleTerm2(5, t, F, visc, L, x))

def rescaleToLength(data, L, xMin):
    return (data-xMin)/L - 0.5

def runFlowProfile(args, xRange, L):
    # theory
    x = np.linspace(xRange[0],xRange[1],100)
    scaledX = rescaleToLength(x, L, xRange[0])

    files = [("0002","2"),("0010","10"),("0020","20"),("0100","100")]
    files = [(args.input_dir + "/out_fluid_" + f +
              ".dat",n) for f,n in files]

    datalist = [(np.loadtxt(filename), label) for filename, label in files]

    # Setup figure
    plt.rcParams.update({'font.size': 10})
    plt.figure(1, figsize=(12,8))

    for data, label in datalist:
        scaledData = [rescaleToLength(data[:,1], L, xRange[0]),data[:,2]]
        plt.plot(scaledData[0], scaledData[1], 'o', label=label)

    plt.plot(scaledX, poiseuille(x,L,xRange[0],2), label="theory t=2")
    plt.plot(scaledX, poiseuille(x,L,xRange[0],10), label="theory t=10")
    plt.plot(scaledX, poiseuille(x,L,xRange[0],20), label="theory t=20")
    plt.plot(scaledX, poiseuille(x,L,xRange[0],100), label="theory t=100")

    plt.axhline(0, c='black')
    plt.axvline((1.0-xRange[0])/L-0.5, c='black')
    plt.axvline((2.0-xRange[0])/L-0.5, c='black')

    plt.legend()
    plt.title("Evolving periodic flow profile in tube")
    plt.xlabel("y position")
    plt.ylabel("x velocity")

    if args.file:
        outfile = None
        if args.out:
            outfile = args.out
        else:
            outfile = args.input_dir+'/plot.png'
        plt.savefig(outfile, bbox_inches='tight')
    if args.window:
        plt.show()

def runFitTest(args, xRange, L):
    files = []
    for i in range(1,301):
        fileName = args.input_dir+"/out_fluid_"+format(i,'04')+".dat"
        files.append((fileName,i))

    data = [(np.loadtxt(filename,usecols=[1,2]), n) for filename, n in files]
    # for d in data:
    #     d[0][:,0] = rescaleToLength(d[0][:,0], L, xRange[0])

    # test fit for each file
    fit = np.empty([0,2])
    for d in data:
        time = d[1]
        fitAtTime = 0
        for particle in d[0]:
            position = particle[0]
            velocity = particle[1]
            theoryVelocity = poiseuille(position,L,xRange[0],time)
            fitAtTime += (velocity-theoryVelocity)**2/theoryVelocity
        fit = np.vstack((fit,[time,fitAtTime]))

    plt.plot(fit[:,0],fit[:,1])
    plt.xlabel("time")
    plt.ylabel("ChiSq")

    if args.file:
        outfile = None
        if args.out:
            outfile = args.out
        else:
            outfile = args.input_dir+'/fit.png'
        plt.savefig(outfile, bbox_inches='tight')
    if args.window:
        plt.show()

def main():
    args = read_args()

    # "input"
    xRange = [1.0 + float(args.dp)/2.0,
              2.0 - float(args.dp)/2.0]
    L = xRange[1] - xRange[0]

    if args.fittest:
        runFitTest(args, xRange, L)
    else:
        runFlowProfile(args, xRange, L)


if __name__ == '__main__':
    main()
