#!/bin/sh
#
# Generate dependency files from source files.
# Adapted from Peter Millers' "Recursive Make Considered Harmful"
#
# This should be used to create .d files *before* any compilation
# takes place. If .d files are created at the same time as
# compilation, this requires other hacks to ensure that all
# dependencies are built; if we don't know the dependencies when
# building a source file, how do we know what else to build?
#
# Arguments:
# [*.o and *.d directory] [CXXFLAGS] [*.cpp file]
# Output is then redirected to the file:
# obj/*.d
#
# Changes from original:
# * gcc to g++
# * g++ -MG flag removed, this adds includes to the .d even if they
# don't exist with no warning. The motivation is that the includes
# could be created at a later time. This may be useful at a later
# date, but it can make debugging harder. I originally used -MG and
# didn't realise that includes weren't being found since I hadn't
# provided the correct -I include directory. In this case -MG still
# added the headers to the .d but without the correct paths.
#

DIR="$1" # directory of .d file
shift 1 # shifts positional parameters to the left by 1
# generate .d file, then prefix target with directory
# -MM ignores system headers.
g++ -MM "$@" |
sed -e :a -e '/\\$/N; s/\\\n//; ta' \
-e "s@^\(.*\)\.o:@$DIR/\1.o:@"