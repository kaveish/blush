#!/bin/sh
#
# Process dependency files further to
#
# Arguments:
# [*.d file] [object directories]
#
# start with:
# blush/src/bin/blush.o: modules/blush/src/bin/blush.cpp  \
# modules/blush/src/lib/initialize.hpp
# becomes:
# clang/release/serial/blush/src/bin/blush.o ... \
# obj/depends/blush/src/bin/blush.d: modules/blush/src/bin/blush.cpp \
# modules/blush/src/lib/initialize.hpp

DEPFILE="$1"
shift 1 # shifts positional parameters to the left by 1
# read current obj name from dependency file
OBJ=$(cat $DEPFILE | sed -e 's/\(^.*\):.*/\1/')
# take obj and add prefix to all obj names, the first obj name is actually a .d
NEWOBJ=$(echo obj/depends "$@" | sed -e "s;[^ ]*;&/$OBJ;g" -e "s;\.o;\.d;")
# replace old obj with all the newobj
sed -i "s;\(^.*\)\(:.*\);$NEWOBJ\2;" $DEPFILE
