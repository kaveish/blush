# BLUSH project makefile
#
# - Created using principles from Peter Miller's Recursive Make Considered
#   Harmful.
# - Dependencies are created before all object files.
# - Binaries and obj files are placed in subdirectories depending on compilation
#   options. This allows ALL options to be run at once if required.
#
# Usage:
# - Set option flags to activate a particular build route,
#   e.g. make DEBUG=0
# - Possible option flags
#   - DEBUG=1
#   - ALLOPTS=0
#   - COMPILEROPTS=[list]
#   - RELEASEOPTS=[list]
#   - PARALLELOPTS=[list]
# - Possible command line targets:
#   - [none] - runs "debug" by default
#   - clean - remove obj dir containing .o and .d files
#   - cleanall - remove obj and bin dirs
#   - cleanbin - remove bin dir (remove in future)
#   - test - runs all tests which would be created with the provided options
#
# Directory structure is:
# bin/COMPILERTYPE/RELEASETYPE/PARALLELTYPE/MODULENAME/[binaries]
#                                                     /test
# modules/MODULE/src/lib/[.hpp .cpp]
#                   /bin/[.hpp .cpp]
#               /test/[.hpp .cpp]
#               /module.mk
#
# Todo:
# - Binaries end up classed as intermediate files and aren't recreated if
#   deleted.
# - Add intel and test, it should work but need to test in build environ.
#
# Advice:
# I'd start by putting all the platform-specific defines in a separate makefile. That way you can do:
# include $(TARGET).make
# Where $(TARGET).make defines CC and other variables for each particular platform. Then you can call make recursively setting TARGET to what you want. Something like:
# build:
#     $(MAKE) TARGET=platform1 all
#     $(MAKE) TARGET=platform2 all

SHELL := /bin/bash

# ---------------------------------------------------
# Global settings (modify these for new modules etc.)
# ---------------------------------------------------

MODULES := blush

# --------------------------------------------------------
# Process options, options can only be turned ON, not OFF.
# --------------------------------------------------------

# Options
DEBUG := 1
ALLOPTS := 0

# Set option lists depending on variables set in command line
ifndef $(COMPILEROPTS)
	ifeq ($(DEBUG), 1)
		COMPILEROPTS := clang
		RELEASEOPTS := debug
		PARALLELOPTS := serial
	else
		COMPILEROPTS := gnu
		RELEASEOPTS := release
		PARALLELOPTS := serial
	endif
	ifeq ($(ALLOPTS), 1)
		COMPILEROPTS := gnu clang #intel
		RELEASEOPTS := debug release
		PARALLELOPTS := serial parallel
	endif
endif

# -------------------------------------
# Shared variables for all build routes
# -------------------------------------

# Variables eventually passed to compiler
CXXFLAGS := -std=c++11 -static -pthread -isystem /home/david/local_libraries/gcc/include/ # pthread for gtest, removed frounding-math
LDFLAGS := -L /home/david/local_libraries/lib/
LDLIBS := -pthread -lgtest # flags for gtest

# Variables added to the above lists for specific build routes
CXXFLAGS_RELEASE := -O3
CXXFLAGS_DEBUG := -g3
CXXFLAGS_PARALLEL :=
LDFLAGS_RELEASE :=
LDFLAGS_PARALLEL :=

# Warnings
CXXFLAGS += -pedantic \
 -Wall \
 -Wextra \
 -Wcast-align \
 -Wcast-qual \
 -Wno-ctor-dtor-privacy \
 -Wdisabled-optimization \
 -Wformat=2 \
 -Winit-self \
 -Wmissing-declarations \
 -Wmissing-include-dirs \
 -Wno-missing-field-initializers \
 -Wold-style-cast \
 -Woverloaded-virtual \
 -Wredundant-decls \
 -Wshadow \
 -Wsign-promo \
 -Wstrict-overflow=5 \
 -Wswitch-default \
 -Wundef \
 -Werror \
 -Wno-unknown-pragmas \
 -Wno-error=undef \
 -Wno-undef \
 -Wno-error=strict-overflow \
 -Wno-strict-overflow
# Unused:
# -Wconversion
# -Wsign-conversion

# -------------------------------
# Functions to build object files
# -------------------------------

# Utility functions
fRemoveTopDir = $(foreach item,$(1),$(shell echo $(item) | sed 's,^[^/]*/,,'))

fDirname = $(patsubst %/,%,$(dir $(1)))

# Used by included module.mk files
fObjFromSrc = $(addsuffix .o,\
$(call fRemoveTopDir,$(call fRemoveTopDir,$(basename $(1)))))

# Used for calculating .d dependency files
fRemoveTopObjDirs = $(call fRemoveTopDir,$(call fRemoveTopDir,$(1)))

# Creating lists of objects for a given binary
fBinToBinObj = \
$(addprefix obj/$(call fRemoveTopDir,$(call fDirname, $(1)))/,\
$(filter %/$(notdir $(1)).o,\
$(MOD_$(notdir $(call fDirname, $(1)))_OBJBIN)))

fBinToLibObj = \
$(addprefix obj/$(call fRemoveTopDir,$(call fDirname, $(1)))/,\
$(MOD_$(notdir $(call fDirname, $(1)))_OBJLIB))

fBinToTestObj = \
$(addprefix obj/$(call fRemoveTopDir,$(call fDirname, $(1)))/,\
$(MOD_$(notdir $(call fDirname, $(1)))_OBJTEST))

# Combining the above lists to create all binary dependencies
fBinToObj = $(call fBinToBinObj,$(1)) $(call fBinToLibObj,$(1))

fTestBinToObj = $(call fBinToTestObj,$(1)) $(call fBinToLibObj,$(1))

# Creates dependencies for a module on a given build route
fModuleBinPathToBins = \
$(addprefix $(1)/,$(basename $(notdir $(MOD_$(notdir $(1))_SRCBIN)))) \
$(1)/test

# --------------------------------
# Read modules from subdirectories
# --------------------------------

# Each module needs its own module.mk file
include $(MODULES:%=modules/%/module.mk)

# Modules need to see headers from their own and other modules
CXXFLAGS += $(patsubst %,-Imodules/%/src/lib,$(MODULES))

# form: bin/COMPILERTYPE/RELEASETYPE/PARALLELTYPE/MODULENAME/bins

BINS := $(basename \
$(foreach mod, $(MODULES), \
$(addprefix $(mod)/, \
$(notdir $(MOD_$(mod)_SRCBIN)))))

BINS := bin
BINS := $(foreach type,gnu clang intel,$(addsuffix /$(type),$(BINS)))
BINS := $(foreach type,release debug,$(addsuffix /$(type),$(BINS)))
BINS := $(foreach type,serial parallel,$(addsuffix /$(type),$(BINS)))

MODULEBINPATHS := $(foreach mod,$(MODULES),$(addsuffix /$(mod),$(BINS)))

DEPS := $(patsubst %.o,%.d,$(addprefix obj/depends/,\
$(foreach mod,$(MODULES),$(addprefix $(mod)/,$(MOD_$(mod)_OBJBIN))) \
$(foreach mod,$(MODULES),$(addprefix $(mod)/,$(MOD_$(mod)_OBJLIB))) \
$(foreach mod,$(MODULES),$(addprefix $(mod)/,$(MOD_$(mod)_OBJTEST)))))

OBJPREFIXDIRS := $(addprefix obj/,$(call fRemoveTopDir,$(BINS)))

TESTBINS := $(foreach mod,$(MODULES)\
,$(addsuffix /$(mod)/test,$(BINS)))

BINS := $(foreach mod,$(MODULES)\
,$(addsuffix \
$(addprefix /$(mod)/, $(basename $(notdir $(MOD_$(mod)_SRCBIN))))\
,$(BINS)))

#
# rules
#

.SECONDARY: # prevent deletion of intermediate files

all : bin

bin : $(addprefix bin/, $(COMPILEROPTS))
	@mkdir -p $@; touch $@

# compiler types
%/gnu : CXX := g++
%/gnu : CXXFLAGS_PARALLEL += -fopenmp
%/gnu : LDFLAGS_PARALLEL += -fopenmp
%/gnu : CXXFLAGS_DEBUG += -Og
%/gnu : CXXFLAGS += -Wlogical-op \
-Wnoexcept \
-Wstrict-null-sentinel
%/gnu : $(addprefix %/gnu/, $(RELEASEOPTS))
	@mkdir -p $@; touch $@

%/clang : CXX := clang-3.5
%/clang : LDLIBS += -lstdc++ -lm
%/clang : CXXFLAGS_PARALLEL += # requires installation of clang openmp library
%/clang : CXXFLAGS_DEBUG += -O0
%/clang : CXXFLAGS += -Wno-missing-braces
%/clang : $(addprefix %/clang/, $(RELEASEOPTS))
	@mkdir -p $@; touch $@

%/intel : CXX := icpc
%/intel : CXXFLAGS_PARALLEL += -parallel
%/intel : LDFLAGS_PARALLEL += -parallel
%/intel : CXXFLAGS_DEBUG += -O0
%/intel : LDFLAGS_RELEASE += -ipo # consider trying -no-prec-div and PGO
%/intel : CXXFLAGS += -diag-disable 3180 # don't show errors about openmp
                                         # pragmas not existing
%/intel : $(addprefix %/intel/, $(RELEASEOPTS))
	@mkdir -p $@; touch $@

# release types
%/release : CXXFLAGS += $(CXXFLAGS_RELEASE)
%/release : LDFLAGS += $(LDFLAGS_RELEASE)
%/release : $(addprefix %/release/, $(PARALLELOPTS))
	@mkdir -p $@; touch $@

%/debug : CXXFLAGS += $(CXXFLAGS_DEBUG)
%/debug : $(addprefix %/debug/, $(PARALLELOPTS))
	@mkdir -p $@; touch $@

# parallel types
%/serial : $(addprefix %/serial/, $(MODULES))
	@mkdir -p $@; touch $@

%/parallel : CXXFLAGS += $(CXXFLAGS_PARALLEL)
%/parallel : LDFLAGS += $(LDFLAGS_PARALLEL)
%/parallel : $(addprefix %/parallel/, $(MODULES))
	@mkdir -p $@; touch $@

# module

.SECONDEXPANSION: # allows use of automatic variables in prerequisites

# puts binaries in the appropriate module folder
$(MODULEBINPATHS) : $$(call fModuleBinPathToBins,$$@)
	@mkdir -p $@; touch $@

# binary type

# application binaries
$(BINS) : $$(call fBinToObj,$$@)
	$(info $@)
	@mkdir -p $(@D)
	@$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# test binaries
$(TESTBINS) : $$(call fTestBinToObj,$$@)
	$(info $@)
	@mkdir -p $(@D)
	@$(CXX) $(LDFLAGS) -o $@ $^ $(LDLIBS)

# dependencies for obj files

# include dependencies (using "-include" rather than "include" to
# avoid unnecessary "No such file or directory" errors caused by a
# limitation of make
include $(DEPS)
# calculate C++ include dependencies
obj/%.d : modules/$$(basename $$(call fRemoveTopObjDirs,$$@)).cpp
	$(info $@)
	@mkdir -p $(@D)
	@./depend.sh $(call fRemoveTopObjDirs,$(@D)) $(CXXFLAGS) $< > $@
	@./dependprefix.sh $@ $(OBJPREFIXDIRS)

# obj files

obj/%.o :
	$(info $@)
	@mkdir -p $(@D)
	@$(CXX) $(CXXFLAGS) -c $< -o $@

# other phony targets

test:
	$(foreach t,$(TESTBINS),./$(t);)

clean :
	$(RM) -r obj

cleanall :
	$(RM) -r obj bin

cleanbin :
	$(RM) -r bin

.PHONY: all test clean cleanall
