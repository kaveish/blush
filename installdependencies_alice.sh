#!/bin/bash
set -x

cwd=$(pwd)
LIBPATH="/home/d/dam19/local_libraries/"

printf "## Clipper ## \n\n"
cd external/clipper/cpp
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$LIBPATH -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++ ..
make all install
cd ..
rm -r build
cd $cwd

printf "\n## Eigen ## \n\n"
cd external/eigen
mkdir build
cd build
cmake -D -DEIGEN_INCLUDE_INSTALL_DIR=$LIBPATH/include/ -DCMAKE_INSTALL_PREFIX=$LIBPATH -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++ ..
make all install
cd ..
rm -r build
cd $cwd

printf "\n## GoogleTest ## \n\n"
cd external/googletest
mkdir build
cd build
cmake -DBUILD_SHARED_LIBS=ON -Dgtest_build_samples=ON -G"Unix Makefiles" -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++ ..
make
mkdir -v -p "$LIBPATH/include" "$LIBPATH/lib"
cp -v -a ../include/gtest $LIBPATH/include/
cp -v -a lib*.so $LIBPATH/lib/
cd ..
rm -r build
cd $cwd

printf "\n## yaml-cpp ## \n\n"
cd external/yaml-cpp
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX:PATH=/ -D CMAKE_C_COMPILER=gcc -D CMAKE_CXX_COMPILER=g++ ..
make all install DESTDIR=$LIBPATH
cd ..
rm -r build
cd $cwd
