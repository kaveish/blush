#!/bin/bash

cwd=$(pwd)

cd external/clipper/cpp
mkdir build
cd build
cmake ..
sudo make install
cd ..
rm -r build
cd $cwd

cd external/eigen
mkdir build
cd build
cmake ..
sudo make install
cd ..
rm -r build
cd $cwd

cd external/googletest
mkdir build
cd build
cmake -DBUILD_SHARED_LIBS=ON -Dgtest_build_samples=ON -G"Unix Makefiles" ..
make
sudo cp -a ../include/gtest /usr/local/include/
sudo cp -a lib*.so /usr/local/lib/
cd ..
rm -r build
cd $cwd

cd external/yaml-cpp
mkdir build
cd build
cmake ..
sudo make install
cd ..
rm -r build
cd $cwd
